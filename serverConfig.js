﻿/*
160519 kdesign

 server side configrations

*/






var ecoaceAppConfig = {
  datetime       : "2016-05-18 20:55:09.334383",
  parent_address : "1000000",
  power_factor   : "80",

}






var serverConfig = {

  CHILD_NUM : 8,	// max child node
  ADC_NUM   : 4,   // max adc channels in one child

  SYSCONFIG_DEFAULT_FILE : './defaultData/sysConfig.json',    // config def file
  LaTEST_DEVAULT_FILE    : './defaultData/Latest.json',    // config def file
  /*
    mongodb
  */
  MONGO_URL : 'mongodb://127.0.0.1:27017/ecoAce',  // mongodb url

  /*
    configs for parent  (initial data)

      childSerials >>> child1-8 serial No
      childTxInterval >>> children tx interval time array
      fchannel  >>> system wireless frequency channel
      parent serial No read From parent by s command response
  */
  parentConfig : {
    childSerials : [0x80000001, 0x80000002, 0x80000003, 0x80000004, 0x80000005, 0x80000006, 0x80000007, 0x81004e47],
    childTxIntervals : [ 60, 60, 60, 60, 60, 60, 60, 60],
    fchannel : 18,
    parentSerialNo : 0x80000000,
    appId : 0x80000000
  },

  /*
    chileren registered
  */
  childrenRegistered : [0,0,0,0,0,0,0,0],


  /*
    children outer registered
  */
  childrenOuterRegistered : [0,0,0,0,0,0,0,0],

  /*
    children outputs

     child1 to 9
     1: on, 0: off

  */
  childrenOutputs : [0,0,0,0,0,0,0,0],

  /*
    dhildren output timer

      childrenTimerena    1: active, 0: off     (child 1-8)
      childrenTImer       start time, stop time (child 1-8)
  */
  childrenTimerEna : [1,1,1,1,1,1,1,1,],
  childrenTimer : [['08:00','17:00'], ['08:00','17:00'], ['08:00','17:00'], ['08:00','17:00'], ['08:00','17:00'], ['08:00','17:00'], ['08:00','17:00']],

  /*
    children connection state

    timestumpChangeTime : unixtime of timestump change
    timestumps          : pre-timestupm
    childConnection     : connection state 2:ok, 1:yellow, 0:none

      timestumpchange > txIntervalTime >>> childConnection state yellow
      timestumpchange > txIntervalTime * 2 >>> childConneciton state none

  */
  timestumpChangeTime : [0,0,0,0,0,0,0,0],
  timestumps : [0,0,0,0,0,0,0,0],
  childConnectionState : [0,0,0,0,0,0,0,0],

  /*
    dousa mode  /  normal  or test
      test is alternate day demand control disable

    demandCtrl     available at test mode only
      if true, demand control is ordinary
      if false, demand control is disable (allday off)

  */
  dousaMode : 'normal',
  demandCtrl : true,


  /*
    uart connection state
  */
  uartAvailable : false,
  //uartOpen : false,

  /*
   children meas error

   if wcon != 2, meas data is not catch from child
   then data is used last cached data
   if uncached is over 3(>=3), data is unavailable and indicate ERR
    wconCount : errorカウント数(30分毎更新)
    measErr   : 0:正常,　'E': error (30分毎更新)

  */
  wconSafeState : 2,
  measErrLimit : 3,
  wconCount : [0,0,0,0,0,0,0,0],
  measErr : [0,0,0,0,0,0,0,0],
  MEAS_ERR_STR : "ERR",
  /*
   log file関連
  */
  logs: {
    FILE_DIR : './ecoaceLogs/',
    FILE_DIR_CSV : './ecoaceCsv/',
    FILE_LOG1MIN : 'd1min_',
    FILE_LOG30MIN : 'd30min_',
    FILE_CSV30MIN : 'power_'
  },
  /*
   csv maxday string
  */
  MAXDAYSTR: 'MAX/DAY'
}




module.exports = serverConfig;
