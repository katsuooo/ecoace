/*
  uart data for test

*/

var config = require('./serverConfig');


/*
 crc shifter
*/
function shifter_crc(crc){
  for(var i=0; i<8; i++){
    if(crc & 0x01){
      crc = crc >> 1;
      crc = crc ^ 0xa001;
    }else{
      crc = crc >> 1;
    }
  }
  return crc;
}
/*
 gen crc
*/
function genCrc(data, crcnum){
	var crc = 0xffff;
	for(var i=0; i<crcnum; i++){
		crc = crc ^ data[i];
		crc = shifter_crc(crc);
	}
  return crc;
}
/*
 set crc
*/
function setCrc(command){

  var crc = genCrc(command, command.length);
  var bcrc = Buffer.alloc(2);
  bcrc.writeUInt16BE(crc, 0);
  return Buffer.concat([command, bcrc]);
}

var childCounter = 0;   // 0-7
/*
 set child No
*/
function incChildCounter(){
    childCounter++;
    if(childCounter >= 8){
        childCounter = 0;
    }
}

var uartTest = {
    /*
     c command response dummy

     sample
    <Buffer 80 00 00 00 00 00 00 00 00 00 19 00 00 00 00 00 00 00 00 00 00 00 80 0e>
      var tsno = sno.toString(16);  // 4-5
  var timeStump = recieve_buffer.readUInt16BE(4).toString(16);  // 4-5
  var chuukei = recieve_buffer.readUInt8(6).toString(16);       // 6
                                                                // 7 dummy
  var power = (recieve_buffer.readUInt16BE(8)/1000).toFixed(3);      // 8-9
  var temp = recieve_buffer.readInt8(10).toString(10);          // 10
                                                                // 11 dummy
  var adc1 = recieve_buffer.readUInt16BE(12).toString(16);  // 12-13
  var adc2 = recieve_buffer.readUInt16BE(14).toString(16);  // 14-15
  var adc3 = recieve_buffer.readUInt16BE(16).toString(16);  // 16-17
  var adc4 = recieve_buffer.readUInt16BE(18).toString(16);  // 18-19
      param  : none
      return : buffer data (24)
    */
    dummyCcommandResponse : function(){
        serials = config.parentConfig.childSerials;
        var bufs = [];
        var serialBytes = Buffer.allocUnsafe(4);
        serialBytes.writeUInt32BE(serials[childCounter],0);
        var i;
        for(i=0; i<4; i++){
            var x = serialBytes[i];
            bufs.push(x);
        }
        for(; i<22; i++){
            if((i === 12) || (i === 14) || (i === 16) || (i === 18)){
                bufs.push(0x00);        // adc1-4 = 0x1000
            }else{
                bufs.push(0x28);             
            }
            /*
            if(i === 13){       // adc1 lowwer
                bufs.push(0xff);
            }else{
                bufs.push(0x00);
            }
            if(i === 17){       // adc3 lowwer
                bufs.push(0xff);
            }else{
                bufs.push(0x00);
            }
            */
        }     
        var bufsByte = Buffer.from(bufs);
        bufsByte = setCrc(bufsByte);
        incChildCounter();
        return bufsByte;
    }

}


module.exports = uartTest;