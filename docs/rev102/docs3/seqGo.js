/*
servertoMongo/seqGo()
*/




/*
 per min sequence start

*/
seqGo(interval, xjson){
    childConnection.checkWirelessConnection(xjson);       // wireless connection state generate
    // set connection data
    serverConfig.childConnectionState.forEach( (connection, index) => {
      xjson.childs[index].wcon = connection.toString(10);
    });
    // set outputs data
    serverConfig.childrenOutputs.forEach( (output, index) => {
      xjson.childs[index].output = output;
    });

    /*
    not save data per 10sec read
    */
    if(interval === 0){
      console.log('sec read');
      saveLatestOnly(xjson);
      serverLogs.lqiLogs(xjson);                    // lqi console out
      return;
    }

    serverLogs.log1min(JSON.stringify(xjson));		// logging

    serverLogs.lqiLogs(xjson);                    // lqi console out

    MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
      if(err){
        return console.error(err);
      }
      //var collection = db.collection('t_settings');
      var collection = db.collection('config');
      collection.find({}).toArray(function(err, docs){
        if (err) {
          console.log(err);
          return console.error(err);
        };
        db.close();
        if(docs.length === 0){	// not exist t_settings
          calcPower.setChildrenParams(JSON.parse(JSON.stringify(configDefault)));		// children's parameter refresh
        }else{
          calcPower.setChildrenParams(docs[0]);		// children's parameter refresh
        }
        power1min = calcPower.calcNowPower(xjson);	// set power data to global
        if(interval === 1){  // 1min interval
          console.log('--create 1min--');
          //serverLogs.log1min(JSON.stringify(xjson)); // logging >>> 1st line
          save1minPower(power1min);                 // save 1min data
        }else {              // 30min interval
          console.log('--create 30min--');
          serverLogs.log30min(JSON.stringify(power1min));		// logging
          read30min();
        }
      });
    });
  }