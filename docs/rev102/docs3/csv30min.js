/*
 30min csv dump

param : data >>> json power data
        flag24 >>> 24h csv end data
return: none

json format
{
    "_id" : ObjectId("57441810af599dac215d96b3"),
    "time" : "2016-05-24T18:00:00",
    "power_sum_30m" : 289.49951171875,
    "co2_sum_30m" : 91.0475964355469,
    "power" : {
        "c1" : [
            1.422119140625,
            1.422119140625,
            13.4219360351563,
            19.9212646484375
        ],
        ... to "c8"
    },
    "output" : {
        "c1" : "on",
        "c2" : "on",
        "c3" : "on",
        "c4" : "on",
        "c5" : "on",
        "c6" : "on",
        "c7" : "on",
        "c8" : "on"
    }
}

csv titles:
子1_CH1,子1_CH2,子1_CH3,子1_CH4,子1_出力, .... 2 to 8 same, 電力量, CO2量 (42item)
*/
csv30min: (data, flag24) => {
    var date = data.time.split('T')[0];
    if(!fs.existsSync(FILE_DIR_CSV)){
      fs.mkdirSync(FILE_DIR_CSV);
    }
    var fname = FILE_DIR_CSV + FILE_CSV30MIN + date + '.csv';
    var isCsv = false;
    try {
      fs.accessSync(fname, fs.F_OK);
      isCsv = true;
    } catch (e) {
      isCsv = false;
    }

    var titles = '';
    titles += 'Day,';
    titles += '1pTOTAL(kw),';
    titles += '3pTOTAL(kw),';
    titles += '3pMAX/DAY,'; 
    titles += 'TOTAL(CO2),';

    for (var i=0; i<8; i++){
      titles += 'RE0' + (i+1).toString(10) + '_CH1,';
      titles += 'RE0' + (i+1).toString(10) + '_CH2,';
      titles += 'RE0' + (i+1).toString(10) + '_CH3,';
      titles += 'RE0' + (i+1).toString(10) + '_CH4,';
      titles += 'RE0' + (i+1).toString(10) + '_Output,';
    }
    titles = titles.substr(0, titles.length-1);
    titles += '\r\n';  
    // titles end


    if(isCsv === false){  // new file
      fs.appendFileSync(fname, titles);
    }
    /*
     sum line1 fai1
    */
    var pstr = '';    // filing data
    pstr += data.time.substr(0, data.time.length-3) + ',';  // time / cut second
    //pstr += rounding(2, data.power_sum_30m) + ',';        // 1fai power sum    
    var powerFloat = parseFloat(data.power_sum_30m);
    var roundedPower = powerFloat.toFixed(2);
    pstr += roundedPower + ',';
    //pstr += rounding(2, data.power3_sum_30m) + ',';        // 3fai power sum
    powerFloat = parseFloat(data.power3_sum_30m);
    roundedPower = powerFloat.toFixed(2);
    pstr += roundedPower + ',';
    pstr += ',';                                          // add max culumn    
    pstr += '-,';                                         // co2 sum is '-'   
    for(var i=0; i<defs.CHILD_NUM; i++){
      var label = 'c' + (i+1).toString(10);
      for(var j=0; j<defs.ADC_NUM; j++){
        pstr += (data.power[label][j] + data.power3[label][j]).toFixed(2) + ',';
      }

      // output data
      if( defs.childrenOutputs[i] === 1){
        pstr +=  'on' + ',';
      }else{
        pstr +=  'off' + ',';
      }
    }
    pstr += '\r\n';

    fs.appendFileSync(fname, pstr);

    /*
      append sum and max tag
    */
    if(flag24 === true){
      csvAddSum(fname);
    }
  },