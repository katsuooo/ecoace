calc30min : function(jdata){
    //console.log(jdata);
    //console.log(jdata[100]);
    var num;	// power data num;
    var power1_sum = new Array();
    var power3_sum = new Array();    
    var hai = new Array();
    for(var j=0; j<serverConfig.ADC_NUM; j++){
      hai.push(0.0); 
    }
    for(var i=0; i<serverConfig.CHILD_NUM; i++){
      power1_sum.push(hai.slice(0));	// 8child * 4chan data , deep copy
      power3_sum.push(hai.slice(0));	// 8child * 4chan data , deep copy      
    }
    var avr_num = 0;    // 1min data num (only power data exist) 
    jdata.forEach(function(d){	// root
      if ('childs' in d){
        var isPower = false;
        d.childs.forEach(function(dd){	           //childs loop
          if ('power' in dd){
            isPower = true;
            var no = parseInt(dd.no) - 1;					// child no
            for(var j=0; j<serverConfig.ADC_NUM; j++){	// adc chan loop
              if(pw_params[no].fai[j] == 1){
                power1_sum[no][j] += dd.power[j];
                power3_sum[no][j] = 0;
              }else{
                power1_sum[no][j] = 0;
                power3_sum[no][j] += dd.power[j];               
              }
            }
          }
        });
        if(isPower){
          avr_num++;						// data num count with existing check
        }
      }
    });
    //console.log('ps',power_sum);
    var power1_avr = {};
    var power3_avr = {};    
    for(i=0; i<serverConfig.CHILD_NUM; i++){
      var hai1 = [];
      var hai3 = [];      
      for(j=0; j<serverConfig.ADC_NUM; j++){
        //hai.push(power1_sum[i][j] / avr_num * convKWH);	// power_sum / dnum * hour
        hai1.push(power1_sum[i][j] / avr_num );	// power_sum / dnum (kw)
        hai3.push(power3_sum[i][j] / avr_num );	// power_sum / dnum (kw)        
      }
      var keyname = 'c' + (i+1).toString(10);
      power1_avr[keyname] = hai1.slice(0);
      power3_avr[keyname] = hai3.slice(0);      
    }
    powers = {};
    powers.power1 = power1_avr;
    powers.power3 = power3_avr;  
    return JSON.parse(JSON.stringify(powers))
  },