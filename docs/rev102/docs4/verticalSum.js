

var serverLogsCalc = {
    /*
      csv vertical sums
  
      param  : lines,   csv text data
               sums ,   sum result return
               maxIndex,3fai power maxval index 
                calcParams.data = lines;
                calcParams.sums = sums;
                calcParams.maxIndex = maxIndex;
    */
    verticalSum : function(calcParams) {
      //test
      //calcTest.genLines(lines);
      var maxVal = 0.0;
      var maxIndex = 0;
      var lines = calcParams.data;
      var sums = [];
      for(var i=0; i<34; i++){
        sums.push(0.0);
      } 
      lines.forEach((line, index) => {
      var d = line.split(',');
      var start = 5;
      var x = parseFloat(d[5]);
      x = parseFloat(d[start]);
      sums[0] += parseFloat(d[start]);      // child1
      sums[1] += parseFloat(d[start + 1]);
      sums[2] += parseFloat(d[start + 2]);
      sums[3] += parseFloat(d[start + 3]);
  
      start += 5;
      sums[4] += parseFloat(d[start]);      // child2
      sums[5] += parseFloat(d[start + 1]);
      sums[6] += parseFloat(d[start + 2]);
      sums[7] += parseFloat(d[start + 3]);
  
      start += 5;
      sums[8] += parseFloat(d[start]);      // child3
      sums[9] += parseFloat(d[start + 1]);
      sums[10] += parseFloat(d[start + 2]);
      sums[11] += parseFloat(d[start + 3]);
  
      start += 5;
      sums[12] += parseFloat(d[start]);      // child4
      sums[13] += parseFloat(d[start + 1]);
      sums[14] += parseFloat(d[start + 2]);
      sums[15] += parseFloat(d[start + 3]);
  
      start += 5;
      sums[16] += parseFloat(d[start]);      // child5
      sums[17] += parseFloat(d[start + 1]);
      sums[18] += parseFloat(d[start + 2]);
      sums[19] += parseFloat(d[start + 3]);
  
      start += 5;
      sums[20] += parseFloat(d[start]);      // child6
      sums[21] += parseFloat(d[start + 1]);
      sums[22] += parseFloat(d[start + 2]);
      sums[23] += parseFloat(d[start + 3]);
  
      start += 5;
      sums[24] += parseFloat(d[start]);      // child7
      sums[25] += parseFloat(d[start + 1]);
      sums[26] += parseFloat(d[start + 2]);
      sums[27] += parseFloat(d[start + 3]);
  
      start += 5;
      sums[28] += parseFloat(d[start]);      // child8
      sums[29] += parseFloat(d[start + 1]);
      sums[30] += parseFloat(d[start + 2]);
      sums[31] += parseFloat(d[start + 3]);
  
      // co2, power
      sums[32] += parseFloat(d[1]);  // power sum
      sums[33] += parseFloat(d[2]);  // power3 sum    
      //sums[35] += parseFloat(d[3]);  // co2 sum
      if(maxVal < parseFloat(d[2])){
        maxVal = parseFloat(d[2]);
        maxIndex = index;
      }
    });
    //sums[32] += sums[32] * 0.5;  // power1 sum >>> kwh
    //sums[33] += sums[33] * 0.5;  // power3 sum >>> kwh
    for( var i=0; i<sums.length; i++){
      sums[i] = sums[i] * 0.5;            // all sum value * 0.5
    };
    calcParams.sums.length = 0;
    calcParams.sums = calcParams.sums.concat(sums);
    calcParams.maxIndex = maxIndex;
    //console.log(calcParams);
    }
  };  // endof class
  
  