/*
 servertoMongo.create30minPower()

 これに計測エラー処理を追加。
 measErrDetection/powerWithErrCalc
 に分解移植

*/



/*
 30min save data create

 param  : powerData = {power1fai, power3fai}
*/

function create30minPower(powerData){
    var flag24 = false;   // 24:00 indicator
    var d = JSON.parse(JSON.stringify(power30minData));
    var timestr = moment().format().split('+');  // [2016-11-30T11:02:07, 9:00]
    // check 0:00
    var checkTime = moment();
      //test
      //  checkTime.hours(0);
      //  checkTime.minutes(0);
    if((checkTime.hours() === 0)&&(checkTime.minutes() === 0)){
      flag24 = true;
      checkTime.subtract(1, 'days');
      timestr = checkTime.format().split('+');
      //console.log(timestr, typeof(timestr));
      timestr[0] = timestr[0].replace('00:00', '24:00');
      d.time = timestr[0];
    }else{
      d.time = timestr[0];
    }
    d.power_sum_30m = calcPower.get30minPowerSum(powerData.power1);
    d.power3_sum_30m = calcPower.get30minPowerSum(powerData.power3);
    //d.co2_sum_30m = calcPower.getCo2(powerData);
    d.co2_sum_30m = '';         // only sum calculate
    d.power = JSON.parse(JSON.stringify(powerData.power1));
    d.power3 = JSON.parse(JSON.stringify(powerData.power3));  
    d.output = serverConfig.childrenOutputs;
  
    //d.powerWithErr = measErrDetectionMain.calc();       // 計測エラー処理
  
    serverLogs.csv30min(JSON.parse(JSON.stringify(d)), flag24);		// logging
  
  
  
    var str = timestr[0].split('T');
    var colName = 'power_' + str[0];
    //console.log(colName, d.time);
  
  
    MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
      if(err){
        console.log(err);
        return console.error(err);
      }
      var collection = db.collection(colName);
      collection.insert(d, function(err, docs){
        if (err) {
          console.log(err);
              return console.error(err);
        };
        db.close();
        events.ev.emit('add30minPower');
        deleteCacheIn30sequence();
          //db.close();
      });
    });
  
  }
  