180829
30minの計測データ保存
計測エラーを評価する。
servertoMongo/read30min()
 cache1minデータで直前30min間のデータを集計、保存していたが、ここに計測エラー評価を追加する。
　read30min末部
          /*  original
          var avrPower = calcPower.calc30min(docs);
          create30minPower(avrPower);
          */
         　measErrDetectionMain.calcSavePer30min(docs);   // 計測エラーを評価する
  に変更する。
measErrDetectionMain/calcSavePer30min()の処理







180828
変更箇所

計測エラー検出 (30分毎, wcon != 2 をカウントする)
servertoMonto/read30min() cache1minを全読み後のcallback
>>> measErrDetectionMain.count()
    wcon != 2 をカウント
    serverConfig.wconCount // wcon != 2 カウント値配列
    serverConfig.measErr　 // measErr 計測エラーをセット


このセット後に、
create30minPower(avrPower);　avrPowerはcalc30minPower()での1,3faiへのデータ分離&アベレージングの結果
でavrPowerから個別センサ値を集計して
  d.power_sum_30m = calcPower.get30minPowerSum(powerData.power1);
  d.power3_sum_30m = calcPower.get30minPowerSum(powerData.power3);
  //d.co2_sum_30m = calcPower.getCo2(powerData);
  d.co2_sum_30m = '';         // only sum calculate
  d.power = JSON.parse(JSON.stringify(powerData.power1));
  d.power3 = JSON.parse(JSON.stringify(powerData.power3));  
  d.output = serverConfig.childrenOutputs;
の値をpower計測値として保存している。
これを残し、エラー判別


かきずらい。
構成をかえる。
データは現データも残すが、制御を書き直す。