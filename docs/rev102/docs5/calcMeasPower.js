/*
   calc meas power
   sum power1fai & power3fai >>> return  hai[chan1 .. chan4]

   param  : doc (30min json)
   return : power sum array[4]

*/
function calcMeasPower(doc, chanLabel){
    var fpower = powerFormat(doc.power[chanLabel]);
    var fpower3 = powerFormat(doc.power3[chanLabel]);
    var sums = [4];
    for (var i=0; i<4; i++){
      sums[i] = fpower[i] + fpower3[i];
    }
    console.log('sums', sums);
    return sums;
  }