/*
 test用にソケットを使わないモンゴインターフェース
*/

var mongodb = require('mongodb'), MongoClient = mongodb.MongoClient;
var serverConfig = require('../serverConfig');
var fs = require('fs');
var moment = require('moment');
var powerWithErrMongo = require('../measErrDetection/powerWithErrMongo');
var events = require('../ecoEvents');

/*
 gen collection name
*/
function genColName(){
    //var colName = powerWithErrMongo.getColName();    
    var colName = 'power_' + moment().format().split('T')[0];
    return colName;
}
/*
 30min保存レコード数
*/
var recNum = 0;
var recCounter = 0;
/*
 30min data save
 param : colName モンゴコレクション名
         d       30minデータ
*/
function save30minToMongo(d, dindex){
    var colName = genColName();
    //console.log(colName, dindex);
    //var d = powerWithErrMongo.get30minJson();
    MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
      if(err){
        console.log(err);
        return console.error(err);
      }
      var collection = db.collection(colName);
      collection.insert(d, function(err, docs){
        if (err) {
          console.log(err);
          return console.error(err);
        };
        db.close();
        //console.log('dbs:',dindex);
        if(recNum === ++recCounter){
            events.ev.emit('testMongoStoreEnd');
        }
      });
    });  
}
/*
 drop collection
*/
function dropCollection(){
    var colName = genColName();
    console.log(colName);
    MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
      if(err){
        return console.error('del error:', err);
      }
      var collection = db.collection(colName);
      collection.drop(function(err, docs){
        if (err) {
          return console.error('del error2', err);
        };
        db.close();
        events.ev.emit('testMongoDrop');
      });
    });
}
  
/*
 dummy data 追加コマンド
*/
function addOne(d){
    var colName = genColName();
    MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
      if(err){
        console.log(err);
        return console.error(err);
      }
      var collection = db.collection(colName);
      collection.insert(d, function(err, docs){
        if (err) {
          console.log(err);
          return console.error(err);
        };
        db.close();
        events.ev.emit('testMongoAddOne');
      });
    });  
}
/*
*/
var mongoForTest = {
    save30min: (d, dindex) => {
        save30minToMongo(d, dindex);
    },
    dropCollection: () => {
        dropCollection();
    },
    addOne: (d) => {
        addOne(d);
    },
    setNum: (x) => {
        recNum = x;
        recCounter = 0;
    }
}

//console.log();
module.exports = mongoForTest;