/*
 test39minMongoから
 mongoからeventをもらって非同期制御
*/
/*
 gen 30min json array
*/
var d30 = require('./json30min/power30minWithErr.json');
var moment = require('moment');

/*

 data作成部

*/

/*
 gen 30min json array
*/
var d30 = require('./json30min/power30minWithErr.json');
var moment = require('moment');
//console.log(d30);

/*
日付をとりかえる。 input: 2016-11-30T11:02:07 (string)
*/
function changeDay(timed){
    var xdate = timed.split('T')[1];     // date以外の部分
    var newdate = moment().format().split('T')[0];  // 今のdate
    timed = newdate + 'T' + xdate;
    //console.log(newdate, timed);
    return timed;
};
//var teststr = moment().format();
//console.log(teststr);
var timestr = moment().format().split('+');  // [2016-11-30T11:02:07, 9:00]
//console.log(timestr);
var datestr = timestr[0].split('T')[0];
console.log(datestr);
var newh = [];
d30.forEach( (j) => {
    var newd = JSON.parse(JSON.stringify(j));
    newd.time = changeDay(newd.time);
    newd.powerWithErrWithoutDemand.view.time = changeDay(newd.powerWithErrWithoutDemand.view.time);
    newh.push(newd);
});
//日付変更のチェック（２か所）
newh.forEach( (j) => {
    console.log(j.time);
    console.log('view_time', j.powerWithErrWithoutDemand.view.time);
});
/*
 整形
*/
//console.log(newh[0]);
/*
 haskey調べて、_idのレコードを消す
*/
newh2 = []
newh.forEach( (j) => {
    if('_id' in j){
        delete j._id;
    }
    newh2.push(j);
});
console.log('gen json end');


/*
 collection の delete
 以降はイベントハンドラでmongoforTestと通信
*/
var mongoForTest = require('./mongoForTest');
//mongoForTest.dropCollection();
mongoForTest.addOne({a: 1});      // dummy data 

/*
 event handler
*/
var events = require('../ecoEvents');

/*
 collectionドロップ前にデータを一個入れる。
 collectionがないとエラーとなるので、エラー防止用のダミーデータ追加終了のイベント
*/
events.ev.on('testMongoAddOne', () => {
    console.log('mongo add one event on');
    mongoForTest.dropCollection();      // collectionをdropする
});
/*
 collection追加前にdrop処理終了のイベント >>> コレクションを順にインサートする
*/
function stored(){
    newh2.forEach((d, index) => {
        mongoForTest.save30min(d, index);
    });
}
events.ev.on('testMongoDrop', () => {
    console.log('mongo drop event on');
    mongoForTest.setNum(newh2.length);
    stored();
});


/*
 データストア終了イベント >>>　実動作

*/
var servertoMongo = require('../servertoMongo');
var measErrDetectionMain = require('../measErrDetection/measErrDetectionMain');
// cache1min
var testCache1min = require('./testCache1min');
var docs = testCache1min.get();

/*
first try
*/
/*
events.ev.on('testMongoStoreEnd', () => {
    console.log('mongo store_end event on');
    //servertoMongo.test();       //refresh30mins();　下コメント
    //servertoMongo/read30min()内部から
    // 1min全読みデータが必要
    measErrDetectionMain.calcSavePer30min(docs);   // 計測エラーを評価する
    servertoMongo.test()    //.save30minToMongo();    
});
*/
/*
 servertoMongo/refresh30min()からだと
 powerWithErrMongo.getColName()で0:00のcolNameとなる。
 
 どこからいけばいいか？
 servertoMongo/save30minToMongo()
   powerWithMongoのデータを使う
 servertoMongo/read30min()
   cache1minを全読みし、データが存在する場合、
          measErrDetectionMain.calcSavePer30min(docs);   // 計測エラーを評価する
          save30minToMongo();
          とつづく。

*/
/*
save30minToMongo()の後
SyntaxError: Unexpected token u in JSON at position 0
発生箇所を探す
save30minToMongo() insert ok
deleteCacheIn30sequence() 30min処理中でcache1minデータをremove >>> ok
save1minPrefetch() 古い1minデータを全消去後、新30minの最初の1minデータを保存
 >>> ここでエラー発生 引数なしで、power1minを collection cache1minに保存。
 power1minデータをセットできていない。
デバッグノートからこのデータをとってきてセットするインターフェースをつける。


*/



/*
 second try
*/
/*
var cached = require('./cache1min.json');
events.ev.on('testMongoStoreEnd', () => {
    console.log('mongo store_end event on');
    servertoMongo.testSetPower1min(cached);
    measErrDetectionMain.calcSavePer30min(docs);   // 計測エラーを評価する
    servertoMongo.save30minToMongo()    //.save30minToMongo();    
});
*/
/*
 csvファイルfooterなしはできた。が、時刻がおかしい。
 おかしくない。cache1minは0-29minのデータなのでタイムデータはつかえない。
 で、momentから作っている。
 だからこれでOKか。
 現在時を操作していないので、最後のデータが現在時になる。
 2400の制御を追加する。
*/

/*
 try3
 現在時をテスト用に制御して、2400フラグを制御する。
 最終データの時刻制御は行わない。関係ないはず。

 check24AndCsvEnding(docs)ないでpowerWithErrTime.getFlag24()で24時確認
 test後にキャッシュ1minが保存されている。
 次のテストに必要？
 */
var cached = require('./cache1min.json');
events.ev.on('testMongoStoreEnd', () => {
    console.log('mongo store_end event on');
    servertoMongo.testSetPower1min(cached);
    measErrDetectionMain.calcSavePer30min(docs);   // 計測エラーを評価する
    servertoMongo.save30minToMongo()    //.save30minToMongo();    
});
/*
時間文字列の生成
powerWithErrJson
powerWithErrTime
の２か所ある。
csv作成時はcsvTimeでpowerWithErrJson.getTime()から-30minタイム文字列を作成
    getTime: () => {
        return power30min.powerWithErrWithoutDemand.view.time;
    },
???
view.timeは　30min前の時間では？？？

2400フラグは
powerWithErrTimeにあるのだが、どこからたたいているのか。

// 計測開始時刻 (現在時-30minの時間)を取
powerWithErrTime/
function genStartTime(){
    var sTime = moment(genTime());    
    var sTimestr = sTime.subtract(30, 'minute').format().split('+');
    return sTimestr[0];
}
自モジュール内からたたいている。
powerWithErrView/genView()からgetStartTime()をたたいている。
これで2400フラグもセットされる。
powerWithErrTime/genTime()に
    console.log('pweTime.genTime:', checkTime.format(), 'timestr', timestr, '24flag:', flag24);
    //test setTime()
    checkTime.hour(0);
    checkTime.minutes(0); 
を追記してテスト。
csvファイルが
power_2018-10-15.csv
power_invalid date.csv >>> 中のレコードの日付もInvalid date
の２つができる。
0:00に時間を設定しなければ、正常な日付レコードになる。

モーメントでスタートタイム生成時に、現在時を文字列にした原料をつかう。
0:00のとき、24:00に変換している。
この値がモーメントの範囲外でinvalid dataになっている。
なるほど。

*/
