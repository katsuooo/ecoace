/*
 csv gen
*/


var testCache1min = require('./testCache1min');
var d = testCache1min.get();
/*
 csvParamの確認
*/
var measErrDetectionMain = require('../measErrDetection/measErrDetectionMain');
measErrDetectionMain.calcSavePer30min(d);

var powerWithErrMongo = require('../measErrDetection/powerWithErrMongo');
var d = powerWithErrMongo.get30minJson();

/*
console.log(d);
console.log(d.powerWithErrWithoutDemand.view.power);
*/

/*
 csv params のモニタ
*/
/*
var csvParams = require('../measErrDetection/csvParams');
csvParams.print();
*/


/*
 powerデータのシムからcsv allの作成
*/


var power = require('./power.json');
var powerh = [];
powerh.push(power);

var csvGenAllMain = require('../measErrDetection/csvGenAllMain');
csvGenAllMain.genAll(powerh);

//console.log(d.powerWithErrWithoutDemand.view.power3_sum_30m);
