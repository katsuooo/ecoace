/*
 csv test
*/


/*
 data load
*/
var testCache1min = require('./testCache1min');
var d = testCache1min.get();
var measErrDetectionMain = require('../measErrDetection/measErrDetectionMain');
//measErrDetectionMain.calcSavePer30min(d);   // 計測エラーを評価する
/*
calcSavePer30min()のloggingの手前を実行
*/
var serverConfig = require('../serverConfig');
var powerWithErr = require('../measErrDetection/powerWithErr');
var calcPower = require('../calcPower');

measErrDetectionMain.MeasErrCountAndJudge(d);      // 計測エラーの検出
var avrPower = calcPower.calc30min(d);   // power 1,3fai 分離, アベレージング
powerWithErr.noErrPower(avrPower);                // 計測エラーを評価しない電力集計(旧型式データの作成)
powerWithErr.gen30minPowers();                    // 計測エラー判別追加 
//powerWithErr.logging();                           // csv 30min append これだけはじく



/*
var csvFile = require('../measErrDetection/csvFile');
csvFile.genFile();
*/
var powerWithErrTime = require('../measErrDetection/powerWithErrTime');
powerWithErrTime.testSetFlag24();


var csvMain = require('../measErrDetection/csvMain');
csvMain.gen30min();