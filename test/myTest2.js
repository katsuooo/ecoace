/*
 cache1minデータを用意し、severtoMongo/read30min()の後をテストする


 servertoMongo / save1minPrefetch()でエラーで止まるが、powerデータは格納される
*/

var testCache1min = require('./testCache1min');
var d = testCache1min.get();
//console.log(d);

var measErrDetectionMain = require('../measErrDetection/measErrDetectionMain');
measErrDetectionMain.calcSavePer30min(d);   // 計測エラーを評価する

/*
power値
*/
var powerWithErrJson = require('../measErrDetection/powerWithErrJson');
var jd = powerWithErrJson.getAll();
//console.log(jd.powerWithErrWithoutDemand);
console.log(jd.powerWithErrWithoutDemand.view.power);
console.log(jd.powerWithErrWithoutDemand.calc.power);
console.log(jd.powerWithErrWithoutDemand.calc.power3);

/*
 計測エラーモニタ
*/
var serverConfig = require('../serverConfig');
console.log('err', serverConfig.measErr);
console.log('count', serverConfig.wconCount);
