/*
 csv footer 作成

 30min時の処理をエミュレート

 cache1minデータ用意(dummy)
 30minデータ処理
 >>> flag24を強制セット
 新30minデータの保存
   >>> 全30minデータ読み出し
        >>> viewへの処理
        >>> 24時check
            >>> 集計してcsvに追記 
*/



/*
 cache1mins (dummy)
*/
var testCache1min = require('./testCache1min');
var docs = testCache1min.get();

//30minデータ処理
var measErrDetectionMain = require('../measErrDetection/measErrDetectionMain');
measErrDetectionMain.calcSavePer30min(docs);   // 計測エラーを評価する

/*
for-debug
*/
//test flag24 true
var powerWithErrTime = require('../measErrDetection/powerWithErrTime');
powerWithErrTime.testSetFlag24();       // flag24 set


//save30minToMongo();
var servertoMongo = require('../servertoMongo');
servertoMongo.test();       // save30minToMongo

/*
 prefetchの1minデータがなくてエラーになるが、
 genfooterまでは実行できてる。
*/