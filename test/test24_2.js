/*
 servertoMongo/read30min() sim

 cache1min (dummy)
 measErrDetectionMain.calcSavePer30min()
 save30minToMongo();
*/


var testCache1min = require('./testCache1min');
var docs = testCache1min.get();

var measErrDetectionMain = require('../measErrDetection/measErrDetectionMain');
measErrDetectionMain.calcSavePer30min(docs);   // 計測エラーを評価する

/*
for-debug
*/
//test flag24 true
var powerWithErrTime = require('../measErrDetection/powerWithErrTime');
powerWithErrTime.testSetFlag24();       // flag24 set


//save30minToMongo();
var servertoMongo = require('../servertoMongo');
servertoMongo.test();       // save30minToMongo


/*
 servertoMongo/save1minPrefetch()でエラーがでる。
 seqtoGoでとってたやつ。

 これもdummyをつくるか、、、
 check24AndCsvEnding()の作成のため。
 ここまではきていた。

*/

