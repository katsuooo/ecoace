/*
 2400 csvテスト
*/


/*
*/

var testCache1min = require('./testCache1min');
var d = testCache1min.get();
//console.log(d);

var measErrDetectionMain = require('../measErrDetection/measErrDetectionMain');
//measErrDetectionMain.calcSavePer30min(d);   // 計測エラーを評価する






/*
var servertoMongo = require('../servertoMongo');

console.log(servertoMongo);
servertoMongo.test();
*/

/*
log
*/
//powerWithErrLogs.csv30min()

/*
 24h flag test set
*/
//var powerWithErrTime = require('../measErrDetection/powerWithErrTime');

/*
measErrDetectionMain.calcSavePer30min()をぜんぶ走らせると、
flag24が途中でたてられない。

バラではしらせて、途中でフラグをたてる。
*/

var cache1mins = d;
var powerWithErr = require('../measErrDetection/powerWithErr');
var calcPower = require('../calcPower');

//calcSavePer30min
measErrDetectionMain.MeasErrCountAndJudge(cache1mins);      // 計測エラーの検出
var avrPower = calcPower.calc30min(cache1mins);   // power 1,3fai 分離, アベレージング
powerWithErr.noErrPower(avrPower);                // 計測エラーを評価しない電力集計(旧型式データの作成)
powerWithErr.gen30minPowers();                    // 計測エラー判別追加 

//test flag24 true
var powerWithErrTime = require('../measErrDetection/powerWithErrTime');
powerWithErrTime.testSetFlag24();       // flag24 set

powerWithErr.logging();                           // csv 30min append

/*
 loggingはアペンドのみ。
*/