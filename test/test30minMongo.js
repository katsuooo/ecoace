/*
 30minデータをモンゴに流し、2400動作をテストする

 23:30までのデータをいれて、2400のデータをパースするところから走らせる
 powerWithErrMongo.getColName()あたりがどうなるのか？？？
 mongoのデータは2400まできれいにはいっているが。
*/

/*
 gen 30min json array
*/
var d30 = require('./json30min/power30minWithErr.json');
var moment = require('moment');
//console.log(d30);

/*
日付をとりかえる。 input: 2016-11-30T11:02:07 (string)
*/
function changeDay(timed){
    var xdate = timed.split('T')[1];     // date以外の部分
    var newdate = moment().format().split('T')[0];  // 今のdate
    timed = newdate + 'T' + xdate;
    //console.log(newdate, timed);
    return timed;
};
//var teststr = moment().format();
//console.log(teststr);
var timestr = moment().format().split('+');  // [2016-11-30T11:02:07, 9:00]
//console.log(timestr);
var datestr = timestr[0].split('T')[0];
console.log(datestr);
var newh = [];
d30.forEach( (j) => {
    var newd = JSON.parse(JSON.stringify(j));
    newd.time = changeDay(newd.time);
    newd.powerWithErrWithoutDemand.view.time = changeDay(newd.powerWithErrWithoutDemand.view.time);
    newh.push(newd);
});
//日付変更のチェック（２か所）
newh.forEach( (j) => {
    //console.log(j.time);
    //console.log(j.powerWithErrWithoutDemand.view.time);
});
/*
 整形
*/
//console.log(newh[0]);
/*
 haskey調べて、_idのレコードを消す
*/
newh2 = []
newh.forEach( (j) => {
    if('_id' in j){
        //console.log('ari');
        delete j._id;
    }
    newh2.push(j);
});
/*
 mongo 書き込み
*/
var mongoForTest = require('./mongoForTest');


//mongoForTest.save30min(newh2[0]);
/*
while(mongoForTest.readBusy()){
    //console.log('busy');
}
*/
newh2.forEach((d, index) => {
    console.log(index);
    mongoForTest.save30min(d, index);
});

console.log('input end');

/*
 events
    var events = require('./ecoEvents');

    events.ev.emit('configChangeEvent');  // >>>> servertoMongo


    events.ev.on('refreshLatest', (powerData) => {
        console.log('mongoif getEmmit!!!!');
        io.emit('r_readLatest', powerData);
    });



    // ecoEvents.js
    var eventEmitter = require('events').EventEmitter;
    class ecoEvents_proto{

  constructor(){
    this.ev = new eventEmitter;     // latest socketio chuukei
    this.configEv = new eventEmitter;
  }
}

*/


var events = require('../ecoEvents');

events.ev.on('testMongoDrop', () => {
    console.log('evon, testMongoDrop')
});




