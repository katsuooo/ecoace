/*
 旧システムデータが残っている状態で稼働させた場合。
 30minバルクデータ処理時にpowerWithErrWithoutDemandデータがないエラーが起こる。
 この状況をシミュレートする。

 test30minMongoEv.jsをベースにinputファイルをmix jsonにする。
*/

/*

 data作成部

*/

/*
 gen 30min json array
*/
// powerWithErrWithoutDemandのみのデータ
//var d30 = require('./json30min/power30minWithErr.json');
// powerWithErrWithoutDemand有り無し混在のデータ
var d30 = require('./json30min/mix.json');
var moment = require('moment');


/*
日付をとりかえる。 input: 2016-11-30T11:02:07 (string)
*/
function changeDay(timed){
    var xdate = timed.split('T')[1];     // date以外の部分
    var newdate = moment().format().split('T')[0];  // 今のdate
    timed = newdate + 'T' + xdate;
    //console.log(newdate, timed);
    return timed;
};
//var teststr = moment().format();
//console.log(teststr);
var timestr = moment().format().split('+');  // [2016-11-30T11:02:07, 9:00]
//console.log(timestr);
var datestr = timestr[0].split('T')[0];
console.log(datestr);
var newh = [];
d30.forEach( (j) => {
    var newd = JSON.parse(JSON.stringify(j));
    newd.time = changeDay(newd.time);
    // データにpowerWithErrWithoutDemandがないものを混ぜたので存在チェック
    if('powerWithErrWithoutDemand' in newd){
        newd.powerWithErrWithoutDemand.view.time = changeDay(newd.powerWithErrWithoutDemand.view.time);
    }
    newh.push(newd);
});
//日付変更のチェック（２か所）
newh.forEach( (j) => {
    console.log(j.time);
    if('powerWithErrWithoutDemand' in j){
        console.log('view_time', j.powerWithErrWithoutDemand.view.time);
    }
});
/*
 整形
*/
//console.log(newh[0]);
/*
 haskey調べて、_idのレコードを消す
*/
newh2 = []
newh.forEach( (j) => {
    if('_id' in j){
        delete j._id;
    }
    newh2.push(j);
});
console.log('gen json end');


/*
 collection の delete
 以降はイベントハンドラでmongoforTestと通信
*/
var mongoForTest = require('./mongoForTest');
//mongoForTest.dropCollection();
mongoForTest.addOne({a: 1});      // dummy data 

/*
 event handler
*/
var events = require('../ecoEvents');

/*
 collectionドロップ前にデータを一個入れる。
 collectionがないとエラーとなるので、エラー防止用のダミーデータ追加終了のイベント
*/
events.ev.on('testMongoAddOne', () => {
    console.log('mongo add one event on');
    mongoForTest.dropCollection();      // collectionをdropする
});
/*
 collection追加前にdrop処理終了のイベント >>> コレクションを順にインサートする
*/
function stored(){
    newh2.forEach((d, index) => {
        mongoForTest.save30min(d, index);
    });
}
events.ev.on('testMongoDrop', () => {
    console.log('mongo drop event on');
    mongoForTest.setNum(newh2.length);
    stored();
});


/*
 データストア終了イベント >>>　実動作

*/
var servertoMongo = require('../servertoMongo');
var measErrDetectionMain = require('../measErrDetection/measErrDetectionMain');
// cache1min
var testCache1min = require('./testCache1min');
var docs = testCache1min.get();

var cached = require('./cache1min.json');
events.ev.on('testMongoStoreEnd', () => {
    console.log('mongo store_end event on');
    servertoMongo.testSetPower1min(cached);
    measErrDetectionMain.calcSavePer30min(docs);   // 計測エラーを評価する
    servertoMongo.save30minToMongo()    //.save30minToMongo();    
});


/*
 2400 をまたぐテスト

 mix json処理されるのは2400越えのタイミングでcsv allを作成するとき。
 measErrDetection/powerWithErrTime/set24Flag()
 で時刻を0:00に強制する。
*/


/*
 変更前エラー
 measErrDetection/csvGenBody(12)でエラーがでる。
 getMaxIndex()で引数docs(d30min配列)のforLoop内。
 頭ではじいてみる。
*/