
/*
160529 kdesign
 uart control

serialport : 115200 8 1 n
winreg     : get com port No
*/


var sp = require('serialport');
var Registry = require('winreg')
,   regKey = new Registry({                        // new operator is optional
      hive: Registry.HKLM,                        // open registry hive HKEY_CURRENT_USER
      key:  '\\HARDWARE\\DEVICEMAP\\SERIALCOMM\\' // open key containing autostart programs
    })
var moment = require('moment');
var servertoMongo = require('./servertoMongo');
var serverConfig = require('./serverConfig');
var events = require('./ecoEvents');
var uartTest = require('./uartTest');



var portName = 'COM1';   // comport name (devault)
//var comExist = false; // change serverConfig.uartAvailable

/*
 uart recieve data
*/
var recieve_buffer = Buffer.alloc(0);
console.log('rb',recieve_buffer);

/*
 * koki No    1
 * time stump 2
 * chuukei    1
 * denatu     2
 * ondo       1
 * ry output  1
 * adc        2,2,2,2 >>> 8
 * crc        2
 *
 * total 18 byte
 */
//var recieve_dnum = 18;  // command r
var recieve_dnum = 24;      // command c response length

var commandChildNo = 1;     // recieve sequence control val

var seqSelector = 1;        // 1min or 30min selector, call MongoSequence


//var serial = new sp.serialPort();   // serial port
/*
 load config to parent

*/
var configtoParent = false;     // loading complete flag


/*
 serial info set
*/

function setSerial(){
  serial = new sp.SerialPort(portName, {
    baudRate: 115200,
    dataBits: 8,
    parity:   'none',
    stopBits: 1,
    flowControl: false,
  });
}

/*
 crc shifter
*/
function shifter_crc(crc){
  for(var i=0; i<8; i++){
    if(crc & 0x01){
      crc = crc >> 1;
      crc = crc ^ 0xa001;
    }else{
      crc = crc >> 1;
    }
  }
  return crc;
}
/*
 gen crc
*/
function genCrc(data, crcnum){
	var crc = 0xffff;
	for(var i=0; i<crcnum; i++){
		crc = crc ^ data[i];
		crc = shifter_crc(crc);
	}
  return crc;
}
/*
 set crc
*/
function setCrc(command){
  var crc = genCrc(command, command.length);
  var bcrc = Buffer.alloc(2);
  bcrc.writeUInt16BE(crc, 0);
  return Buffer.concat([command, bcrc]);
}
/*
 check crc
*/
function checkCrc(dlength){
  var crc = recieve_buffer.readUInt16BE(dlength-2);  // recieve data lest 2
  var calc = genCrc(recieve_buffer, dlength-2);    // re-calc rdata
  console.log('gen_crc', calc, crc);
  if(calc !== crc){
    return false;
  }
  return true;
}

/*
  シリアルNoから子機Noを得る

  param  : child serial no
  return : child sno index
*/

function getChildNo(sNo){
  //var childSno = servertoMongo.getChildSno();
  var childSno = serverConfig.parentConfig.childSerials;
  for(var i=0; i<serverConfig.CHILD_NUM; i++){
    if(childSno[i] === sNo) {
      return i;
    }
  }
  return -1;
}




/*
 fetch child read data

  set jsondata for childs from uart recieve data

*/
/*
var childLatist = {
  time: '',
  childs:[
   {no:'1', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'2', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'3', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'4', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'5', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'6', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'7', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'8', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'}
  ]
};
*/
var childLatist = {
  time: '',
  childs:[
   {no:'1', serial:'80000001', timestump:'0', chuukei:'0', power:'3.000', temp:'25', adc:['0','0','0','0'], ryMon:'0', lqi:'0', output:'0', wcon:'0'},
   {no:'2', serial:'80000002', timestump:'0', chuukei:'0', power:'3.000', temp:'25', adc:['0','0','0','0'], ryMon:'0', lqi:'0', output:'0', wcon:'0'},
   {no:'3', serial:'80000003', timestump:'0', chuukei:'0', power:'3.000', temp:'25', adc:['0','0','0','0'], ryMon:'0', lqi:'0', output:'0', wcon:'0'},
   {no:'4', serial:'80000004', timestump:'0', chuukei:'0', power:'3.000', temp:'25', adc:['0','0','0','0'], ryMon:'0', lqi:'0', output:'0', wcon:'0'},
   {no:'5', serial:'80000005', timestump:'0', chuukei:'0', power:'3.000', temp:'25', adc:['0','0','0','0'], ryMon:'0', lqi:'0', output:'0', wcon:'0'},
   {no:'6', serial:'80000006', timestump:'0', chuukei:'0', power:'3.000', temp:'25', adc:['0','0','0','0'], ryMon:'0', lqi:'0', output:'0', wcon:'0'},
   {no:'7', serial:'80000007', timestump:'0', chuukei:'0', power:'3.000', temp:'25', adc:['0','0','0','0'], ryMon:'0', lqi:'0', output:'0', wcon:'0'},
   {no:'8', serial:'80000008', timestump:'0', chuukei:'0', power:'3.000', temp:'25', adc:['0','0','0','0'], ryMon:'0', lqi:'0', output:'0', wcon:'0'}
  ]
};

/*
function fetchChildData(){
  console.log(recieve_buffer);
  if(checkCrc() === false){
    return; // data no refresh
  }

  console.log('crc ok');

  var sno = recieve_buffer.readUInt32BE(0);   // 0-3
  var tsno = sno.toString(10);
  var addzeroNum = 7 - tsno.length;
  for(var i=0; i<addzeroNum; i++){
    tsno = '0' + tsno;
  }
  //console.log('tsno_str', tsno);
  var childNo = getChildNo(tsno);   // シリアルNoから子機Noを得る
　　console.log(tsno, childNo);
  var adc1 = recieve_buffer.readUInt16BE(4).toString(16);  // 4-5
  var adc2 = recieve_buffer.readUInt16BE(6).toString(16);  // 6-7
  var adc3 = recieve_buffer.readUInt16BE(8).toString(16);  // 8-9
  var adc4 = recieve_buffer.readUInt16BE(10).toString(16);  // 10-11
  var output = recieve_buffer.readUInt8(12).toString(10); // 12
  if(output == 1){
    output = 'on';
  }else{
    output = 'off';
  }
  var rssi = recieve_buffer.readUInt8(13).toString(10); // 13
  var channel = recieve_buffer.readUInt8(14).toString(10); // 14
  childLatist.childs.forEach(function(child){
    if(child.no === childNo){
      child.adc = [adc1, adc2, adc3, adc4];
      child.output = output;
      child.rssi = rssi;
      child.channels = channel;
    }
  });
  console.log('childList', childLatist.childs[0]);


}
*/

/*
 fetch child uart rx data


 param  : none
 return : none
*/

/*
// r command
function fetchChildData(){
  console.log(recieve_buffer);
  if(checkCrc() === false){
    return; // data no refresh
  }

  console.log('crc ok');

  //var sno = recieve_buffer.readUInt32BE(0);   // 0-3
  //var tsno = sno.toString(10);
  var sno = recieve_buffer.readUInt8(0);   // childIndex
  var childNo = (sno+1).toString(10);     // chald no string
  console.log(sno, childNo);

  //var addzeroNum = 7 - tsno.length;
  //for(var i=0; i<addzeroNum; i++){
  //    tsno = '0' + tsno;
  //}

  //console.log('tsno_str', tsno);
  //var childNo = getChildNo(tsno);   // シリアルNoから子機Noを得る
　　//console.log(tsno, childNo);
  var adc1 = recieve_buffer.readUInt16BE(4).toString(16);  // 4-5
  var adc2 = recieve_buffer.readUInt16BE(6).toString(16);  // 6-7
  var adc3 = recieve_buffer.readUInt16BE(8).toString(16);  // 8-9
  var adc4 = recieve_buffer.readUInt16BE(10).toString(16);  // 10-11
  var output = recieve_buffer.readUInt8(12).toString(10); // 12
  if(output == 1){
    output = 'on';
  }else{
    output = 'off';
  }
  var lqi = recieve_buffer.readUInt8(13).toString(10); // 13
  var channel = recieve_buffer.readUInt8(14).toString(10); // 14

  //childLatist.childs.forEach(function(child){
  //  if(child.no === childNo){
  //    child.adc = [adc1, adc2, adc3, adc4];
  //    child.output = output;
  //    child.rssi = rssi;
  //    child.channels = channel;
  //  }
  //});

//console.log(childLatist.childs[0], sno);
  //console.log(childLatist.childs[sno][adc]);
  childLatist.childs[sno].adc = [adc1, adc2, adc3, adc4];
  childLatist.childs[sno].output = output;
  childLatist.childs[sno].lqi = lqi;
  childLatist.childs[sno].channel = channel;

  console.log('childList', childLatist.childs[sno]);
}
*/



// c command version

//---------------------------------
// parent child data format
//----------------------------------
// this type size 24
/*
typedef struct{
 uint32 serial;		// 4 serial no
 uint16 timeStump;	// 2 time stump
 uint8  chuukei;		// 1 chuukei
 uint8  dummy1;		// 1 dummy
 uint16 power;		// 2 power vol (*1000)
 int8  temp;			// 1 temparature
 uint8  dummy2;		// 1 dummy
 uint16 adc[4];		// 8 adcs
 uint8  ryMonitor;	// 1 ry monitor input
 uint8  lqi;			// 1 lqi
 uint8  crc[2];		// 2 ry monitor input
} _childRx;				//24
*/

function fetchChildData(){
  // test
  //recieve_buffer = uartTest.dummyCcommandResponse();

  console.log(recieve_buffer);
  if(checkCrc(recieve_dnum) === false){
    return; // data no refresh
  }

  console.log('crc ok');

  var sno = recieve_buffer.readUInt32BE(0);   // 0-3
  var childNo = getChildNo(sno);   // シリアルNoから子機Noを得る
　　console.log(childNo);
//childNo = 0;    // test
  if(childNo === -1) return;        // serial not match my list
  var tsno = sno.toString(16);  // 4-5
  var timeStump = recieve_buffer.readUInt16BE(4).toString(16);  // 4-5
  var chuukei = recieve_buffer.readUInt8(6).toString(16);       // 6
                                                                // 7 dummy
  var power = (recieve_buffer.readUInt16BE(8)/1000).toFixed(3);      // 8-9
  var temp = recieve_buffer.readInt8(10).toString(10);          // 10
                                                                // 11 dummy

  var adc1 = recieve_buffer.readUInt16BE(12).toString(16);  // 12-13
  var adc2 = recieve_buffer.readUInt16BE(14).toString(16);  // 14-15
  var adc3 = recieve_buffer.readUInt16BE(16).toString(16);  // 16-17
  var adc4 = recieve_buffer.readUInt16BE(18).toString(16);  // 18-19

// test
/*
  var adc1 = '6c14';
  var adc2 = '5680';
  var adc3 = '409b';
  var adc4 = '2ae8';
*/
  //^^^^^^^^

  var ryMonitor = recieve_buffer.readUInt8(20).toString(16);  // 20
  /*
  var output = recieve_buffer.readUInt8(12).toString(10); // 12
  if(output == 1){
    output = 'on';
  }else{
    output = 'off';
  }
  */
  var lqi = recieve_buffer.readUInt8(21).toString(10); // 21
  childLatist.childs[childNo].serial = tsno;
  childLatist.childs[childNo].timestump = timeStump;
  childLatist.childs[childNo].chuukei = chuukei;
  childLatist.childs[childNo].power = power;
  childLatist.childs[childNo].temp = temp;
  childLatist.childs[childNo].adc = [adc1, adc2, adc3, adc4];
  childLatist.childs[childNo].ryMon = ryMonitor;
  childLatist.childs[childNo].lqi = lqi;

  childLatist.childs[childNo].output = serverConfig.childrenOutputs[childNo];     // add output states
  console.log('childList', childLatist.childs[childNo]);
}





















/*
 uart command mode

  0: c command (read children data)
  1: s command (set config data)
  2: o command (set output)

*/
var uart_mode = 0;    // default c commandd



    /*
     get com No
    */
var uartOpenWait = false;
function getComNo(){
  if(uartOpenWait === true) return;
      regKey.values(function (err, items) {  // items =  array of RegistryItem
        if (err)
          console.log('ERROR: '+err);
        else {
          for (var i=0; i<items.length; i++) {
            //console.log('ITEM: '+items[i].name+'\t'+items[i].type+'\t'+items[i].value);
            //console.log('ITEM2: '+items[i].hive+'\t'+items[i].key+'\t'+items[i].arch);

            if((items[i].name === '\\Device\\VCP0') &&
               (items[i].type === 'REG_SZ')){
              console.log('comNo:', items[i].value);
              portName = items[i].value;
              uartOpenWait = true;
              serverConfig.uartAvailable = true;
            }
          }
          if(serverConfig.uartAvailable === false){
            console.log('no modules');
          }else{
            setSerial();
            serial.on('open', function(err){
              if(err){
                console.log('uart_open_error', err);
                return;
              }
              console.log('serial_open');
              uartOpenWait = false;
              servertoMongo.setUartState();
            });
            /*
             serial recieve
            */
            serial.on('data',function(data){
              //console.log('serial_receive:', data.toString('utf-8'));
              //console.log(data[0].toString(16), data[1].toString(16));

              //var totallen = data.length + recieve_buffer.length;
              recieve_buffer = Buffer.concat([recieve_buffer, data]);

              /*
                s command return waitting ???
              */
              if(uart_mode === 1){
                if(recieve_buffer.length < 10){
                  // not complete
                  console.log(recieve_buffer);
                  return;
                }
                console.log('s command response', recieve_buffer);
                if(checkCrc(10) === false){
                  recieve_buffer = Buffer.alloc(0);   // clear buffer
                  return; // data no refresh
                }
                console.log('s response crc ok!!!!!!!!!!');
                // serial no
                var parentSno = recieve_buffer.readUInt32BE(0);
                //console.log(parentSno.toString(16));
                if(serverConfig.parentConfig.parentSerialNo !== parentSno){
                  serverConfig.parentConfig.parentSerialNo = parentSno;
                  parentSno -= 0x80000000;          // format change
                  console.log(parentSno.toString(16));
                  servertoMongo.setParentSerialNo(parentSno.toString(16));
                }
                // application id
                var appid = recieve_buffer.readUInt32BE(4);
                if(serverConfig.parentConfig.appId !== appid){
                  console.log('appid', appid.toString(16), serverConfig.parentConfig.appId);
                  console.log(typeof(servertoMongo.setAppId));
                  servertoMongo.setAppId(appid.toString(16));
                }
                uart_mode = 0;      // c command mode
                recieve_buffer = Buffer.alloc(0);   // clear buffer
                configtoParent = true;    // config load OK!
                return;
              }else if(uart_mode === 2){      // o command
                if(recieve_buffer.length < 3){
                  // not complete
                  console.log(recieve_buffer);
                  return;
                }
                if(checkCrc(3) === false){
                  recieve_buffer = Buffer.alloc(0);   // clear buffer
                  return; // data no refresh
                }
                console.log(recieve_buffer);
                uart_mode = 0;                      // c command mode
                recieve_buffer = Buffer.alloc(0);   // clear buffer
                getAdcs();                          // get adc start
                return;
              }
              /*
                c command response wait
              */
              if(recieve_buffer.length < recieve_dnum){
                // not complete
                console.log(recieve_buffer);
                return;
              }else{
                var sss = '';
                for(var i=0; i<recieve_buffer.length; i++){
                  sss += recieve_buffer[i].toString(16);
                }
                console.log(sss, recieve_buffer.length, recieve_dnum);

                fetchChildData();     // rdata save
                recieve_buffer = Buffer.alloc(0);   // clear buffer
                if(commandChildNo < 8){
                  commandChildNo += 1;
                  getChildFromParent();     // re-start
                }else if(commandChildNo === 8){
                  //console.log(childLatist);
                  var timestr = moment().format().split('+');
                  childLatist.time = timestr[0];
                  var djson = JSON.parse(JSON.stringify(childLatist));
                  servertoMongo.seqGo(seqSelector, djson);
                }
              }

            });
          }
    }
  }); // end of resistoryKeyVal
}

/*
  uart write go


  param  : command
  return : none
*/
function uartWriteGo(command){
  if(serverConfig.uartAvailable === false || uartOpenWait === true) return;
  serial.write(command, function(err){
    if(err){
      console.log('serial_write', err);
      serverConfig.uartAvailable = false;
      serial.close(function(err){
        console.log('comClose', err);
      });
      servertoMongo.setUartState();
      //getComNo();
      //return;
    }else{
      serial.drain(function(){
        console.log('serial_drain');
      });
    }
  });
}

/*
 read child datas

 serialNo vales:[child1_sno, .... to child8]
*/

/*
 command r + serial No
/*
var childSno = {
  no1: '0000001',
  no2: '0000002',
  no3: '0000003',
  no4: '0000004',
  no5: '0000005',
  no6: '0000006',
  no7: '0000007',
  no8: '0000008',
};
function getChildFromParent(){
  if(comExist === false){
    getComNo();
    if(comExist === false){
      return;
    }
  }

  uartBusy = true;    // wait next flag
  var sno = 'no' + commandChildNo.toString(10);
  var command_str = 'r' + childSno[sno];
  var command = Buffer.from(command_str);
  command = setCrc(command);  //add crc
  //console.log(command);
  //console.log('command',command);
  serial.write(command, function(){
    console.log('serial_write');
    serial.drain(function(){
      console.log('serial_drain');
    });
  });
}
*/
/*
 command r + childNo
*/

/*
// r command
function getChildFromParent(){
  if(comExist === false){
    getComNo();
    if(comExist === false){
      return;
    }
  }

  uartBusy = true;    // wait next flag

  var command_str = 'rx';     // x is dummy for child no
  var command = Buffer.from(command_str);
  command[1] = commandChildNo;    // x -> child no
  console.log('command', command);
  command = setCrc(command);  //add crc
  //console.log(command);
  //console.log('command',command);
  serial.write(command, function(){
    console.log('serial_write');
    serial.drain(function(){
      console.log('serial_drain');
    });
  });
}
*/

// c command

function getChildFromParent(){
  if(serverConfig.uartAvailable === false){
    getComNo();
    if(serverConfig.uartAvailable === false){
      return;
    }
  }

  uartBusy = true;    // wait next flag

  var command_str = 'cx';     // x is dummy for child no
  var command = Buffer.from(command_str);
  command[1] = commandChildNo;    // x -> child no
  console.log('command', command);
  command = setCrc(command);  //add crc
  //console.log(command);
  //console.log('command',command);
  /*
  serial.write(command, function(){
    console.log('serial_write');
    serial.drain(function(){
      console.log('serial_drain');
    });
  });
  */
  uartWriteGo(command);
}



/*
  load config data to parent


   parent data (child configs)

  typedef struct{
  	uint32 cSerialNo[CHILD_NUM];	// 4+8=32
  	uint8 txIntervalTime[CHILD_NUM];	// 8
    uint8 fchannel;
  } _childs;

 command s

 's'              : 1
 child serial     : 4 * 8 = 32
 child txinterval : 2 * 8 = 16
 fchannel         : 1



  param  : none
  return : none
*/
function LoadConfig(){
  if(serverConfig.uartAvailable === false){
    getComNo();
    /*
    if(comExist === false){
      return;
    }
    */
    return;   // serial open wait 
  }


  uartBusy = true;    // wait next flag
  uart_mode = 1;
  var command_str = 's';    // s command
  var command = Buffer.from(command_str);
  // child serial no list
  //console.log('intervals',serverConfig.parentConfig.childTxIntervals);
  serverConfig.parentConfig.childSerials.forEach( (sno) => {
    //console.log('loadConfig',sno.toString(16));
    var bsno = Buffer.alloc(4);
    bsno.writeUInt32BE(sno, 0);
    command = Buffer.concat([command, bsno]);
  });
  // child txinterval list
  //console.log('intervals',serverConfig.parentConfig.childTxIntervals);
  serverConfig.parentConfig.childTxIntervals.forEach( (interval) => {
    //console.log('loadConfig',sno.toString(16));
    var binterval = Buffer.alloc(2);
    binterval.writeUInt16BE(interval, 0);
    command = Buffer.concat([command, binterval]);
  });
  //console.log('loadConfig', command);
  var bchannel = Buffer.alloc(1);
  bchannel.writeUInt8(serverConfig.parentConfig.fchannel, 0);
  command = Buffer.concat([command, bchannel]);
  command = setCrc(command);  //add crc
  //configtoParent = true;    // config load OK!
  uartWriteGo(command);
}




/*
  get adcs

   start child data read sequence


  param  : none
  return : none
*/
function getAdcs(){
  commandChildNo = 1;
  getChildFromParent();
}



/*
 outputs timer check

  param  : ena, childrenTimerEna
           index, child no  0-7
  return : none
*/
function checkOuterTimer(index, nowTime){
  //var startTime = moment();
  //var stopTime = moment();
  var startTime = nowTime.clone();
  var stopTime = nowTime.clone();
  var start = serverConfig.childrenTimer[index][0].split(':');
  startTime.hour(start[0]);
  startTime.minute(start[1]);
  var stop = serverConfig.childrenTimer[index][1].split(':');
  stopTime.hour(stop[0]);
  stopTime.minute(stop[1]);
  console.log(nowTime.format(), startTime.format(), stopTime.format());
  if(startTime.isSame(stopTime)){
    serverConfig.childrenOutputs[index] = 0;
  }
  else if(startTime.isBefore(stopTime)){      // start time < stop time
    if( startTime.isSameOrBefore(nowTime) && nowTime.isBefore(stopTime) ){
      serverConfig.childrenOutputs[index] = 1;
    }else{
      serverConfig.childrenOutputs[index] = 0;
    }
  }else{   // start Time > stop Time
    //if( nowTime.isSameOrBefore(startTime) || stopTime.isBefore(nowTime)){
    if( startTime.isSameOrBefore(nowTime) || nowTime.isBefore(stopTime)){
      serverConfig.childrenOutputs[index] = 1;
    }else{
      serverConfig.childrenOutputs[index] = 0;
    }
  }
}



/*
  check outputs

    output timer setting check


    param  : none
    return : none

*/
function checkOutputs(){

  var nowTime = moment();     // get now time

/*
  serverConfig.childrenTimerEna.forEach( (ena, index) => {
    checkAboutTimerEna(ena, index, nowTime);
  });
  */
  /*
    child registered check
  */

  serverConfig.childrenRegistered.forEach( (reg, index) => {

    if(reg === 1){  // child registered
      /*
        outer control enable check
      */
      if(serverConfig.childrenOuterRegistered[index] === 1){

        /*
          timer control registered check
        */
        if(serverConfig.childrenTimerEna[index] === 1){
          checkOuterTimer(index, nowTime);
        }else{                                        // not timer control
          serverConfig.childrenOutputs[index] = 1;    // outpout always on
        }
      }else{                                        // outer not registered
        serverConfig.childrenOutputs[index] = 0;    // outpout off
      }
    }else{          // not child registered
      serverConfig.childrenOutputs[index] = 0;      // outpout off
    }
  });
  //console.log('checkOutput', nowTime, serverConfig.childrenOutputs);
}

/*
  load outputs to parent

  param  : none
  reutrn : none
*/
function LoadOutputs(){
  /*
  if(serverConfig.uartAvailable === false){
    getComNo();
    if(serverConfig.uartAvailable === false){
      serverConfig.uartAvailable = false; 
      return;
    }
  }
  */
  serverConfig.uartAvailable = true;  
  uartBusy = true;    // wait next flag
  uart_mode = 2;      // o command
  var command_str = 'o';    // o command
  var command = Buffer.from(command_str);
  // child serial no list
  //console.log('intervals',serverConfig.parentConfig.childTxIntervals);
  serverConfig.childrenOutputs.forEach( (outd) => {
    //console.log('loadConfig',sno.toString(16));
    var bout = Buffer.alloc(1);
    bout.writeUInt8(outd, 0);
    command = Buffer.concat([command, bout]);
  });
  command = setCrc(command);  //add crc
  //configtoParent = true;    // config load OK!
  uartWriteGo(command);
}




/*
 monoStick interface class
*/
class monoInterface{
/*
  initiarize
*/
  constructor(){
    getComNo();
  }
  /*
   uart restart
  */
  uartRestart(){
    configtoParent = false;  // confiration load to parent flag reset
    getComNo();    
  }
/*
  get Adc values
*/
/*
  getAdcs(sequenceSelect){
    seqSelector = sequenceSelect;   // 1min or 5min selector
    commandChildNo = 1;
    getChildFromParent();

    //getChildFromParent(2);
  }
*/
  /*
   load complete ???
  */
  getConfigtoParent(){
    return configtoParent;
  }
  /*
    reset confign Loading flag
  */
  resetConfigtoParent(){
    configtoParent = false;
  }

  /*
    set config
  */
  setConfig(){
    LoadConfig();       // config data write to parent

    //console.log(serverConfig.parentConfig);
  }

  /*
    set output

    param  : wequenceSelect  >>> 1min or 30min ???
    return : none
  */
  setOutputs(sequenceSelect){
      seqSelector = sequenceSelect;   // 1min or 5min selector
      /*
        testMode check (alternative day demand control available)
      */
      if(serverConfig.dousaMode === 'test'){
        if(serverConfig.demandCtrl === false){
          serverConfig.childrenOutputs = [0,0,0,0,0,0,0,0];    // outpout off
        }else{
          serverConfig.childrenOutputs = [1,1,1,1,1,1,1,1];    // outpout on
        }
      }else{    // normal mode
        checkOutputs();
      }
      LoadOutputs();
  }
}

var uart = new monoInterface();

module.exports = uart;
