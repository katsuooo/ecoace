﻿/*
160528 kdesign

  server logging functions

*/
/*
history
160907 change csv title line >>> to english

*/

require("babel-core").transform("code");	// use babel

var fs = require('fs');
var moment = require('moment');
var defs = require('./serverConfig');
var serverLogsCalc = require('./serverLogsCalc');
var calcpower = require('./calcPower');

var FILE_DIR = './ecoaceLogs/';
var FILE_DIR_CSV = './ecoaceCsv/';
var FILE_LOG1MIN = 'd1min_';
var FILE_LOG30MIN = 'd30min_';
var FILE_CSV30MIN = 'power_';


// babel test
/*
var evens = [2,4,6,8];
var odds = evens.map(v => v + 1);
var nums = evens.map((v, i) => v+i);
console.log(odds, nums);
*/

/*
 csv dump

param : json data
return: none

json format
{
  "time":"2016-05-21T15:30:00",
  "childs":[
    {
       "no":"1","output":"on",
       "rssi":"32","channels":"18",
       "power":[2.84423828125,2.84423828125,26.8438720703125,39.842529296875]
    },
    {"no":"2","output":"on","rssi":"32","channels":"18","power":[2.84423828125,2.84423828125,26.8438720703125,39.842529296875]},
    {"no":"3","output":"on","rssi":"32","channels":"18","power":[2.84423828125,2.84423828125,26.8438720703125,39.842529296875]},
    {"no":"4","output":"on","rssi":"32","channels":"18","power":[2.84423828125,2.84423828125,26.8438720703125,39.842529296875]},
    {"no":"5","output":"on","rssi":"32","channels":"18","power":[2.84423828125,2.84423828125,26.8438720703125,39.842529296875]},
    {"no":"6","output":"on","rssi":"32","channels":"18","power":[2.84423828125,2.84423828125,26.8438720703125,39.842529296875]},
    {"no":"7","output":"on","rssi":"32","channels":"18","power":[2.84423828125,2.84423828125,26.8438720703125,39.842529296875]},
    {"no":"8","output":"on","rssi":"32","channels":"18","power":[2.84423828125,2.84423828125,26.8438720703125,39.842529296875]}
  ]
}

*/

/*
 round decimal val and tostring

 param　: digit  >>> 小数点以下桁数
         num    >>> number
 return: string val　　　　　　
*/
function rounding(digit, num){
  var multiply = Math.pow(10, digit);
  var rounded = Math.round(num * multiply);
  var str = rounded.toString();
  var istr = str.slice(0, -1 * digit);           // int part
  var dstr = str.slice(-1 * digit, str.length);  // decimal part
  return istr + '.' + dstr;
}

//test
//console.log('round',rounding(2,39.842529296875));


/*
 24:00 csv edit
  add sums
  add max marking

  param: file name
  return: none
*/

function csvAddSum(fname){
  var text;
  var sums = [];
  for(var i=0; i<(4*8+2); i++){
    sums.push(0);
  }
  //console.log(sums);
  text = fs.readFileSync(fname, 'utf8');
  var lines = text.split('\r\n');
  lines.shift();    // first line remove
  lines.pop();      // remove eof
  var calcParams = {};
  calcParams.data = lines;
  calcParams.sums = sums;
  calcParams.maxIndex = 0;
  serverLogsCalc.verticalSum(calcParams);    // calc verticul sum
  console.log('ret:', calcParams);
  /*
    add max
  */
  lines = text.split('\r\n');
  lines.pop();      // remove eof
  //lines[maxIndex+1] += 'MAX' ;    // 1 = add title
  var maxLine = lines[calcParams.maxIndex + 1].split(',');
  maxLine[3] = 'MAX/DAY';
  var newLine = maxLine.join(',');
  lines[calcParams.maxIndex + 1] = newLine;
  /*
    add empty one line over total
  */
  lines.push(',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,');
  /*
    add sums 1fai  [32] power1 [33]power3
  */
  var sumLine = '1pTOTAL(kwh),';
  //sumLine += rounding(2, calcParams.sums[32]) + ',';
  sumLine += calcParams.sums[32].toFixed(2) + ',';
  sumLine += ',';     // 3fai total
  sumLine += ',';     // 3fai max
  //sumLine += rounding(2, calcpower.calcCo2(calcParams.sums[32], 1)) + ','
  sumLine += calcpower.calcCo2(calcParams.sums[32], 1).toFixed(2) + ',';
  for(var child=0; child<8; child++){
    var offset = 4*child;
    for(var i=offset; i<(offset+4); i++){
      if(calcpower.getPowerFai(child, i%4) === 1){
        //sumLine += rounding(2, calcParams.sums[i]);
        sumLine += calcParams.sums[i].toFixed(2);
      }
      sumLine += ',';
    }
    sumLine += ',';    // output line
  }
  lines.push(sumLine);
  /*
    add sums 3fai  [32] power1 [33]power3
  */
  var sumLine3 = '3pTOTAL(kwh),';
  sumLine3 += ',';     // 3fai total
  //sumLine3 += rounding(2, calcParams.sums[33]) + ',';
  sumLine3 += calcParams.sums[33].toFixed(2) + ',';
  sumLine3 += ',';     // 3fai max
  //sumLine3 += rounding(2, calcpower.calcCo2(calcParams.sums[33], 1)) + ',';
  sumLine3 += calcpower.calcCo2(calcParams.sums[33], 1).toFixed(2) + ','
  for(var child=0; child<8; child++){
    var offset = 4*child;
    for(var i=offset; i<(offset+4); i++){
      if(calcpower.getPowerFai(child, i%4) === 3){
        //sumLine3 += rounding(2, calcParams.sums[i]);
        sumLine3 += calcParams.sums[i].toFixed(2);
      }
      sumLine3 += ',';
    }
    sumLine3 += ',';    // output line
  }
  lines.push(sumLine3);
  //console.log(sumLine);
  var newText = '';
  lines.forEach(line => {
    newText += line + '\r\n';
  });
  console.log(lines);
  fs.writeFileSync(fname, newText);
}

/*


  serverlog class



*/
var serverLogs = {
/*
 imin log dump
*/
  log1min: function(data){
    var date = moment().format('YYYY-MM-DD');
    if(!fs.existsSync(FILE_DIR)){
      fs.mkdirSync(FILE_DIR);
    }

    var fname = FILE_DIR + FILE_LOG1MIN + date + '.log';
    fs.appendFileSync(fname, data + '\r\n');
  },
/*
 30min log dump
*/
  log30min: function(data){
    var date = moment().format('YYYY-MM-DD');

    if(!fs.existsSync(FILE_DIR)){
      fs.mkdirSync(FILE_DIR);
    }

    var fname = FILE_DIR + FILE_LOG30MIN + date + '.log';
    fs.appendFileSync(fname, data + '\r\n');
  },
/*
 30min csv dump

param : data >>> json power data
        flag24 >>> 24h csv end data
return: none

json format
{
    "_id" : ObjectId("57441810af599dac215d96b3"),
    "time" : "2016-05-24T18:00:00",
    "power_sum_30m" : 289.49951171875,
    "co2_sum_30m" : 91.0475964355469,
    "power" : {
        "c1" : [
            1.422119140625,
            1.422119140625,
            13.4219360351563,
            19.9212646484375
        ],
        ... to "c8"
    },
    "output" : {
        "c1" : "on",
        "c2" : "on",
        "c3" : "on",
        "c4" : "on",
        "c5" : "on",
        "c6" : "on",
        "c7" : "on",
        "c8" : "on"
    }
}

csv titles:
子1_CH1,子1_CH2,子1_CH3,子1_CH4,子1_出力, .... 2 to 8 same, 電力量, CO2量 (42item)
*/
  csv30min: (data, flag24) => {
    var date = data.time.split('T')[0];
    if(!fs.existsSync(FILE_DIR_CSV)){
      fs.mkdirSync(FILE_DIR_CSV);
    }
    var fname = FILE_DIR_CSV + FILE_CSV30MIN + date + '.csv';
    var isCsv = false;
    try {
      fs.accessSync(fname, fs.F_OK);
      isCsv = true;
    } catch (e) {
      isCsv = false;
    }

    var titles = '';
    titles += 'Day,';
    titles += '1pTOTAL(kw),';
    titles += '3pTOTAL(kw),';
    titles += '3pMAX/DAY,'; 
    titles += 'TOTAL(CO2),';

    for (var i=0; i<8; i++){
      titles += 'RE0' + (i+1).toString(10) + '_CH1,';
      titles += 'RE0' + (i+1).toString(10) + '_CH2,';
      titles += 'RE0' + (i+1).toString(10) + '_CH3,';
      titles += 'RE0' + (i+1).toString(10) + '_CH4,';
      titles += 'RE0' + (i+1).toString(10) + '_Output,';
    }
    titles = titles.substr(0, titles.length-1);
    titles += '\r\n';  
    // titles end


    if(isCsv === false){  // new file
      fs.appendFileSync(fname, titles);
    }
    /*
     sum line1 fai1
    */
    var pstr = '';    // filing data
    pstr += data.time.substr(0, data.time.length-3) + ',';  // time / cut second
    //pstr += rounding(2, data.power_sum_30m) + ',';        // 1fai power sum    
    var powerFloat = parseFloat(data.power_sum_30m);
    var roundedPower = powerFloat.toFixed(2);
    pstr += roundedPower + ',';
    //pstr += rounding(2, data.power3_sum_30m) + ',';        // 3fai power sum
    powerFloat = parseFloat(data.power3_sum_30m);
    roundedPower = powerFloat.toFixed(2);
    pstr += roundedPower + ',';
    pstr += ',';                                          // add max culumn    
    pstr += '-,';                                         // co2 sum is '-'   
    for(var i=0; i<defs.CHILD_NUM; i++){
      var label = 'c' + (i+1).toString(10);
      for(var j=0; j<defs.ADC_NUM; j++){
        pstr += (data.power[label][j] + data.power3[label][j]).toFixed(2) + ',';
      }

      // output data
      if( defs.childrenOutputs[i] === 1){
        pstr +=  'on' + ',';
      }else{
        pstr +=  'off' + ',';
      }
    }
    pstr += '\r\n';

    fs.appendFileSync(fname, pstr);

    /*
      append sum and max tag
    */
    if(flag24 === true){
      csvAddSum(fname);
    }
  },
  
  /*
    lqi logs

    param  : data
    return : none
  */
  lqiLogs: (jdata) => {
    var lqiStr;
    console.log('Lqis:');
    jdata.childs.forEach( (child) => {
      console.log('child'+child.no, child.lqi);
    });
  }
};




module.exports = serverLogs;
