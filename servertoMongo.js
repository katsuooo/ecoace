﻿/*
160518 kdesign

  mongodb interface

*/

require('babel-core').transform('code');	// use babel



//var MONGO_URL = 'mongodb://localhost:27017/myTest';
var mongodb = require('mongodb'), MongoClient = mongodb.MongoClient;
var calcPower = require('./calcPower');
var moment = require('moment');
var serverLogs = require('./serverLogs');
var fs = require('fs');
var serverConfig = require('./serverConfig');		// system defines
var events = require('./ecoEvents')
var childConnection = require('./childConnection');
//var uart = require('./uart');
//var configSys = require('./defaultData/sysConfig.json');


var measErrDetectionMain = require('./measErrDetection/measErrDetectionMain');
var powerWithErrMongo = require('./measErrDetection/powerWithErrMongo');
var powerWithErrTime = require('./measErrDetection/powerWithErrTime');
var csvGenAll = require('./measErrDetection/csvGenAllMain');

console.log('server-to-mongo');

/*
 defines

*/

var SYSCONFIG_DEFAULT_FILE = './defaultData/sysConfig.json';




/*
 server functions
*/

var power1min;		// 1min power data


/*
 add power 30minData , power3, power3_sum_30m
*/
var power30minData = {
  'time':'0:00',
  'power_sum_30m':'1000.00',
  'power3_sum_30m':'1000.00',  
  'co2_sum_30m': '900.00',
  'power': {
    'c1':[10.01,10.02,10.03,10.04],
    'c2':[10.01,10.02,10.03,10.04],
    'c3':[10.01,10.02,10.03,10.04],
    'c4':[10.01,10.02,10.03,10.04],
    'c5':[10.01,10.02,10.03,10.04],
    'c6':[10.01,10.02,10.03,10.04],
    'c7':[10.01,10.02,10.03,10.04],
    'c8':[10.01,10.02,10.03,10.04]
  },
  'power3': {
    'c1':[10.01,10.02,10.03,10.04],
    'c2':[10.01,10.02,10.03,10.04],
    'c3':[10.01,10.02,10.03,10.04],
    'c4':[10.01,10.02,10.03,10.04],
    'c5':[10.01,10.02,10.03,10.04],
    'c6':[10.01,10.02,10.03,10.04],
    'c7':[10.01,10.02,10.03,10.04],
    'c8':[10.01,10.02,10.03,10.04]
  },
  //'output': {'c1': 'on','c2': 'on','c3': 'on','c4': 'on','c5': 'on','c6': 'on','c7': 'on','c8': 'on'}
  'output' : [0,0,0,0,0,0,0,0]
};

/*
  deleat cache1min
*/
function deleteCache1min(){
  MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
    if(err){
      return console.error(err);
    }
    var collection = db.collection('cache1min');
    collection.remove(function(err, docs){
      if (err) {
	  	  return console.error(err);
      };
    	db.close();
    });
  });
}




/*
 initialize
*/
deleteCache1min();		// cache clear
var defFile = fs.readFileSync( SYSCONFIG_DEFAULT_FILE, 'utf-8');
var configDefault = JSON.parse(defFile.toString().trim());
//console.log(configDefault);



/******************************************
 Latist data save

   1min, 30min seq both last work

   save sata is same latist 1min data
*****************************************************/
function saveLatest(powerData){
  //console.log(powerData);
  MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
    if(err){
      return console.error(err);
    }
    var collection = db.collection('Latest');
    collection.update({}, {$set:powerData}, {upsert: true}, function(err, docs){   // upsert
      if (err) {
	  	  return console.error(err);
      };
    	db.close();
     //io.emit('newCussche_set');
      console.log('fire!!!');
      events.ev.emit('refreshLatest', powerData);
    });
  });
}


/******************************************************
 1min sequence


***************************************************/
/*
 power 1min data save
*/
function save1minPower(powerData){
  MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
    if(err){
      return console.error(err);
    }
    var collection = db.collection('cache1min');
    collection.insert(JSON.parse(JSON.stringify(powerData)), function(err, docs){
      if (err) {
	  	  return console.error(err);
      };
    	db.close();
      saveLatest(powerData);
    });
  });
}

/*********************************************************************

 30min squence

read parent & set 1min data (not save)
read cache 1mins
create30minPower
deleteCacheIn30sequence
save1minPrefetch
******************************************/
/*
 save 1min pre fetch data
*/
function save1minPrefetch(){
  MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
    if(err){
      return console.error(err);
    }
    var collection = db.collection('cache1min');
    console.log('save1min start');
    collection.insert(JSON.parse(JSON.stringify(power1min)), function(err, docs){
      if (err) {
	  	  return console.error(err);
      };
      db.close();
      console.log('save1min ok');
      saveLatest(power1min);
    });
  });
}

/*
  deleat cache1min
*/
function deleteCacheIn30sequence(){
  MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
    if(err){
      return console.error(err);
    }
    var collection = db.collection('cache1min');
    collection.remove(function(err, docs){
      if (err) {
	  	  return console.error(err);
      };
      db.close();
      save1minPrefetch(); // save 1min data
    });
  });
}



/*
 30min save data create

 param  : powerData = {power1fai, power3fai}
*/
function create30minPower(powerData){
  var flag24 = false;   // 24:00 indicator
  var d = JSON.parse(JSON.stringify(power30minData));
  var timestr = moment().format().split('+');  // 2016-11-30T11:02:07
  // check 0:00
  var checkTime = moment();
    //test
    //  checkTime.hours(0);
    //  checkTime.minutes(0);
  if((checkTime.hours() === 0)&&(checkTime.minutes() === 0)){
    flag24 = true;
    checkTime.subtract(1, 'days');
    timestr = checkTime.format().split('+');
    //console.log(timestr, typeof(timestr));
    timestr[0] = timestr[0].replace('00:00', '24:00');
    d.time = timestr[0];
  }else{
    d.time = timestr[0];
  }
  d.power_sum_30m = calcPower.get30minPowerSum(powerData.power1);
  d.power3_sum_30m = calcPower.get30minPowerSum(powerData.power3);
  //d.co2_sum_30m = calcPower.getCo2(powerData);
  d.co2_sum_30m = '';         // only sum calculate
  d.power = JSON.parse(JSON.stringify(powerData.power1));
  d.power3 = JSON.parse(JSON.stringify(powerData.power3));  
  d.output = serverConfig.childrenOutputs;

  //d.powerWithErr = measErrDetectionMain.calc();       // 計測エラー処理

  serverLogs.csv30min(JSON.parse(JSON.stringify(d)), flag24);		// logging



  var str = timestr[0].split('T');
  var colName = 'power_' + str[0];
  //console.log(colName, d.time);


  MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
    if(err){
      console.log(err);
      return console.error(err);
    }
    var collection = db.collection(colName);
    collection.insert(d, function(err, docs){
      if (err) {
        console.log(err);
	  	  return console.error(err);
      };
      db.close();
      events.ev.emit('add30minPower');
      deleteCacheIn30sequence();
    	//db.close();
    });
  });

}


/*
 2400ならcsvのfooter書き込み
 旧データ用もない。
*/
function check24AndCsvEnding(docs){
    if(powerWithErrTime.getFlag24() === false){
        console.log('not 24h');
        return;
    }
    csvGenAll.genAll(JSON.parse(JSON.stringify(docs)));    // 2400 csv集計部書き込み
}


/*
 リフレッシュ 30minデータ
 30minデータを全読み。view更新()
*/
function refresh30mins(){
    /*
    if(daysum > 0) daysum = 0;
    var day = new moment();
    var daystr = day.add(daysum,'days').format("YYYY-MM-DD");
    */
    //var collectionName = 'power_' + daystr;
    var collectionName = powerWithErrMongo.getColName();
    console.log('colName', collectionName);
    MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
        if(err){
            return console.error(err);
        }
        var collection = db.collection(collectionName);
        collection.find({}).toArray(function(err, docs) {
            if (err) {
                return console.error(err);
            };
            db.close();
            //io.emit('r_read30min', docs);
            events.ev.emit('refresh30min', docs);
            check24AndCsvEnding(docs);      // 2400 check
        });
    });
}


/*
 30min data save
 param : colName モンゴコレクション名
         d       30minデータ
*/
function save30minToMongo(){
  var colName = powerWithErrMongo.getColName();
  var d = powerWithErrMongo.get30minJson();
  MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
    if(err){
      console.log(err);
      return console.error(err);
    }
    var collection = db.collection(colName);
    collection.insert(d, function(err, docs){
      if (err) {
        console.log(err);
        return console.error(err);
      };
      db.close();
      // test comment off events
      //events.ev.emit('add30minPower'); >>> refresh30mins()に置き換え
      deleteCacheIn30sequence();
      /*
       ここでcsv 2400時の追記とviewの更新をしてみる。
      */
      refresh30mins();
    });
  });  
}


/*
  30min power read & calc >>> set power data
*/
function read30min(){
    MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
      if(err){
        return console.error(err);
      }
      var collection = db.collection('cache1min');
	    collection.find({}).toArray(function(err, docs){
        if (err) {
			  	return console.error(err);
	      };
      	db.close();
        if(docs.length === 0){
          console.log('no cache data');
          save1minPrefetch();       // save 1min data
        }else{
          /*
           30minデータ処理
           cache1minデータ読み込み完了後のデータ処理に計測エラーの検出を追加
          */
          /*  original 180829
          var avrPower = calcPower.calc30min(docs);
          create30minPower(avrPower);
          */
          measErrDetectionMain.calcSavePer30min(docs);   // 計測エラーを評価する
          save30minToMongo();
        }
      });
    });
}


/*
get configs

  read mongo config datas

  param  : none
  return : none
*/
var configReady = false;
//var childSerials = [0x80000001, 0x80000002, 0x80000003, 0x80000004, 0x80000005, 0x80000006, 0x80000007, 0x80000008];
var childSerials = [0x80000001, 0x80000002, 0x80000003, 0x80000004, 0x80000005, 0x80000006, 0x80000007, 0x81004e47];
function getConfigs(){
  MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
    if(err){
      return console.error(err);
    }
    var collection = db.collection('config');
    collection.find({}).toArray(function(err, docs){
      if (err) {
        return console.error(err);
      };
      db.close();
      if(docs.length === 0){
        console.log('no config data');
      }else{
        //console.log('configs', docs);
        serverConfig.parentConfig.fchannel = parseInt(docs[0].fchannel, 10);
        serverConfig.parentConfig.parentSerialNo = parseInt(docs[0].address, 16) + 0x80000000;
        serverConfig.parentConfig.appId = parseInt(docs[0].appId, 16);
        serverConfig.dousaMode = docs[0].dousaMode;
        serverConfig.demandCtrl = docs[0].demandCtrl;
        docs[0].children.forEach( (child) => {
          //console.log(child.serial_no);
          //console.log(child.child, typeof(child.child));
          var no = parseInt(child.child, 10) - 1;
          if( child.serial_no !== ''){
            //console.log( 'test', parseInt(child.serial_no, 16).toString(16));
            //childSerials[no] = parseInt( child.serial_no, 16) + 0x80000000;   // twe serial NO or 0x80000000
            serverConfig.parentConfig.childSerials[no] = parseInt( child.serial_no, 16) + 0x80000000;   // twe serial NO or 0x80000000
            serverConfig.parentConfig.childTxIntervals[no] = parseInt( child.sleep_time, 10);
          }
          // 子機登録情報 / 室外機制御登録
          if(child.registered === 'yes'){
            serverConfig.childrenRegistered[no] = 1;
            if(child.output.registered === 'yes'){
              serverConfig.childrenOuterRegistered[no] = 1;
              if(child.output.timerEna === 'yes'){
                serverConfig.childrenTimerEna[no] = 1;
              }else{
                serverConfig.childrenTimerEna[no] = 0;
              }
            }else{
              serverConfig.childrenOuterRegistered[no] = 0;
              serverConfig.childrenTimerEna[no] = 0;
            }
          }else{
            serverConfig.childrenRegistered[no] = 0;
            serverConfig.childrenTimerEna[no] = 0;
          }
          serverConfig.childrenTimer[no] = [child.output.start, child.output.stop];
        });
        configReady = true;       // config OK!!!
      }
    });
  });
}




/******************************************
 parent serial No write

  param  : serial No (string)
  return : none
*****************************************************/
function saveParentSerialNo(serialNo){
  //console.log(powerData);
  MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
    if(err){
      return console.error(err);
    }
    var collection = db.collection('config');
    collection.update({}, {$set:{address:serialNo}}, {upsert: true}, function(err, docs){   // upsert
      if (err) {
	  	  return console.error(err);
      };
    	db.close();
     //io.emit('newCussche_set');

      //events.ev.emit('parentSerialRenew');
    });
  });
}




/******************************************
 save app id

  param  : app id (string)
  return : none
*****************************************************/
function saveAppId(appid){
  //console.log(powerData);
  MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
    if(err){
      return console.error(err);
    }
    var collection = db.collection('config');
    collection.update({}, {$set:{appId:appid}}, {upsert: true}, function(err, docs){   // upsert
      if (err) {
	  	  return console.error(err);
      };
    	db.close();
     //io.emit('newCussche_set');
      //events.ev.emit('appIdReNew');
    });
  });
}




/******************************************
 save demandCtrl

  param  : demandcont id (bool)
  return : none
*****************************************************/
function saveDemandCtrl(demandcont){
  MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
    if(err){
      return console.error(err);
    }
    var collection = db.collection('config');
    collection.update({}, {$set:{demandCtrl:demandcont}}, {upsert: true}, function(err, docs){   // upsert
      if (err) {
	  	  return console.error(err);
      };
    	db.close();
      //uart.setOutputs(30);        // same to sequenceGo(30) (server_timer)
    });
  });
}



/*
  deleat last day

     3days ago data deleat

  param  : none
  return : none
*/
function delLastDay(){
  var timestr = moment().add(-3, 'days').format().split('+');     // 3 day ago strings
  var str = timestr[0].split('T');
  var colName = 'power_' + str[0];
  console.log('delete colname', colName);
  MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
    if(err){
      return console.error('del error:', err);
    }
    var collection = db.collection(colName);
    /*
    collection.remove(function(err, docs){
      if (err) {
	  	  return console.error('del error2', err);
      };
      console.log('delNumber:', db);    // param = delete no.
    	db.close();
    });
    */
    collection.drop(function(err, docs){
      if (err) {
        return console.error('del error2', err);
      };
      //console.log('delNumber:', db);    // param = delete no.
      db.close();
    });
    //collection.remove();
  });
}



/*
 save latest only (per10sec parent read)

 param  : none
 return : none
*/
function saveLatestOnly(xjson){
  MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
    if(err){
      return console.error(err);
    }
    //var collection = db.collection('t_settings');
    var collection = db.collection('config');
    collection.find({}).toArray(function(err, docs){
      if (err) {
        console.log(err);
        return console.error(err);
      };
      db.close();
      if(docs.length === 0){	// not exist t_settings
        calcPower.setChildrenParams(JSON.parse(JSON.stringify(configDefault)));		// children's parameter refresh
      }else{
        calcPower.setChildrenParams(docs[0]);	  	// children's parameter refresh
      }
      power1min = calcPower.calcNowPower(xjson);	// set power data to global
      saveLatest(power1min);                      // save 1min data
    });
  });
}

/*
  save uart state
    client gamen data
*/
function saveUartState(){
  MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
    if(err){
      return console.error(err);
    }
    var collection = db.collection('serverState');
    collection.update({}, {$set:{uartState : serverConfig.uartAvailable}}, {upsert: true}, function(err, docs){   // upsert
      if (err) {
	  	  return console.error(err);
      };
    	db.close();
      events.ev.emit('changeUartState', serverConfig.uartAvailable);    // socketio request event
    });
  });
}








/*
 export function proto
*/
class servertoMongo_proto{
  constructor(){
    getConfigs();
  }
  /*
  1min,30min timer exection
  */

/*
 per min sequence start

*/
  seqGo(interval, xjson){
    childConnection.checkWirelessConnection(xjson);       // wireless connection state generate
    // set connection data
    serverConfig.childConnectionState.forEach( (connection, index) => {
      xjson.childs[index].wcon = connection.toString(10);
    });
    // set outputs data
    serverConfig.childrenOutputs.forEach( (output, index) => {
      xjson.childs[index].output = output;
    });

    /*
    not save data per 10sec read
    */
    if(interval === 0){
      console.log('sec read');
      saveLatestOnly(xjson);
      serverLogs.lqiLogs(xjson);                    // lqi console out
      return;
    }

    serverLogs.log1min(JSON.stringify(xjson));		// logging

    serverLogs.lqiLogs(xjson);                    // lqi console out

    MongoClient.connect(serverConfig.MONGO_URL, function(err, db) {
      if(err){
        return console.error(err);
      }
      //var collection = db.collection('t_settings');
      var collection = db.collection('config');
      collection.find({}).toArray(function(err, docs){
        if (err) {
          console.log(err);
          return console.error(err);
        };
        db.close();
        if(docs.length === 0){	// not exist t_settings
          calcPower.setChildrenParams(JSON.parse(JSON.stringify(configDefault)));		// children's parameter refresh
        }else{
          calcPower.setChildrenParams(docs[0]);		// children's parameter refresh
        }
        power1min = calcPower.calcNowPower(xjson);	// set power data to global
        if(interval === 1){  // 1min interval
          console.log('--create 1min--');
          //serverLogs.log1min(JSON.stringify(xjson)); // logging >>> 1st line
          save1minPower(power1min);                 // save 1min data
        }else {              // 30min interval
          console.log('--create 30min--');
          serverLogs.log30min(JSON.stringify(power1min));		// logging
          read30min();
        }
      });
    });
  }

  /*
    get config


  */
  getConfigAgain(){
    getConfigs();
  }



  /*
  config ready check

  param  : none
  return : bool
           true >>> config setting ok
           faulse >>> not yet
  */
  getConfigReady(){
    return configReady;
  }

  /*
    reset confign ready

    param  : none
    return : none
  */
  resetConfigReady(){
    configReady = false;
  }


  /*
    set Parent serial no

    param  : serial no string
    return : none
  */
  setParentSerialNo(sno){
    saveParentSerialNo(sno);
  }

  /*
    set app id


    param  : noen
    return : none
  */
  setAppId(appid){
    saveAppId(appid);
  }

  /*
    set demand ctrol    (for testMode)


    param  : noen
    return : none
  */
  setDemandCtrl(demandcont){
    saveDemandCtrl(demandcont);
  }

  /*
    deleat last days

    param  : none
    return : none
  */
  deleatLastDay(){
    delLastDay();
  }
  /*
    set uart state

    param  : none
    return : none
  */
  setUartState(){
    saveUartState();
  }
  /*
   save 30min data (from powerWithErr)
  */
  /*
  save30minToMongo(colName, d){
    save30minToMongo(colName, d);
  }
  */
  save30minToMongo(){
    save30minToMongo();    
  }
  
  /*
   test
  */
  test(){
    save30minToMongo();
     //refresh30mins();
  }
  testSetPower1min(d){
    power1min = d;
  }
}



var servertoMongo = new servertoMongo_proto();

console.log('servertoMongo loaded');

module.exports = servertoMongo;
