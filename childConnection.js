/*
  child connecton check


*/


require('babel-core').transform('code');	// use babel

var moment = require('moment');
//var servertoMongo = require('./servertoMongo');
var serverConfig = require('./serverConfig');





/*
  wireless connection check

*/

class childConnection_proto{
  constructor(){
    console.log('new childConnection');
  }

  /*
    wireless connection state generate

     set serverConfig.childConnectionStete

    param  : child data
    return : none

  */
  checkWirelessConnection(jdata){
    //console.log('check connection', jdata);
    var now = new moment();
    //var nowTime = now.format('HH:mm.ss');
    jdata.childs.forEach( (childx, index) => {
      var childTimeStump = parseInt(childx.timestump, 16);
      if(childTimeStump !== serverConfig.timestumps[index]){
        serverConfig.timestumps[index] = childTimeStump;
        serverConfig.timestumpChangeTime[index] = now.valueOf();
        serverConfig.childConnectionState[index] = 2;
      }else{
        if(serverConfig.childConnectionState[index] !== 0){
          var unixtime = now.valueOf();
          var lastChangeTime = serverConfig.timestumpChangeTime[index];
          var diff = (unixtime - lastChangeTime) / 1000;
          /*
            check   diff Time  >   interval Time * 1.2
          */
          if(diff > (serverConfig.parentConfig.childTxIntervals[index] * 1.2)){
            if(serverConfig.childConnectionState[index] > 0){
              serverConfig.childConnectionState[index] -= 1;
              serverConfig.timestumpChangeTime[index] = now.valueOf();  // change time new for next interval check
            }
          }
        }
      }
    });
    console.log('ts',serverConfig.timestumps);
    console.log('ts_time',serverConfig.timestumpChangeTime);
    console.log('con', serverConfig.childConnectionState);
    //test
    //serverConfig.childConnectionState = [1,1,1,0,0,0,0,0];
  }
}


var childConnection = new childConnection_proto();

module.exports = childConnection;
