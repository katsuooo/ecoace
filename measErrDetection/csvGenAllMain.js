/*
 csv全作成 (2400)
*/
var csvGenHeader = require('./csvGenHeader');
var csvGenBody = require('./csvGenBody');
var csvGenFooter = require('./csvGenFooter');
var csvFile = require('./csvFile');

/*
 旧システムデータを破棄する。
 powerWithErrWithoutDemandデータを含まないデータ
*/
function delOldSysData(docs){
    var cutd = [];
    docs.forEach((d) => {
        if('powerWithErrWithoutDemand' in d){
            cutd.push(d);
        }
    });
    return JSON.parse(JSON.stringify(cutd));
}
/*
 gen all
*/
function genAll(docs){
    docs = delOldSysData(docs);
    var header = csvGenHeader.genHeader(docs);
    var body = csvGenBody.genBody(docs);
    var footer = csvGenFooter.genFooter(docs);
    csvFile.write(header+body+footer);
}
/*
*/
var csvGenAllMain = {
    /*
    */
    genAll : (docs) => {
        genAll(docs);
    }
}


module.exports = csvGenAllMain;