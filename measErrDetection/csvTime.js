/*
 csv time
*/
var powerWithErrJson = require('./powerWithErrJson');

/*
 gen now time
*/
function genNowTime(){
    var csvTime = powerWithErrJson.getTime();    // -30min start time
    return csvTime.substr(0, csvTime.length-3);  // time / cut second
}


var csvTime = {
    /*
    */
    genNowTime : () => {
        return genNowTime();
    }
}


module.exports = csvTime;