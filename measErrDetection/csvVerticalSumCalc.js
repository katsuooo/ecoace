/*
 計算用にデータを加工する。

 viewデータのerrorを0で上書きして、max取得用のデータとする。
 このデータで計算を行い、文字列化と合わせてERR属性を付け戻す。 
*/
var serverConfig = require('../serverConfig');
var calcPower = require('../calcPower');
/*
 縦加算データ (元データ powerWithErrWithoutDemand.view)
*/
var vSums = {
    sum1 : 0.0,
    sum3 : 0.0,
    c1:[0.0,0.0,0.0,0.0],
    c2:[0.0,0.0,0.0,0.0],
    c3:[0.0,0.0,0.0,0.0],
    c4:[0.0,0.0,0.0,0.0],
    c5:[0.0,0.0,0.0,0.0],
    c6:[0.0,0.0,0.0,0.0],
    c7:[0.0,0.0,0.0,0.0],
    c8:[0.0,0.0,0.0,0.0],
}
/*
 co2データ
*/
var co2 = {
    fai1: 0.0,
    fai3: 0.0
}
/*
*/
function getCalcs(docs){
    var calcs = [];
    docs.forEach( (doc) => {
        var v = JSON.parse(JSON.stringify(doc.powerWithErrWithoutDemand.view));
        if(v.power_sum_30m === serverConfig.MEAS_ERR_STR) {
            v.power_sum_30m = 0;
        }
        if(v.power3_sum_30m === serverConfig.MEAS_ERR_STR) {
            v.power3_sum_30m = 0;
        }
        for(var i=0; i<serverConfig.CHILD_NUM; i++){
            var label = 'c' + (i+1).toString(10);
            for(var j=0; j<serverConfig.ADC_NUM; j++){
                if(v.power[label][j] ===  serverConfig.MEAS_ERR_STR){
                    v.power[label][j] = 0;
                }
            }
        }
        calcs.push(v);
    });
    return JSON.parse(JSON.stringify(calcs));
}
/*
 clear sums
*/
function clearVsum(){
    vSums.sum1 = 0.0;
    vSums.sum3 = 0.0;
    for(var i=0; i<serverConfig.CHILD_NUM; i++){
        var label = 'c' + (i+1).toString(10);
        for(var j=0; j<serverConfig.ADC_NUM; j++){
            vSums[label][j] = 0.0;
        }
    }
}
/*
 kwhに変換

 30分間の電力値の積算なので、0.5をかけてkwhに変換する
*/
function toKWH(){
    const convHour = 0.5;
    vSums.sum1 *= convHour;
    vSums.sum3 *= convHour;
    for(var i=0; i<serverConfig.CHILD_NUM; i++){
        var label = 'c' + (i+1).toString(10);
        for(var j=0; j<serverConfig.ADC_NUM; j++){
            vSums[label][j] *= convHour;
        }
    }    
}
/*
 calcデータを縦加算する
*/
function calcVerticalSums(calc){
    clearVsum();
    calc.forEach( (c) => {
        vSums.sum1 += c.power_sum_30m;
        vSums.sum3 += c.power3_sum_30m;
        for(var i=0; i<serverConfig.CHILD_NUM; i++){
            var label = 'c' + (i+1).toString(10);
            for(var j=0; j<serverConfig.ADC_NUM; j++){
                vSums[label][j] += c.power[label][j];
            }
        }
    });
    toKWH();    
}
/*
 パワー値からco2値を算出
*/
function calcCo2(){
    co2.fai1 = calcPower.calcCo2(vSums.sum1, 1);
    co2.fai3 = calcPower.calcCo2(vSums.sum3, 3);
}
/*
*/
var csvVerticalSumCalc = {
    /*
     mongo power >>> powerWithErrWithoutDemand.viewデータのERRを0で上書き
    */
    getCalcs : (docs) => {
        return getCalcs(docs);
    },
    /*
    */
    calcVerticalSums : (calc)=>{
        calcVerticalSums(calc);
    },
    /*
    */
    calcCo2 : () => {
        calcCo2();
    },
    /*
    */
    getCo2 : () => {
        return JSON.parse(JSON.stringify(co2));
    },
    /*
    */
    getVsum : () => {
        return JSON.parse(JSON.stringify(vSums));
    }

}

module.exports = csvVerticalSumCalc;
