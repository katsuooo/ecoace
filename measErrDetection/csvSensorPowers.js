/*
 センサ個別power値
*/
var powerWithErrJson = require('./powerWithErrJson');
var defs = require('../serverConfig');
var csvParams = require('./csvParams');
var serverConfig = require('../serverConfig');
//var csvSensorPowers = require

/*
*/
function genSonsorPowers(){
    var data = powerWithErrJson.getView();
    var pstr = '';
    for(var i=0; i<defs.CHILD_NUM; i++){
        var label = 'c' + (i+1).toString(10);
        for(var j=0; j<defs.ADC_NUM; j++){
          //pstr += (data.power[label][j] + data.power3[label][j]).toFixed(2) + ',';    // calc data or old
            //数値.xxにフォーマット
            if(isNaN(data.power[label][j]) === false){
                data.power[label][j] = data.power[label][j].toFixed(2);
            }
            pstr += data.power[label][j] + ',';
        }
        // output data
        if(csvParams.getMeasErr(i) !== 0){
            pstr +=  serverConfig.MEAS_ERR_STR + ',';
        }else{
            if( defs.childrenOutputs[i] === 1){
                pstr +=  'on' + ',';
              }else{
                pstr +=  'off' + ',';
              }
        }

    }
    return pstr;
}


var csvSensorPowers = {
    /*
    */
    genSensorPowers: () => {
        return genSonsorPowers();
    } 
}


module.exports = csvSensorPowers;