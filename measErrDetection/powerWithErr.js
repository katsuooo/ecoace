/*
 30min powerデータに計測エラー判別をつける（view）
                　　計測値を0にする（集計）
 ２種類のデータをつくる。
*/

var powerNoErr = require('./powerNoErr');
var powerWithErrCalc = require('./powerWithErrCalc');
var powerWithErrView = require('./powerWithErrView');
var powerWithErrJson = require('./powerWithErrJson');
var powerWithErrLogs = require('./powerWithErrLogs');
//var powerWithErrMongo = require('./powerWithErrMongo');


/*
 power値に計測エラーづけ
*/
var powerWithErr = {
    /*
     計測エラーなしのパワー算出(30min)
     作成した旧データは
     powerWithErrCalc/power30min.jsonに保存する
    */
    noErrPower : (powers) => {
        powerNoErr.create30minPower(powers);
    },
    /*
     計測エラーつきデータの作成
    */
    gen30minPowers : () => {
        powerWithErrCalc.genCalc();        // パワー集計用データ作成
        powerWithErrView.genView();         // 画面,csv用個別パワーデータの作成
        powerWithErrJson.saveConfigs();     // serverConfigsの保存()
    },
    /*
     data loggint
    */
    logging : () => {
        //powerWithErrLogs.csv30min();      // no error
        powerWithErrLogs.csv30minWithErr(); // with error
    }
}

module.exports = powerWithErr;