/*
 csv作成用データ

 csv作成用に必要なデータをまとめる。
 config等とかぶった内容のため、保存はしないが、csv内容を見通し良くするため作成する。
*/
var csvp = require('./csvParams.json');
var serverConfig = require('../serverConfig');
var calcPower = require('../calcPower');
/*
 csvParams.json
 cx(x:1-8) 子機No 
  register 子機登録 　1:あり, 0:なし
  outer    室外機制御 1:あり, 0:なし
  measErr　計測エラー E:エラー発生 0:正常
  fai[4]   計測電源   1:単層  3:3相
*/
/*
*/
function getRegister(child){
    return serverConfig.childrenRegistered[child];
}
/*
*/
function getOuter(child){
    return serverConfig.childrenOuterRegistered[child];
}
/*
*/
function getMeasErr(child){
    return serverConfig.measErr[child];
}
/*
*/
function getFais(child){
    var fais = [];
    for(var i=0; i<serverConfig.ADC_NUM; i++){
        fais.push(calcPower.getPowerFai(child, i));
    }
    return JSON.parse(JSON.stringify(fais));
}
/*
*/
function genCsvParams(){
    for(var i=0; i<serverConfig.CHILD_NUM; i++){
        var label = 'c' + (i+1).toString(10);
        csvp[label].register = getRegister(i);
        csvp[label].outer = getOuter(i);
        csvp[label].measErr = getMeasErr(i);
        csvp[label].fai = getFais(i);
    }
}
/*
 power1 計測errorの取得

 child1-8の1fai設定のmeasErrのorを返す(1つあればerrorあり)

 param  :
 return : 1: errorあり, 0:errorなし
*/
function getP1Err(){
    for(var i=0; i<serverConfig.CHILD_NUM; i++){
        var label = 'c' + (i+1).toString(10);
        if(csvp[label].measErr !== 0){      // maesErrあり
            for(var j=0; j<serverConfig.ADC_NUM; j++){
                if(csvp[label].fai[j] === 1){   // 1f設定あり？
                    return 1;
                } 
            }
        }
    }
    return 0;
}
/*
 p3も同様
*/
function getP3Err(){
    for(var i=0; i<serverConfig.CHILD_NUM; i++){
        var label = 'c' + (i+1).toString(10);
        if(csvp[label].measErr !== 0){
            for(var j=0; j<serverConfig.ADC_NUM; j++){
                if(csvp[label].fai[j] === 3){           // 3f設定あり？
                    return 1;
                } 
            }
        }
    }
    return 0;
}
/*
 fai値を得る
*/
function getFai(label, n){
    return csvp[label].fai[n];
}  
/*
 demand設定状態の取得(child単位)
 param  : none
 return : [8] [1,1,1,1,0,0,0,0]
          1: demandOn, 0:demandOff
*/
function getDemandState(){
    var demandState = [];
    for(var i=0; i<serverConfig.CHILD_NUM; i++){
        var label = 'c' + (i+1).toString(10);
        if(csvp[label].register === 1){
            if(csvp[label].outer === 1){     
                demandState.push(1)
            } else {
                demandState.push(0);
            }
        }else{
            demandState.push(0);
        }
    }
    return JSON.parse(JSON.stringify(demandState));
}

/*
*/
var csvParams = {
    /*
     csv params作成
    */
    genCsvParams: () => {
        genCsvParams();
    },
    /*
     power1エラーの取得
    */
    getP1Err: () => {
        return getP1Err();
    },
    /*
     power3エラーの取得
    */
    getP3Err: () => {
        return getP3Err();
    },
    /*
     labelからfaiを得る
    */
    getFai: (label, n) => {
        return getFai(label, n);
    },
    /*
     demand 設定状態（child単位）
    */
    getDemandState: () => {
        return getDemandState();
    },
    /*
     measErrをとる。
    */
    getMeasErr: (child) => {
        return getMeasErr(child);
    },
    /*
     print
    */
    print : () => {
        console.log(csvp);
    }
}

module.exports = csvParams;