/*
 vertical line sum
 24:00 only
*/
var serverConfig = require('../serverConfig');
var csvVerticalSumError = require('./csvVerticalSumError');
var csvParams = require('./csvParams');
var calcPower = require('../calcPower');
var csvVerticalSumMax = require('./csvVerticalSumMax');
var csvVerticalSumCalc = require('./csvVerticalSumCalc');

/*
 oneday vertical sums
*/
var vSums = {
    fai1: {
        sum: 0,
        co2: 0,
        c1: [0,0,0,0],
        c2: [0,0,0,0],
        c3: [0,0,0,0],
        c4: [0,0,0,0],
        c5: [0,0,0,0],
        c6: [0,0,0,0],
        c7: [0,0,0,0],
        c8: [0,0,0,0]
    },
    fai3: {
        sum: 0,
        co2: 0,
        c1: [0,0,0,0],
        c2: [0,0,0,0],
        c3: [0,0,0,0],
        c4: [0,0,0,0],
        c5: [0,0,0,0],
        c6: [0,0,0,0],
        c7: [0,0,0,0],
        c8: [0,0,0,0]
    }
}



/*
 clear sums
*/
function clearSums(){
    vSums.fai1.sum = 0.0;
    vSums.fai1.co2 = 0.0;
    for(var i=0; i<serverConfig.CHILD_NUM; i++){
        var label = 'c' + (i+1).toString(10);
        for(var j=0; j<serverConfig.ADC_NUM; j++){
            vSums.fai1[label][j] = 0.0;
        }
    }

}
/*
 sum Childs

 param  : label c1, c2...
          vSum  加算結果バッファ
          v     doc.powerWithErrWithoutDemand.view
*/
function sumChilds(label, vSum, power){
    for(var i=0; i<serverConfig.ADC_NUM; i++){
        if(csvParams.getFai(label, i) === 1){
            vSums.fai1[label][i] += power[label][i];
        }else{
            vSums.fai3[label][i] += power[label][i];           
        }
    }
}

/*
 ERRORを検出しながら計算するバージョン >>> 使わない
 縦サム
 1fai_30_sum
 3fai_30_sum
 センサ値はviewの値を縦加算し、csvParams.cx.fai[4]でfaiを決定する。
 >>> fai分けは別の箇所でする。
*/
/*
function genVerticalSums(docs){
    csvVerticalSumError.checkErrs(docs);        //計測エラー検出
    clearSums();
    docs.forEach( (doc) => {
        //console.log(doc.powerWithErrWithoutDemand.view);
        var v = doc.powerWithErrWithoutDemand.view;
        var errors = csvVerticalSumError.getErrors();
        if(errors.sum1p === 0){
            vSums.fai1.sum += v.power_sum_30m;
        }else{
            vSums.fai1.sum = serverConfig.MEAS_ERR_STR;
        }
        if(errors.sum3p === 0){
            vSums.fai3.sum += v.power3_sum_30m;
        }else{
            vSums.fai3.sum = serverConfig.MEAS_ERR_STR;
        }
        for(var i=0; i<serverConfig.CHILD_NUM; i++){
            var label = 'c' + (i+1).toString(10);
            if(errors[label] === 0){
                sumChilds(label, vSums, v.power);
            }else{
                csvVerticalSumError.writeErr(label, vSums);
            }
        }
    });
}
*/



/*
 co2計算
     sumLine += calcpower.calcCo2(calcParams.sums[32], 1).toFixed(2) + ',';
     params : value, fai
*/
function calcCo2(){
    vSums.fai1.co2 = calcPower.calcCo2(vSums.fai1.sum, 1);
    vSums.fai3.co2 = calcPower.calcCo2(vSums.fai3.sum, 1);
}


/*
*/
var csvVerticalSum = {
    /*
    */
    genVerticalSums : (docs) => {
        //genVerticalSums(docs);
        csvVerticalSumError.checkErrs(docs);        //計測エラー検出
        var c = csvVerticalSumCalc.getCalcs(docs);        
        calcCo2();
        csvVerticalSumMax.pickMax(calc);
    },
    /*
    */
    get1 : () => {
        return JSON.parse(JSON.stringify(vSums.fai1));
    },
    get3 : () => {
        return JSON.parse(JSON.stringify(vSums.fai3));
    },
    getMax : () => {
        csvVerticalSumMax.getMax();
    }
}

module.exports = csvVerticalSum;