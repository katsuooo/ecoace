/*
 180824
 power with err data
 servertoMongoで算出されるAvrPowerをベースに計測エラー、デマンド属性を
 考慮して値をつくる。
 計算用と、view用のデータを作成する。

 
 計算用
 計測エラー >>> ERR, デマンドOFF >>> 0
 view用
 計測エラー >>> ERR, デマンドOFF >>> 通常のパワー値
*/


var powerWithErrData = {
    calc: {
        power1: {
          c1: [ 0, 0, 0, 0 ],
          c2: [ 0, 0, 0, 0 ],
          c3: [ 0, 0, 0, 0 ],
          c4: [ 0, 0, 0, 0 ],
          c5: [ 0, 0, 0, 0 ],
          c6: [ 0, 0, 0, 0 ],
          c7: [ 0, 0, 0, 0 ],
          c8: [ 0, 0, 0, 0 ] },
        power3: {
          c1: [ 0, 0, 0, 0 ],
          c2: [ 0, 0, 0, 0 ],
          c3: [ 0, 0, 0, 0 ],
          c4: [ 0, 0, 0, 0 ],
          c5: [ 0, 0, 0, 0 ],
          c6: [ 0, 0, 0, 0 ],
          c7: [ 0, 0, 0, 0 ],
          c8: [ 0, 0, 0, 0 ] } },
    view : {

    }
}



module.exports = powerWithErrData;