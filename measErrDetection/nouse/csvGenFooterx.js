/*
 gen csv footer
*/
/*
 form
1pTOTAL(kwh),.0,,,.0,,,,,,.0,,,,,,,,,,,,,,,,,,,,.0,,,,,,,,,,,,,,,
3pTOTAL(kwh),,3.05,,1.60,2.95,.0,.0,.0,,,.0,.0,.0,,.0,.0,.0,.0,,.0,.0,.0,.0,,.0,.0,.0,.0,,,.0,.0,.0,,.0,.0,.0,.0,,.0,.0,.0,.0,,
max

maxを追加。
縦maxの表示。ERRは０として値をとる。

1 title
2 1f sum30m total/max
3 3f 
4 (3f maxday)
5 co2 total/max
6-9   child1 re01_ch1-4 total/max
10-14 child2
15-19 child3
20-24 child4
25-29 child5
30-34 child6
35-39 child7
40-44 child8
45 \r\n
*/
var csvVerticalSum = require('./csvVerticalSum');
var serverConfig = require('../serverConfig');
/*
 1f vertical sum
 1 title
2 1f sum30m total/max
3 (3f total)
4 (3f maxday)
5 co2 total/max
6-9   child1 re01_ch1-4 total/max
10-14 child2
15-19 child3
20-24 child4
25-29 child5
30-34 child6
35-39 child7
40-44 child8
45 \r\n
*/
function sum1f(){
    var sum1 = csvVerticalSum.get1();
    var sum1Line = '';
    sum1Line += '1pTOTAL(kwh)' + ',';
    sum1Line += sum1.sum.toFixed(2) + ',';
    sum1Line += ',,';
    sum1Line += sum1.co2.toFixed(2) + ',';
    for(var i=0; i<serverConfig.CHILD_NUM; i++){
        var label = 'c' + (i+1).toString(10);        
        for(var j=0; j<serverConfig.ADC_NUM; j++){
            sum1Line += sum1[label][j].toFixed(2) + ',';
        }
        sum1Line += ',';    // output
    }
    sum1Line += '\r\n'
    return sum1Line;
}
/*
 3f vertical sum
 1 title
2 (1f sum30m total/max)
3 3f total
4 (3f maxday)
5 co2 total/max
6-9   child1 re01_ch1-4 total/max
10-14 child2
15-19 child3
20-24 child4
25-29 child5
30-34 child6
35-39 child7
40-44 child8
45 \r\n
*/
function sum3f(){
    var sum3 = csvVerticalSum.get3();
    var sum3Line = '';
    sum3Line = '3pTOTAL(kwh)' + ',';
    sum3Line += ',,';
    console.log(sum3);
    sum3Line += sum3.sum.toFixed(2) + ',';    
    sum3Line += sum3.co2.toFixed(2) + ',';
    for(var i=0; i<serverConfig.CHILD_NUM; i++){
        var label = 'c' + (i+1).toString(10);        
        for(var j=0; j<serverConfig.ADC_NUM; j++){
            sum3Line += sum3[label][j].toFixed(2) + ',';
        }
        sum3Line += ',';    // output
    }
    sum3Line += '\r\n'
    return sum3Line;
}
/*
 vertical max vals
  1 title
2 1f sum30m total/max)
3 3f total
4 (3f maxday)
5 co2 total/max
6-9   child1 re01_ch1-4 total/max
10-14 child2
15-19 child3
20-24 child4
25-29 child5
30-34 child6
35-39 child7
40-44 child8
45 \r\n
*/
function maxVals(){
    var mval = csvVerticalSum.getMax();
    var mvalLine = '';
    mvalLine = 'max(kw)' + ',';
    mvalLine += mval.sum1.toFixed(2) + ',';
    mvalLine += mval.sum3.toFixed(2) + ',';    
    mvalLine += mval.co2.toFixed(2) + ',';
    for(var i=0; i<serverConfig.CHILD_NUM; i++){
        var label = 'c' + (i+1).toString(10);        
        for(var j=0; j<serverConfig.ADC_NUM; j++){
            mvalLine += mval[label][j].toFixed(2) + ',';
        }
        mvalLine += ',';    // output
    }
    mvalLine += '\r\n';
    return '';
}
/*
 gen footer
*/
function genFooter(){
    var footer = '';
    footer += ',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,';
    footer += sum1f();
    footer += sum3f();
    footer += maxVals();
    return footer;
}
/*
*/
var csvGenFooter = {
    /*
    */
    genFooter: (docs) => {
        csvVerticalSum.genVerticalSums(docs);
        return genFooter();
    }
}

module.exports = csvGenFooter;