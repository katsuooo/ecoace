/*
 footer 24:00のみ
*/
var cavFooterParse = require('./csvFooterParse');
var csvVerticalSum = require('./csvVerticalSum');


/*
 gen footer

 params  : docs, 30min mongo data
*/
function genFooter(docs){
    cavFooterParse.parseMongo(docs);
    /*
    header, body, footerで全作成する。
    今のも全作成してる。
    */
    /*
    genVerticalSum();
    getMaxday();
    addBlank();
    p1Total();
    p3Total();
    verticalMax();
    */
}


/*
*/
var csvFooter = {
    /*
     gen footer with errror
    */
    genFooter: (docs) => {
        genFooter(docs);
    }
}



module.exports = csvFooter;