/*
180823
計測エラーの検出
計測30min単位ないで通信エラーにより３回以上計測データが取得できない場合、計測エラーとする。
計測エラー発生時はデータ値をERRと表記。計算結果もERR

serverConfigへの追加
serverConfig.wconCount= [0,0,0,0,0,0,0,0];          // wconエラーのカウント
serverConfig.measErr = [0,0,0,0,'E','E','E','E'];   // 計測エラー判定状態
*/

var serverConfig = require('../serverConfig');
var powerWithErr = require('./powerWithErr');
var calcPower = require('../calcPower');
var csvParams = require('./csvParams');         // csv用データを集める


/*
 計測エラーデータ(計算用バッファ)
 serverConfigに同データを追加
*/
var wconCounter = [0,0,0,0,0,0,0,0];
var measErr = [0,0,0,0,0,0,0];

/*
 wcon counter reset
*/
var resetWconCounter = function(){
    wconCounter = [0,0,0,0,0,0,0,0];
    measErr = [0,0,0,0,0,0,0,0];
}
/*
 wcon error count loop
*/
var wconCountLoop = function(childs){
    childs.forEach( (child, index) => {
        if(child.wcon < serverConfig.wconSafeState){
            wconCounter[index] += 1;
        }
    });
}
/*
 wconエラーのカウント値から計測エラーを判別
 ３回以上のwconエラーで計測エラーとする。
 登録されていない子機はエラーにしない。
*/
var judgeMeasErr = function(){
    wconCounter.forEach((count, index) => {
        if ((count >= serverConfig.measErrLimit) &&
            (serverConfig.childrenRegistered[index] != 0)){
            measErr[index] = 'E';
        }else{
            measErr[index] = 0;            
        }
    });
}

/*
 wcon != 2 のカウント

*/
var wconCount = function(jdata){
    resetWconCounter();
    jdata.forEach( (d) => {
        wconCountLoop(d.childs);
    });
    judgeMeasErr();
}

/*
 wcon != 2 のカウント, 計測err判定
*/
function MeasErrCountAndJudge(jdata){
    wconCount(jdata);
    serverConfig.wconCount = JSON.parse(JSON.stringify(wconCounter));
    serverConfig.measErr = JSON.parse(JSON.stringify(measErr));
}

/*
 計測エラー検出メイン
*/
var measErrDetectionMain = {
    start : () => {
        console.log('meas err detection main start');
    },
    /*
     wcon != 2 のカウント, powerデータに計測errをつける
    */
/*    
    count : (jdata) => {
        wconCount(jdata);
        serverConfig.wconCount = JSON.parse(JSON.stringify(wconCounter));
        serverConfig.measErr = JSON.parse(JSON.stringify(measErr));
    },
*/
    /*
     calc power計算
      暫定でエラー情報のみ返す
      保存データ
      wconCount          : wcon != 2 のカウント値
      meadErr            : 計測エラー判定値 error: 'E', 正常 : '0' 
      childrenRegistered : 子機の登録状態 
    */
    calc: () => {
        d = {};
        d.wconCount = serverConfig.wconCount;
        d.measErr = serverConfig.measErr;
        d.childrenRegistered = serverConfig.childrenRegistered;
        return JSON.parse(JSON.stringify(d));
    },
    /*
     30minパワー処理
     計測エラーを考慮しない現処理に、計測エラーを考慮した処理を追加して書き直す。
     param  : cache1min 直前30min間の1min毎データ配列
    */
    calcSavePer30min: (cache1mins) => {
        MeasErrCountAndJudge(cache1mins);      // 計測エラーの検出
        var avrPower = calcPower.calc30min(cache1mins);   // power 1,3fai 分離, アベレージング
        powerWithErr.noErrPower(avrPower);                // 計測エラーを評価しない電力集計(旧型式データの作成)
        csvParams.genCsvParams();              // csv作成用フラグを集める
        powerWithErr.gen30minPowers();                    // 計測エラー判別追加 
        powerWithErr.logging();                           // csv 30min append
    },
    /*
     for test
    */
    MeasErrCountAndJudge: (cache1mins) => {
        MeasErrCountAndJudge(cache1mins);
    }
}


module.exports = measErrDetectionMain;