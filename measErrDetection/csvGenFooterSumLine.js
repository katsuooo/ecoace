/*
 1f, 3fの合計文字列ラインをつくる
*/
var csvVerticalSum = require('./csvVerticalSum');
var serverConfig = require('../serverConfig');
var csvParams = require('./csvParams');

/*
 1,3 faiのsum lineを生成する。

 1f vertical sum
 1 title
2 1f sum30m total/max
3 (3f total)
4 (3f maxday)
5 co2 total/max
6-9   child1 re01_ch1-4 total/max
10-14 child2
15-19 child3
20-24 child4
25-29 child5
30-34 child6
35-39 child7
40-44 child8
45 \r\n
 3f vertical sum
 1 title
2 (1f sum30m total/max)
3 3f total
4 (3f maxday)
5 co2 total/max
6-9   child1 re01_ch1-4 total/max
10-14 child2
15-19 child3
20-24 child4
25-29 child5
30-34 child6
35-39 child7
40-44 child8
45 \r\n
*/
var line1 = '';
var line3 = '';
/*
 viewのERR >>> 0に変換したデータ
*/
var vsum = {};
/*
 計測エラー状態
*/
var errors = {};
/*
*/
function genTitle(){
    line1 += '1pTOTAL(kwh)' + ',';
    line3 += '3pTOTAL(kwh)' + ',';
}
/*
*/
function genTotal1(){
    if(errors.sum1p === 1){
        line1 += serverConfig.MEAS_ERR_STR;
    }else{
        line1 += vsum.sum1.toFixed(2);
    }
    line1 += ',';
    line3 += ',';
}
/*
*/
function genTotal3(){
    if(errors.sum3p === 1){
        line3 += serverConfig.MEAS_ERR_STR;
    }else{
        line3 += vsum.sum3.toFixed(2);
    }
    line1 += ',';
    line3 += ',';
}
/*
 blank
*/
function genBlank(){
    line1 += ',';
    line3 += ',';
}
/*
 gen co2
*/
function genCo2(){
    var co2 = csvVerticalSum.getCo2();
    if(errors.sum1p === 1){
        line1 += serverConfig.MEAS_ERR_STR;
    }else{
        line1 += co2.fai1.toFixed(2);
    }   
    if(errors.sum3p === 1){
        line3 += serverConfig.MEAS_ERR_STR;
    }else{
        line3 += co2.fai3.toFixed(2);
    }
    genBlank(); 
}
/*
 child val

 error.cx = 1 >>> 'ERR'
            0 >>> 
*/
function childVal(label, adc){
    if(errors[label] === 1){
        return serverConfig.MEAS_ERR_STR;
    }
    return vsum[label][adc].toFixed(2);
}
/*
 chile sensor data

 check fais        / 1 or 3 by
 check meas errors / if err, 'ERR' matched fai cell 
*/
function genChilds(){
    for(var i=0; i<serverConfig.CHILD_NUM; i++){
        var label = 'c' + (i+1).toString(10);
        for(var j=0; j<serverConfig.ADC_NUM; j++){
            if( csvParams.getFai(label, j) === 1){
                line1 += childVal(label, j) + ',';
                line3 += ',';
            }else{
                line1 += ',';
                line3 += childVal(label, j) + ',';
            }   
        }
        genBlank();     // output
    }
}
/*
*/
function genSumLines(){
    line1 = '';
    line3 = '';
    vsum = csvVerticalSum.getVsum();
    errors = csvVerticalSum.getErrors();
    genTitle();
    genTotal1();
    genTotal3();
    genBlank();     // maxday
    genCo2();
    genChilds();
    line1 += '\r\n';
    line3 += '\r\n';
}
/*
*/
var csvGenFooterSumLine = {
    /*
    */
    genSumLines : () => {
        genSumLines();
    },
    /*
     1fai sum lineと3fai sum lineを得る
    */
    getSumLines : () => {
        return line1 + line3;
    }
}


module.exports = csvGenFooterSumLine;