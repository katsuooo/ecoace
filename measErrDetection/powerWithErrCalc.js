/*
 180824
 計測error 計算用データの作成/保存
 powerWithErrJson/power30minバッファに保存

  calc用power個別データ >>> 計測エラー E, デマンドOFF = 0
*/

var powerWithErrJson = require('./powerWithErrJson');
var serverConfig = require('../serverConfig');
var calcpower = require('../calcPower');


/*
 measErrのセット
 serverConfig.measErr != 0 のチャンネルパワー値を'E'に >>> 0にする
 serverConfig.childrenOuterRegistered != 0 のチャンネルパワー値を0に

 power計算時、先に８chan計算した後、エラー評価&ラベリングするため
*/
function setMeasErrAnd(powers){
    /*
    console.log('count', serverConfig.wconCount);
    console.log('err', serverConfig.measErr);
    console.log('reg', serverConfig.childrenRegistered);
    console.log('demand', serverConfig.childrenOuterRegistered);
    */

    for(var i=0; i<serverConfig.CHILD_NUM; i++){
        var label = 'c' + (i+1).toString(10);
        if(serverConfig.measErr[i] != 0){                       //計測エラー
            for(var j=0; j<serverConfig.ADC_NUM; j++){
                //powers.power[label][j] = 'E';
                //powers.power3[label][j] = 'E';
                powers.power[label][j] = 0.0;
                powers.power3[label][j] = 0.0;
            }
        }else if(serverConfig.childrenOuterRegistered === 0){   // デマンドオフ
            for(var j=0; j<serverConfig.ADC_NUM; j++){
                powers.power[label][j] = 0.0;
                powers.power3[label][j] = 0.0;
            }        
        }
    }

}
/*
 1,3fai 設定情報を保存
 縦sum計算時に必要
*/
/*
function saveFaiInfo(){
    var fais = powercalc.getPowerFais();
}
*/
/*
 gen calc data
*/
function genCalc(){
    var powers = powerWithErrJson.getNoErrPowers();
    setMeasErrAnd(powers);
    //powers.measErr = serverConfig.measErr;
    //powers.childrenOuterRegistered = serverConfig.childrenOuterRegistered;
    powerWithErrJson.saveCalcs(JSON.parse(JSON.stringify(powers)));
}

/*
*/
var powerWithErrCalc = {
    /*
     30min毎の保存データの作成
     servertoMongo/create30minの作成をコピーし、
     計測エラー込みデータを追加作成する
    */
    genCalc: () => {
        genCalc();
    }
}


module.exports = powerWithErrCalc;