/*
 mongo用データ作成
*/
var powerWithErrJson = require('./powerWithErrJson');
//var servertoMongo = require('../servertoMongo');


/*
 gen collection name
*/
function genCollectionName(jtime){
    var timeStr = jtime.split('T');
    return 'power_' + timeStr[0];

}

/*
*/
var powerWithErrMongo = {
    /*
     get power collection name
    */
    getColName : () => {
        return genCollectionName(powerWithErrJson.getTime());
    },
    /*
     get 30min jsons
    */
    get30minJson : () => {
        return powerWithErrJson.getAll();
    }
}


module.exports = powerWithErrMongo;