/*
 csv header
*/
/*
format
aircon-control,,,,,on,off..... 
Day,1pTOTAL(kw),3pTOTAL(kw),3pMAX/DAY,TOTAL(CO2),RE01_CH1,RE01_CH2,RE01_CH3,RE01_CH4,RE01_Output,RE02_CH1,RE02_CH2,RE02_CH3,RE02_CH4,RE02_Output,RE03_CH1,RE03_CH2,RE03_CH3,RE03_CH4,RE03_Output,RE04_CH1,RE04_CH2,RE04_CH3,RE04_CH4,RE04_Output,RE05_CH1,RE05_CH2,RE05_CH3,RE05_CH4,RE05_Output,RE06_CH1,RE06_CH2,RE06_CH3,RE06_CH4,RE06_Output,RE07_CH1,RE07_CH2,RE07_CH3,RE07_CH4,RE07_Output,RE08_CH1,RE08_CH2,RE08_CH3,RE08_CH4,RE08_Output
*/
var serverConfig = require('../serverConfig');
var csvParams = require('./csvParams');

/*
 dummy 4 ()
 day, 1ptotal, 3ptotal, 3pmax, total(co2),re01_ch1,....
*/
const DEMANDSTART = 'aircon-control,,,,,';
/*
 gen demand string

 param  : demand >>> [0,0,0,0,1,1,1,1]
          0: demandOff, 1: demandOn
          チャンネル単位で
*/
function genDemandString(demand){
    var demandStr = DEMANDSTART;
    demand.forEach( (d) => {
        if(d == 1){
            demandStr += 'on,on,on,on,,';       // add output ''
        }else{
            demandStr += 'off,off,off,off,,';   // add output ''           
        }
    });
    demandStr += '\r\n';
    return demandStr;
}
/*
 gen demand

 デマンド状態は最新データを使用して取得する
 powerWithErrWithoutDemand.childrenRegistered
 powerWithErrWithoutDemand.childrenOuterRegistered 
 がともに1のときデマンドON状態
*/
function genDemand(docs){
    var newest = docs.length - 1;
    var reg = docs[newest].powerWithErrWithoutDemand.childrenRegistered;
    var outer = docs[newest].powerWithErrWithoutDemand.childrenOuterRegistered;
    var demand = [];
    for(var i=0; i<serverConfig.CHILD_NUM; i++){
        if((reg[i] === 1)&&(outer[i] === 1)){
            demand.push(1);
        }else{
            demand.push(0);
        }
    }
    return genDemandString(demand);
}
/*
 gen title label
*/
function genTitleLabel(){
    var titles = '';
    titles += 'Day,';
    titles += '1pTOTAL(kw),';
    titles += '3pTOTAL(kw),';
    titles += '3pMAX/DAY,'; 
    titles += 'TOTAL(CO2),';

    for (var i=0; i<8; i++){
      titles += 'RE0' + (i+1).toString(10) + '_CH1,';
      titles += 'RE0' + (i+1).toString(10) + '_CH2,';
      titles += 'RE0' + (i+1).toString(10) + '_CH3,';
      titles += 'RE0' + (i+1).toString(10) + '_CH4,';
      titles += 'RE0' + (i+1).toString(10) + '_Output,';
    }
    titles = titles.substr(0, titles.length-1);
    titles += '\r\n';      
    return titles;
}
/*
 gen csv header
*/
function genHeader(docs){
    var demand = genDemand(docs);
    var titleLabel = genTitleLabel();
    return demand + titleLabel; 
}
/*
 csv paramの値からheaderを生成する
*/
function getHeaderByParam(){
    var demandState = csvParams.getDemandState();
    var demand =  genDemandString(demandState);
    var titleLabel = genTitleLabel();
    return demand + titleLabel; 
}
/*
*/
var csvGenHeader = {
    /*
     header gen
    */
    genHeader : (docs) => {
        return genHeader(docs);
    },
    /*
    */
    getHeaderByParam : () => {
        return getHeaderByParam();
    }
}

module.exports = csvGenHeader;