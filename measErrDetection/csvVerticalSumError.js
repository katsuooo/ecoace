/*
 verticalサム計算時の計測エラーチェック
*/
var serverConfig = require('../serverConfig');
var csvParams = require('./csvParams');

/*
 error flag
 0:正常、 1:error
*/
var errors = {
    sum1p: 0,
    sum3p: 0,
    c1: 0,
    c2: 0,
    c3: 0,
    c4: 0,
    c5: 0,
    c6: 0,
    c7: 0,
    c8: 0
}
/*
 error flag clear
*/
function clearErrors(){
    errors.sum1p = 0;
    errors.sum3p = 0;
    errors.c1 = 0;
    errors.c2 = 0;
    errors.c3 = 0;
    errors.c4 = 0;
    errors.c5 = 0;
    errors.c6 = 0;
    errors.c7 = 0;
    errors.c8 = 0;
}
/*
 check & save
 ERR : 1, 正常: 0
*/
function isErr(d, key){
    if(d === 'ERR'){
        errors[key] = 1
    }
}
/*
 エラーチェック

 ERRがある場合、以降は計算せずERR
 計算時につかうフラグを作成する。
 doc.powerWithErrWithoutDemand.view.powerデータをチェックする

 c1-8については、[4]配列すべてにERRがはいるので、[0]のみチェックする
*/
function checkErrs(docs){
    clearErrors();
    docs.forEach( (doc) => {
        isErr(doc.powerWithErrWithoutDemand.view.power_sum_30m, 'sum1p');
        isErr(doc.powerWithErrWithoutDemand.view.power3_sum_30m, 'sum3p');
        isErr(doc.powerWithErrWithoutDemand.view.power.c1[0], 'c1');
        isErr(doc.powerWithErrWithoutDemand.view.power.c2[0], 'c2');
        isErr(doc.powerWithErrWithoutDemand.view.power.c3[0], 'c3');
        isErr(doc.powerWithErrWithoutDemand.view.power.c4[0], 'c4');
        isErr(doc.powerWithErrWithoutDemand.view.power.c5[0], 'c5');
        isErr(doc.powerWithErrWithoutDemand.view.power.c6[0], 'c6');
        isErr(doc.powerWithErrWithoutDemand.view.power.c7[0], 'c7');
        isErr(doc.powerWithErrWithoutDemand.view.power.c8[0], 'c8');
    });
}

/*
 child にerrorをかく
*/
function writeErr(label, vSums){
    for(var i=0; i<serverConfig.ADC_NUM; i++){
        if(csvParams.getFai(label, i) === 1){
            vSums.fai1[label][i] = serverConfig.MEAS_ERR_STR;
        }else{
            vSums.fai3[label][i] = serverConfig.MEAS_ERR_STR;          
        }        
    }
}
/*
 errorフラグの取得
*/
function getErrors(){
    return JSON.parse(JSON.stringify(errors));
}
/*
*/
var csvVerticalSumError = {
    /*
    */
    checkErrs: (docs) => {
        checkErrs(docs);
    },
    /*
    */
    getErrors: () => {
        return getErrors();
    },
    /*
    */
    writeErr: (label, vSums) => {
        writeErr(label, vSums);
    }
}

module.exports = csvVerticalSumError;