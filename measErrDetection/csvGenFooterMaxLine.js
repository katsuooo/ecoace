/*
　max値ラインの生成

csv title
Day,1pTOTAL(kw),3pTOTAL(kw),3pMAX/DAY,TOTAL(CO2),RE01_CH1,RE01_CH2,RE01_CH3,RE01_CH4,RE01_Output,RE02_CH1,RE02_CH2,RE02_CH3,RE02_CH4,RE02_Output,RE03_CH1,RE03_CH2,RE03_CH3,RE03_CH4,RE03_Output,RE04_CH1,RE04_CH2,RE04_CH3,RE04_CH4,RE04_Output,RE05_CH1,RE05_CH2,RE05_CH3,RE05_CH4,RE05_Output,RE06_CH1,RE06_CH2,RE06_CH3,RE06_CH4,RE06_Output,RE07_CH1,RE07_CH2,RE07_CH3,RE07_CH4,RE07_Output,RE08_CH1,RE08_CH2,RE08_CH3,RE08_CH4,RE08_Output

*/
var csvVerticalSumMax = require('./csvVerticalSumMax');
var serverConfig = require('../serverConfig');

/*
 縦max値をpickした行
*/
var maxLine = ``;

/*
 vertical max vals
  1 title
2 1f sum30m total/max)
3 3f total
4 (3f maxday)
5 co2 total/max
6-9   child1 re01_ch1-4 total/max
10-14 child2
15-19 child3
20-24 child4
25-29 child5
30-34 child6
35-39 child7
40-44 child8
45 \r\n
*/
function genMaxLine(){
    var maxs = csvVerticalSumMax.getMax();
    maxLine = '';
    maxLine += 'max(kw)' + ',';
    maxLine += maxs.sum1.toFixed(2) + ',';
    maxLine += maxs.sum3.toFixed(2) + ',';
    maxLine += ',';     // maxday
    maxLine += maxs.co2.toFixed(2) + ',';
    for(var i=0; i<serverConfig.CHILD_NUM; i++){
        var label = 'c' + (i+1).toString(10);  
        for(var j=0; j<serverConfig.ADC_NUM; j++){
            maxLine += maxs[label][j].toFixed(2) + ',';      
        }
        maxLine += ',';     // output
    }
    maxLine += '\r\n';
}

var csvGenFooterMaxLine = {
    /*
    */
    getMaxLine : () => {
        genMaxLine();
        return maxLine;
    }
}


module.exports = csvGenFooterMaxLine;