/*
 sum pwer1
*/
var powerWithErrJson = require('./powerWithErrJson');
var csvGenBody = require('./csvGenBody');
/*
 sum power 1fai
*/
function genSumP1(){
    var data = powerWithErrJson.getView(); 
    /*
    var powerFloat = parseFloat(data.power_sum_30m);
    var roundedPower = powerFloat.toFixed(2);
    return roundedPower;
    */
    return csvGenBody.errCheck(data.power_sum_30m);
}

var csvSumP1 = {
    /*
    */
    genSumP1: () => {
        return genSumP1();
    }
}


module.exports = csvSumP1;
