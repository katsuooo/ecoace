/*
 電力値の集計（計測エラー評価つき）
 aggregate = 集計
*/
var powerWithErrJson = require('./powerWithErrJson');
var calcPower = require('../calcPower');
var csvParams = require('./csvParams');
var serverConfig = require('../serverConfig');

/*
 計測エラーフラグの追加 1fai

 エラー時はERRをかえす
*/
function addErrP1(power){
    if(csvParams.getP1Err() !== 0){
        return serverConfig.MEAS_ERR_STR;
    }
    return power;
}
/*
 計測エラーフラグの津池 3fai

 エラー時はERRをかえす
*/
function addErrP3(power3){
    if(csvParams.getP3Err() !== 0){
        return serverConfig.MEAS_ERR_STR;
    }
    return power3;   
}

/*
 power 集計
 power_sum_30m
*/
function power(){
    var power = powerWithErrJson.getCalcPower();
    power = calcPower.get30minPowerSum(power); 
    return addErrP1(power);
}

/*
 power3 集計
 power3_sum_230m
*/
function power3(){
    var power3 = powerWithErrJson.getCalcPower3();
    power3 = calcPower.get30minPowerSum(power3);
    return addErrP3(power3);
}


/*
 exports
*/
var powerWithErrAggregate = {
    /*
     power 1fai 集計
    */
    power : () => {
        return power();
    },
    /*
     power 3fai 集計
    */
    power3 : () => {
        return power3();
    }
}

module.exports = powerWithErrAggregate;