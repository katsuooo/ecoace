/*
 tate-sum

 power_30mデータのpowerWithErrWithoutDemand.viewデータを使用。
 単に縦にsum / pickMaxを行う。
 計算後、fai分け、ERRつけ、toStringを行う。
*/
var serverConfig = require('../serverConfig');
var csvVerticalSumError = require('./csvVerticalSumError');
var csvParams = require('./csvParams');
var calcPower = require('../calcPower');
var csvVerticalSumMax = require('./csvVerticalSumMax');
var csvVerticalSumCalc = require('./csvVerticalSumCalc');

/*
 viewのERR >>> 0に変換したデータ
*/
var calc = [];
/*
*/
var csvVerticalSum = {
    /*
    */
    genVerticalSums: (docs) => {
        csvVerticalSumError.checkErrs(docs);            //計測エラーをフラグ化
        calc = csvVerticalSumCalc.getCalcs(docs);      // viewデータをERR >>> 0に変換  
        csvVerticalSumCalc.calcVerticalSums(calc);
        csvVerticalSumCalc.calcCo2();
        csvVerticalSumMax.pickMax(calc);
    },
    /*
     get calcs
    */
    getCalcs: () => {
        return JSON.parse(JSON.stringify(calc));
    },
    /*
     get 計測エラー
    */
    getErrors: () => {
        return csvVerticalSumError.getErrors();
    },
    /*
     get co2
    */
    getCo2: () => {
        return csvVerticalSumCalc.getCo2();
    },
    /*
     get vertical sum (error convert to 0)
    */
    getVsum: () => {
        return csvVerticalSumCalc.getVsum();
    }
}


module.exports = csvVerticalSum;