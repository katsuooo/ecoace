/*
 csv header
*/
var csvGenHeader = require('./csvGenHeader');

/*
 gen Title label 
*/
function genTitle(){
    var titles = '';
    titles += 'Day,';
    titles += '1pTOTAL(kw),';
    titles += '3pTOTAL(kw),';
    titles += '3pMAX/DAY,'; 
    titles += 'TOTAL(CO2),';

    for (var i=0; i<8; i++){
      titles += 'RE0' + (i+1).toString(10) + '_CH1,';
      titles += 'RE0' + (i+1).toString(10) + '_CH2,';
      titles += 'RE0' + (i+1).toString(10) + '_CH3,';
      titles += 'RE0' + (i+1).toString(10) + '_CH4,';
      titles += 'RE0' + (i+1).toString(10) + '_Output,';
    }
    titles = titles.substr(0, titles.length-1);
    titles += '\r\n';
    return titles;
}
/*
*/
function getHeader(){
    return csvGenHeader.getHeaderByParam();   
}



/*
*/
var csvTitle = {
    /*
     genTitle
    */
    genTitle : () => {
        return genTitle();
    },
    /*
    gen aircon- + title
    */
    getHeader : () => {
        return getHeader();
    }
}


module.exports = csvTitle;






