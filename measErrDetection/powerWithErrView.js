/*
 計測エラーつきパワーデータ view用の作成/保存

 power値 
  子機登録なし : 0
  正常パワー値 : 1f + 3f 値
  計測エラー時 : ERR
 demandの設定にはよらない。
*/


var powerWithErrJson = require('./powerWithErrJson');
var serverConfig = require('../serverConfig');
var powerWithErrTime = require('./powerWithErrTime');
var powerWithErrAggregate = require('./powerWithErrAggregate');
/*
 measErr状態セット
*/
function setMeasErr(){
    return JSON.parse(JSON.stringify(['ERR', 'ERR', 'ERR', 'ERR']));
}
/*
 デマンドオフ状態セット
*/
/*
function setDemmandOff(){
    return JSON.parse(JSON.stringiry([0.0, 0.0, 0.0, 0.0]));
}
*/
/*
 パワー1,3faiの加算

 param  : label c1, c2, ... c8
          powers {power:{}, power3:{}}
*/
function setPowerSum(label, powers){
    var hai = [];
    for(var j=0; j<serverConfig.ADC_NUM; j++){
        var p = powers.power[label][j] + powers.power3[label][j];
        hai.push(p);
    }
    return JSON.parse(JSON.stringify(hai));
}
/*
 1,3 faiデータの加算データ作成
 センサ値
 measErr    = ERR
 正常時     =  1f + 3f 値
*/
function add1and3(powers){
    var sums = {};
    for(var i=0; i<serverConfig.CHILD_NUM; i++){
        var label = 'c' + (i+1).toString(10);
        var newp = [];
        if(serverConfig.measErr[i] != 0){       // 計測エラー時
            newp = setMeasErr();
        }else{                                  // 通常計測時
            newp = setPowerSum(label, powers);
        }
        sums[label] = newp;
    }
    return JSON.parse(JSON.stringify(sums));
}
/*
 個別パワー値を計測ERR評価付きで取得する。
*/
function genPower(){
    var powers = powerWithErrJson.getNoErrPowers(); //1,3fai 仕分け値
    return add1and3(powers);   
}

/*
 viewデータの設定
*/
/*
d format
こんな形でviewデータを保存する。

{ time: '2018-10-11T13:38:18',
  power:
   { c1:
      [ 0.02495740000000001,
        0.0532876,
        0.06799360895999997,
        0.07033028480000002 ],
     c2:
      [ 0.0266438, 0.0532876, 0.07266696063999994, 0.07149862272000003 ],
     c3:
      [ 0.07500363647999998,
        0.06916194688,
        0.06799360895999997,
        0.07500363647999998 ],
     c4: [ 'ERR', 'ERR', 'ERR', 'ERR' ],
     c5: [ 0, 0, 0, 0 ],
     c6: [ 0, 0, 0, 0 ],
     c7: [ 0, 0, 0, 0 ],
     c8: [ 0, 0, 0, 0 ] },
  power_sum_30m: 0,
  power3_sum_30m: 'ERR' }
*/
function genView(){
    var d = {};
    d.time = powerWithErrTime.genStartTime();           // 現在時文字列 >>> 開始時刻にする(-30min)
    d.power = genPower();                               // センサ電力値with計測エラー配列
    d.power_sum_30m = powerWithErrAggregate.power();    // power 1f 集計
    d.power3_sum_30m = powerWithErrAggregate.power3();  // power 3f 集計
    powerWithErrJson.saveViews(JSON.parse(JSON.stringify(d)));
}

/*
*/
var powerWithErrView = {
    /*
     view用データ作成/保存
     power_sum_30m:
     power3_sum_30m:
     co2_sum_30m:
     power : {power:, power3:}
     time  :

    */
    genView : () => {
        genView();
    }
}


module.exports = powerWithErrView;