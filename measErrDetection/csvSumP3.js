/*
 sum power3
*/
var powerWithErrJson = require('./powerWithErrJson');
var csvGenBody = require('./csvGenBody');

function genSumP3(){
    var data = powerWithErrJson.getView(); 
    /*
    var powerFloat = parseFloat(data.power3_sum_30m);
    var roundedPower = powerFloat.toFixed(2);
    return roundedPower;
    */
    return csvGenBody.errCheck(data.power3_sum_30m);
}

var csvSumP3 = {
    /*
    */
    genSumP3 : () => {
        return genSumP3();
    }
}


module.exports = csvSumP3;