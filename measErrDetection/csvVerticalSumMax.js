/*
 max値の取得
*/
var serverConfig = require('../serverConfig');

/*
 vartical max val
*/
var vMax = {
    sum1: 0,
    sum3: 0,
    co2:  0,
    c1: [0,0,0,0],
    c2: [0,0,0,0],
    c3: [0,0,0,0],
    c4: [0,0,0,0],
    c5: [0,0,0,0],
    c6: [0,0,0,0],
    c7: [0,0,0,0],
    c8: [0,0,0,0]  
}
/*
 fai3 power30mのmax値index
*/
var fai3MaxIndex = 0;

/*
 max値のクリア
*/
function clearMax(){
    fai3MaxIndex = 0;
    vMax.sum1 = 0.0;
    vMax.sum3 = 0.0;
    vMax.co2 = 0.0;
    for(var i=0; i<serverConfig.CHILD_NUM; i++){
        var label = 'c' + (i+1).toString(10);
        for(var j=0; j<serverConfig.ADC_NUM; j++){
            vMax[label][j] = 0.0;
        }
    }
}
/*
 縦max値をとる。fai3_sum_30のmaxIndexもとる。
 faiは気にせず、単にview.powerの値のmax値をpickする。

 param  : calc powerWithErrWithoutDemand.viewのERR >>> 0に置き換えたデータ
*/
function pickMax(calc){
    clearMax();

    //console.log(docs[0].powerWithErrWithoutDemand.view);
    calc.forEach( (cc, index) => {   
        if(cc.power_sum_30m > vMax.sum1){
            vMax.sum1 = cc.power_sum_30m;
        }
        if(cc.power3_sum_30m > vMax.sum3){
            vMax.sum3 = cc.power3_sum_30m;
            fai3MaxIndex = index;
        }
        for(var i=0; i<serverConfig.CHILD_NUM; i++){
            var label = 'c' + (i+1).toString(10);        
            for(var j=0; j<serverConfig.ADC_NUM; j++){
                if(cc.power[label][j] > vMax[label][j]){
                    vMax[label][j] = cc.power[label][j];
                }
            }
        }
    });
}
/*
*/
var csvVerticalSumMax = {
    /*
    */
    pickMax: (calc) => {
        pickMax(calc);
    },
    /*
    */
    getMax: () => {
        return JSON.parse(JSON.stringify(vMax));
    }
}

module.exports = csvVerticalSumMax;