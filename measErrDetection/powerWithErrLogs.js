/*
 loggint
*/



var serverLogs = require('../serverLogs');
var powerWithErrJson = require('./powerWithErrJson');
var powerWithErrTime = require('./powerWithErrTime');
var csvMain = require('./csvMain');

/*
 24時flag / powerWithErrTime / genTimeで書き込まれる
*/
/*
var flag24 = false;

function setFlag24(){
    flag24 = true;
}
function clearFlag24(){
    flag24 = false;  
}
*/
/*
 csv 30min
*/
function csv30min(){
    var jd = powerWithErrJson.getAll();
    serverLogs.csv30min(jd, powerWithErrTime.getFlag24());
}
/*
 計測エラーつき
*/
function csv30minWithErr(){
    csvMain.gen30min();
}
/*
*/
var powerWithErrLogs = {
    /*
     per 30min csv append
    */
    csv30min : () => {
        csv30min();
    },
    /*
     with error
    */
    csv30minWithErr: () => {
        csv30minWithErr();
    }
    /*
    setFlag24 : () => {
        setFlag24();
    },
    clearFlag24 : () => {
        clearFlag24();
    }
    */

}

module.exports = powerWithErrLogs;