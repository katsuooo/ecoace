/*
 計測エラー処理がない場合のフッター
 縦Maxなし 空行 + 2行
 ERRの箇所の集計はNaNになる。
*/
var fs = require('fs');
var serverLogsCalc = require('../serverLogsCalc');
var calcpower = require('../calcPower');

/*
 gen footer at no meas err (blank + 2 line)
*/
function genFooter(fname){
    var text;
    var sums = [];
    for(var i=0; i<(4*8+2); i++){
      sums.push(0);
    }
    //console.log(sums);
    text = fs.readFileSync(fname, 'utf8');
    var lines = text.split('\r\n');
    lines.shift();    // first line remove
    lines.pop();      // remove eof
    var calcParams = {};
    calcParams.data = lines;
    calcParams.sums = sums;
    calcParams.maxIndex = 0;
    serverLogsCalc.verticalSum(calcParams);    // calc verticul sum
    console.log('ret:', calcParams);
    /*
      add max
    */
    lines = text.split('\r\n');
    lines.pop();      // remove eof
    //lines[maxIndex+1] += 'MAX' ;    // 1 = add title
    var maxLine = lines[calcParams.maxIndex + 1].split(',');
    maxLine[3] = 'MAX/DAY';
    var newLine = maxLine.join(',');
    lines[calcParams.maxIndex + 1] = newLine;
    /*
      add empty one line over total
    */
    lines.push(',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,');
    /*
      add sums 1fai  [32] power1 [33]power3
    */
    var sumLine = '1pTOTAL(kwh),';
    //sumLine += rounding(2, calcParams.sums[32]) + ',';
    sumLine += calcParams.sums[32].toFixed(2) + ',';
    sumLine += ',';     // 3fai total
    sumLine += ',';     // 3fai max
    //sumLine += rounding(2, calcpower.calcCo2(calcParams.sums[32], 1)) + ','
    sumLine += calcpower.calcCo2(calcParams.sums[32], 1).toFixed(2) + ',';
    for(var child=0; child<8; child++){
      var offset = 4*child;
      for(var i=offset; i<(offset+4); i++){
        if(calcpower.getPowerFai(child, i%4) === 1){
          //sumLine += rounding(2, calcParams.sums[i]);
          sumLine += calcParams.sums[i].toFixed(2);
        }
        sumLine += ',';
      }
      sumLine += ',';    // output line
    }
    lines.push(sumLine);
    /*
      add sums 3fai  [32] power1 [33]power3
    */
    var sumLine3 = '3pTOTAL(kwh),';
    sumLine3 += ',';     // 3fai total
    //sumLine3 += rounding(2, calcParams.sums[33]) + ',';
    sumLine3 += calcParams.sums[33].toFixed(2) + ',';
    sumLine3 += ',';     // 3fai max
    //sumLine3 += rounding(2, calcpower.calcCo2(calcParams.sums[33], 1)) + ',';
    sumLine3 += calcpower.calcCo2(calcParams.sums[33], 1).toFixed(2) + ','
    for(var child=0; child<8; child++){
      var offset = 4*child;
      for(var i=offset; i<(offset+4); i++){
        if(calcpower.getPowerFai(child, i%4) === 3){
          //sumLine3 += rounding(2, calcParams.sums[i]);
          sumLine3 += calcParams.sums[i].toFixed(2);
        }
        sumLine3 += ',';
      }
      sumLine3 += ',';    // output line
    }
    lines.push(sumLine3);
    //console.log(sumLine);
    var newText = '';
    lines.forEach(line => {
      newText += line + '\r\n';
    });
    //console.log(lines);
    fs.writeFileSync(fname, newText);

}
/*
*/
var csvNoErrFooter = {
    /*
    */
    genFooter: (fname) => {
        genFooter(fname);
    }
}

module.exports = csvNoErrFooter;