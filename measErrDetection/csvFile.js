/*
 csv file 確認と生成
*/
var fs = require('fs');
var serverConfig = require('../serverConfig');
var powerWithErrJson = require('./powerWithErrJson');
var csvTitle = require('./csvTitle');
//var csvGenHeader = require('./csvGenHeader');

//var fname = '';     // fileName
/*
 gen fname
*/
function genFname(){
    var data = powerWithErrJson.getView();
    var date = data.time.split('T')[0];
    //console.log('csvFile/genFname date:', data);
    const FILE_DIR_CSV = serverConfig.logs.FILE_DIR_CSV;
    const FILE_CSV30MIN = serverConfig.logs.FILE_CSV30MIN;
    if(!fs.existsSync(FILE_DIR_CSV)){
      fs.mkdirSync(FILE_DIR_CSV);
    }
    fname = FILE_DIR_CSV + FILE_CSV30MIN + date + '.csv';
    return fname;
}

/*
 isFile check & generate
*/
function genFile(){
    var fname = genFname();
    var isCsv = false;
    try {
      fs.accessSync(fname, fs.F_OK);
      isCsv = true;
    } catch (e) {
      isCsv = false;
    }
    /*
     fileがなければ、headerをつくってファイル作成
    */
    if(isCsv === false){
        //fs.appendFileSync(fname, csvTitle.genTitle());    // Day,.... 2line
        fs.appendFileSync(fname, csvTitle.getHeader());      // aircon-.... 3line
    }  
}
/*
 新１行追加
*/
function newLineAppend(line){
    fs.appendFileSync(genFname(), line);
}
/*
 書き換え
*/
function write(newText){
    console.log('write function');
    console.log(genFname());
    fs.writeFileSync(genFname(), newText);
}
/*
*/
var csvFile = {
    /*
     存在確認と生成
    */
    genFile : () => {
        genFile();
    },
    /*
     今30minデータの追記
    */
    newLineAppend : (line) => {
        newLineAppend(line);
    },
    /*
    */
    /*
    getFname: () => {
        return fname;
    },
    */
    /*
     書き換え
    */
    write: (newText) => {
        write(newText);
    }
}


module.exports = csvFile;