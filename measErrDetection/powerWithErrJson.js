/*
 計測エラーつきパワーデータバッファ
*/
require("babel-core").transform("code");	// use babel


var power30min = require('./power30minWithErr.json');
var serverConfig = require('../serverConfig');

/*
 旧データ保存
*/
function saveNoErrs(jpower){
    power30min.time = jpower.time;
    power30min.power_sum_30m = jpower.power_sum_30m;
    power30min.power3_sum_30m = jpower.power3_sum_30m;
    power30min.co2_sum_30m = jpower.co2_sum_30m;
    power30min.power = jpower.power;
    power30min.power3 = jpower.power3;
    power30min.output = jpower.output;    
}

/*
 calc用データ保存
 measErr[], childrenOuterRegistered[]も >>> ほかでserverConfigデータをまとめる
*/
function saveCalcs(powers){
    power30min.powerWithErrWithoutDemand.calc.power = powers.power;
    power30min.powerWithErrWithoutDemand.calc.power3 = powers.power3;   
}
/*
 view用データ保存
 me
*/
function saveViews(viewd){
    power30min.powerWithErrWithoutDemand.view.time = viewd.time;
    power30min.powerWithErrWithoutDemand.view.power = viewd.power;
    power30min.powerWithErrWithoutDemand.view.power_sum_30m = viewd.power_sum_30m;
    power30min.powerWithErrWithoutDemand.view.power3_sum_30m = viewd.power3_sum_30m;
}
/*
 serverConfigデータの保存
*/
function saveConfigs(){
    power30min.powerWithErrWithoutDemand.childrenRegistered = serverConfig.childrenRegistered;
    power30min.powerWithErrWithoutDemand.childrenOuterRegistered = serverConfig.childrenOuterRegistered;
    power30min.powerWithErrWithoutDemand.wconCount = serverConfig.wconCount;
    power30min.powerWithErrWithoutDemand.measErr = serverConfig.measErr;
}
/*
 get all json
*/
function getAll(){
    return JSON.parse(JSON.stringify(power30min));
}

/*
*/
var powerWithErrJson = {
    /*
     計測エラー評価前のデータ保存
     旧データ形式
    */
    saveNoErrs: (jpower) => {
        saveNoErrs(jpower);
    },
    /*
     計算用データの保存
    */
    saveCalcs: (powers) => {
        saveCalcs(powers);
    },
    /*
     view用データの保存
    */
    saveViews: (viewd) => {
        saveViews(viewd);
    },
    /*
     get noErr power & power3
    */
    getNoErrPowers: () => {
        var powers = {}
        powers.power = power30min.power;
        powers.power3 = power30min.power3;
        return JSON.parse(JSON.stringify(powers));
    },
    /*
     get calc powers
     計算用データの取得
    */
    getCalcPowers: () => {
        return JSON.parse(JSON.stringify(power30min.powerWithErrWithoutDemand.calc));
    },
    /*
     get calc power
    */
    getCalcPower: () => {
        return JSON.parse(JSON.stringify(power30min.powerWithErrWithoutDemand.calc.power));
    },
    /*
     get calc power3
    */
    getCalcPower3: () => {
        return JSON.parse(JSON.stringify(power30min.powerWithErrWithoutDemand.calc.power3));
    },
    /*
     serverConfig関連データの保存
    */
    saveConfigs: () => {
        saveConfigs();
    },
    /*
     get all json / power30min.json
    */
    getAll: () => {
        return getAll();
    },
    /*
     get time

     return  : meas end time
    */
    getTime: () => {
        // start time
        //return power30min.powerWithErrWithoutDemand.view.time;
        // stop time
        return power30min.time;
    },
    /*
     get powerWithErr
    */
    getPowerWithErr: () => {
        return JSON.parse(JSON.stringify(power30min.powerWithErrWithoutDemand));
    },
    /*
     get view
    */
    getView: () => {
        return JSON.parse(JSON.stringify(power30min.powerWithErrWithoutDemand.view));
    }
}


module.exports = powerWithErrJson;