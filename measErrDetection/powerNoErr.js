
/*
 計測エラー検知しない、現パワー計算の書き直し
*/
var calcPower = require('../calcPower');
var powerWithErrJson = require('./powerWithErrJson');
var power30minData = require('./power30minData.json');  // 旧30min format
var moment = require('moment');
var serverConfig = require('../serverConfig');

/*
 power 30minの計算
*/
var gen30min = function(powerData){
    //var flag24 = false;   // 24:00 indicator
    var d = JSON.parse(JSON.stringify(power30minData));
    var timestr = moment().format().split('+');  // 2016-11-30T11:02:07
    var checkTime = moment();
    if((checkTime.hours() === 0)&&(checkTime.minutes() === 0)){
      //flag24 = true;
      checkTime.subtract(1, 'days');
      timestr = checkTime.format().split('+');
      timestr[0] = timestr[0].replace('00:00', '24:00');
      d.time = timestr[0];
    }else{
      d.time = timestr[0];
    }
    d.power_sum_30m = calcPower.get30minPowerSum(powerData.power1);
    d.power3_sum_30m = calcPower.get30minPowerSum(powerData.power3);
    //d.co2_sum_30m = calcPower.getCo2(powerData);
    d.co2_sum_30m = '';         // only sum calculate
    d.power = JSON.parse(JSON.stringify(powerData.power1));
    d.power3 = JSON.parse(JSON.stringify(powerData.power3));  
    d.output = serverConfig.childrenOutputs;    

    // powerwithErrJsonに保存
    powerWithErrJson.saveNoErrs(JSON.parse(JSON.stringify(d)));
}




var powerNoErr = {
    /*
     servertoMonto/create30minPowerの前半power計算部
    */
    create30minPower: (powers) => {
        gen30min(powers);
    }
}


module.exports = powerNoErr;