/*
 30minデータの時刻表記
*/
var moment = require('moment');
//var powerWithErrLogs = require('./powerWithErrLogs');

/*
 24:00フラグ
*/
var flag24 = false;

/*
 現在時より2400フラグをセットする。
 取得したmoment()時間を返す
*/
function set24Flag(){
    var mTime = moment();
    //test 0:00
    //mTime.hour(0);
    //mTime.minute(0);
    if((mTime.hours() == 0)&&(mTime.minutes() <= 2)){
        flag24 = true;
    }else{
        flag24 = false;
    }
    return mTime;
}
/*
 30min data time label
　現在時から時間文字列を作成する。
　0:00分取得のデータは24:00のlabelに変換する
  00:00 >>> 24:00の変換は、genStartTiem()から呼ばれたときにエラーとなるので
  コメントにする。　
*/
function genTime(){
    //flag24 = false;   // 24:00 indicator
    var timestr = moment().format().split('+');  // 2016-11-30T11:02:07
    var checkTime = moment();
    var timeLabel = '';
    console.log('pweTime.genTime:', checkTime.format(), 'timestr', timestr, '24flag:', flag24);
    //test setTime()
    //checkTime.hour(0);
    //checkTime.minutes(0); 
    if((checkTime.hours() == 0)&&(checkTime.minutes() <= 2)){
        flag24 = true;
        //powerWithErrLogs.setFlag24();     // 2400 flag set
        checkTime.subtract(1, 'days');
        timestr = checkTime.format().split('+');
        //timestr[0] = timestr[0].replace('00:00', '24:00');
        timeLabel = timestr[0];
    }else{
        flag24 = false;
        //powerWithErrLogs.clearFlag24();       // 2400 flag reset
        timeLabel = timestr[0];
    }
    console.log('24flag_after', flag24, timestr);
    return timeLabel;
}
/*
 計測開始時刻 (現在時-30minの時間)を取得
*/
function genStartTime(){
    //var sTime = moment(genTime());   >>> 24:00がはいった場合エラーになる 
    var sTime = set24Flag();
    var sTimestr = sTime.subtract(30, 'minute').format().split('+');
    return sTimestr[0];
}


/*
*/
var powerWithErrTime = {
    /*
     timeLabel作成
    */
    genTime: () => {
        return genTime();
    },
    /*
     start timeLabel作成 30min前
    */
    genStartTime: () => {
        return genStartTime();
    },
    /*
     24:00フラグの取得
    */
    getFlag24: () => {
        return flag24;
    },
    /*
     for test
    */
    testSetFlag24: () => {
        flag24 = true;
    }
}


module.exports = powerWithErrTime;