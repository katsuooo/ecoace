/*
 csv body data from json
*/
var serverConfig = require('../serverConfig');
/*
 get maxDay index

 旧システムでの取得データと混在でくる場合、旧データは破棄する。
 csvGenAllで破棄する。
*/
function getMaxIndex(docs){
    var maxIndex = 0;
    var maxval = 0.0;
    docs.forEach( (doc, index) => {
        if(doc.powerWithErrWithoutDemand.view.power3_sum_30m === serverConfig.MEAS_ERR_STR){
            // meas
        }
        else if(doc.powerWithErrWithoutDemand.view.power3_sum_30m > maxval){
            maxIndex = index;
            maxval = doc.powerWithErrWithoutDemand.view.power3_sum_30m;
        }
    });
    return maxIndex;
}
/*
 child sensor vals
 
 view.power.cx[1,2,3,4]
 output
  doc.output[0,0,0,0,1,1,1,1] >>> on or off
  doc.powerWithErrWithoutDemand.measErr[0,0,0,0,E,E,E,E] >>> 0: normal, E: error

*/
function getChilds(doc){
    var sensors = '';
    for(var i=0; i<serverConfig.CHILD_NUM; i++){
        var label = 'c' + (i+1).toString(10);
        for(var j=0; j<serverConfig.ADC_NUM; j++){
            if(typeof(doc.powerWithErrWithoutDemand.view.power[label][j]) === 'string'){
                sensors += doc.powerWithErrWithoutDemand.view.power[label][j] + ',';
            }else{
                sensors += doc.powerWithErrWithoutDemand.view.power[label][j].toFixed(2) + ',';
            }
        }
        if(doc.powerWithErrWithoutDemand.measErr[i] !== 0){
            sensors += serverConfig.MEAS_ERR_STR + ',';         // output
        }else{
            if(doc.output[i] === 0){
                sensors += 'off' + ',';         // output
            }else{
                sensors += 'on' + ',';         // output              
            }
        }
    }
    return sensors;
}
/*
 ERR or num

 if num, format .xx
*/
function errCheck(cell){
    if(cell === serverConfig.MEAS_ERR_STR){
        return cell;
    }
    return cell.toFixed(2);
}
/*
 parse one line
 date, 1fsum, 3fsum, 3fmaxday, co2, ch11,ch12, ch13....
*/
function parseOne(doc, maxday){
    var line = '';
    /*
     time label 0:00-23:00
    */
    //line += doc.powerWithErrWithoutDemand.view.time + ',';          // time
    /*
     time label 0:30-24:00
    */
    line += doc.time + ',';          // time
    line += errCheck(doc.powerWithErrWithoutDemand.view.power_sum_30m) + ','; // p1sum
    line += errCheck(doc.powerWithErrWithoutDemand.view.power3_sum_30m) + ','; // p3sum    
    if(maxday === 1){
        line +=  serverConfig.MAXDAYSTR + ',';                      // p3maxday
    }else{
        line += ',';
    }
    line += '-,';           // co2
    line += getChilds(doc);
    return line;
}
/*
 gen body
*/
function genBody(docs){
    var maxIndex = getMaxIndex(docs);
    var lines = '';
    docs.forEach( (doc, index) => {
        var match = 0;
        if(index === maxIndex){
            match = 1;
        }
        lines += parseOne(doc, match) + '\r\n';
    })
    return lines;
}
/*
*/
var csvGenBody = {
    /*
     csv body作成

     param  : docs >>> mongo power-xxxx json配列
    */
    genBody : (docs) => {
        return genBody(docs);
    },
    /*
     err check

     'ERR'処理
    */
    errCheck : (cell) => {
        return errCheck(cell);
    }
}

module.exports = csvGenBody;