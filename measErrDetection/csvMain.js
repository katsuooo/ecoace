/*
 csv (計測エラー付き)　作成
 デマンドデータの追加(TOP)
 maxvalの追加(end)
 timeの30min引き

 生成と30minアペンドのみ
 最終集計(2400)はcsvFooterMainで制御。
*/

var powerWithErrTime = require('./powerWithErrTime');
var csvFile = require('./csvFile')
var csvLabel = require('./csvLabel');
var csvSumP1 = require('./csvSumP1');
var csvSumP3 = require('./csvSumP3');
var csvTime = require('./csvTime');

var csvSensorPowers = require('./csvSensorPowers');
var csvNoErrFooter = require('./csvNoErrFooter');



/*
 今30minのラインデータ作成
*/
function genCurrentLine(){
    var pstr = '';    // filing data
    pstr += csvTime.genNowTime() + ',';     // 時刻作成
    pstr += csvSumP1.genSumP1() + ',';      // fai1 sum30
    pstr += csvSumP3.genSumP3() + ',';      // fai3 sum30
    pstr += ',';                                          // add max culumn    
    pstr += '-,';                           // co2 dummy
    pstr += csvSensorPowers.genSensorPowers() + '\r\n';
    return pstr;
}


var csvMain = {
    /*
     30min append
    */
    gen30min : () => {
        csvFile.genFile();      // file生成
        csvFile.newLineAppend(genCurrentLine());
        /*
        if(powerWithErrTime.getFlag24()){
            //csvVerticalSum.verticalSums();
            csvNoErrFooter.genFooter(csvFile.getFname());
        }
        */
    }
}


module.exports = csvMain;