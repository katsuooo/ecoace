﻿/*
160515 kdesign

  time check

1s interval
pre min, exection

*/

console.log('server_timer1');
var moment = require('moment');
var servertoMongo = require('./servertoMongo');
var uart = require('./uart');
var events = require('./ecoEvents');
var dousaMode = require('./dousaMode');
var serverConfig = require('./serverConfig');

/*
  config change Event


*/


/*
  config changeEcent

   mongo confign re-read
   uart  to parent re-load

  param  : none
  return : none

*/
events.ev.on('configChangeEvent', () => {
  console.log('configChangeEvent');
  //getConfigs();    // config read again
  servertoMongo.resetConfigReady();
  uart.resetConfigtoParent();
  servertoMongo.getConfigAgain();   // reget
});





/*
  save 1min data
*/

var testData = {
  time: '',
  childs:[
   {no:'1', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'2', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'3', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'4', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'5', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'6', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'7', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'8', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'}
  ]
};


/*
 1min data create

 from parent unit data
*/
function create1min(){
  var timestr = moment().format().split('+');
  testData.time = timestr[0];
  var djson = JSON.parse(JSON.stringify(testData));
  servertoMongo.create1min(djson);
  //servertoMongo.cache1min(djson);
  //serverLogs.log1min(JSON.stringify(testData));
}

/*
 30min data

 caluculate 30min data
*/
function save30min(){
  servertoMongo.read30min();
}


/*
 sequence 1min

  parameter refresh
  create 1min data
  save 1min data
*/
function sequence1min(){
  var timestr = moment().format().split('+');
  testData.time = timestr[0];
  var djson = JSON.parse(JSON.stringify(testData));
  servertoMongo.save1min(djson);
}

/*
 sequence 30min

  parameter refresh
  create 1min data >>> keep
  create & save 30min data
  save 1min data
*/
function sequence30min(){
  var timestr = moment().format().split('+');
  testData.time = timestr[0];
  var djson = JSON.parse(JSON.stringify(testData));
  servertoMongo.seq30min(djson);
}

/*
 interval process sequence

 per 1min or per 30min

/// 1min
  parameter refresh
  create 1min data
  save 1min data

/// 30min
  parameter refresh
  create 1min data >>> keep
  create & save 30min data
  save 1min data
*/

function sequenceGo(minTime){
  //uart.getAdcs(minTime);
  uart.setOutputs(minTime);


  /*
  var timestr = moment().format().split('+');
  testData.time = timestr[0];
  var djson = JSON.parse(JSON.stringify(testData));
  servertoMongo.seqGo(minTime, djson);
  */
}






/*
 initialize
*/

var lastMinute = moment().format('mm');		// last minute data initialize
var lastDay    = moment().date();   // last day data initialize



/*
 timer scheduler

 parent access & save to mongo 1min children data
*/
function server_timer(){

  setInterval(function(){
    if(servertoMongo.getConfigReady() === false){   // wait setting config datas
      return;
    }
    // uart check  
    if(serverConfig.uartAvailable === false){
      uart.uartRestart();
      return;   // open wait
    }
    if(uart.getConfigtoParent() === false){
      uart.setConfig();
      return;
    }
    var now = moment();
    var nowMinute = now.format('mm');
    var nowDay = now.date();
    var nowSec = now.second();
    //test
    //nowMinute = '00'; lastMinute = '01'; lastDay = 0;
    if(nowMinute !== lastMinute){
      lastMinute = nowMinute;
      //test
      //nowMinute = '00';
      //test + 3line
      //if(nowMinute === '47'){   // testで条件追加
      //  sequenceGo(30);	
      //} else 
      if(nowMinute === '00') {
//      if(nowMinute % 2){		// test
        //test
        //lastDay = 0;
        /*
          change day check
        */
        //console.log('days:', nowDay, lastDay);
        if(nowDay !== lastDay){
          lastDay = nowDay;
          dousaMode.checkDousaMode();       // testMode check
          servertoMongo.deleatLastDay();    // 3days ago data delete
        }
        sequenceGo(30);		// 1min read, 30min save, 1min save sequence
      }else if(nowMinute === '30') {
        sequenceGo(30);		// 1min read, 30min save, 1min save sequence
      }else{
        sequenceGo(1);		// 1min only sequence
      }
    }else{
      /*
      read per 10 sec
      */
      console.log(nowSec);
      if(nowSec % 10 === 0){
        console.log('secGo');
        sequenceGo(0);
      }
    }
    //test no wait
         //sequenceGo(1);		// 1min only sequence
         //sequenceGo(30);
  }, 1000);
  //}, 2000);
}




module.exports = server_timer;
