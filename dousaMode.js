/*
  dousa mode control

*/


var servertoMongo = require('./servertoMongo');
var serverConfig = require('./serverConfig');		// system defines


/*
  dousa mode check

  param  : none
  return : none
*/
class dousaMode_proto{
  checkDousaMode(){
    console.log('dousa-mode check');
    if(serverConfig.dousaMode === 'test'){
      if(serverConfig.demandCtrl === false){
        serverConfig.demandCtrl = true;
      }else{
        serverConfig.demandCtrl = false;
      }
      servertoMongo.setDemandCtrl(serverConfig.demandCtrl);
    }
  }
}

var dousaMode = new dousaMode_proto();

module.exports = dousaMode;
