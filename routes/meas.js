﻿var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('meas', {title: '計測データ | ecoAce'});
});

module.exports = router;
