﻿/*
160518 kdesign

  calc power consumption


*/

var serverConfig = require('./serverConfig');		// system defines


/*
  children power calucuration parameter

  pf : power factor - 80-100
  ac : ac-type - 3pai-200, 1pai-200, 1pai-100
  sensor : current-range - 50a, 100a, 200a, 500a
  co : coeficient - 3pai=1.732, 1pai=1.0

*/
/*
var pw_params = [
  {ac:[200,200,200,200], sensor:[100,100,100,100], pf:[1,1,1,1], co:[1.0, 1.0, 1.0, 1.0]},
  {ac:[200,200,200,200], sensor:[200,100,100,100], pf:[1,1,1,1], co:[1.0, 1.0, 1.0, 1.0]},
  {ac:[200,200,200,200], sensor:[300,100,100,100], pf:[1,1,1,1], co:[1.0, 1.0, 1.0, 1.0]},
  {ac:[200,200,200,200], sensor:[400,100,100,100], pf:[1,1,1,1], co:[1.0, 1.0, 1.0, 1.0]},
  {ac:[200,200,200,200], sensor:[500,100,100,100], pf:[1,1,1,1], co:[1.0, 1.0, 1.0, 1.0]},
  {ac:[200,200,200,200], sensor:[600,100,100,100], pf:[1,1,1,1], co:[1.0, 1.0, 1.0, 1.0]},
  {ac:[200,200,200,200], sensor:[700,100,100,100], pf:[1,1,1,1], co:[1.0, 1.0, 1.0, 1.0]},
  {ac:[200,200,200,200], sensor:[800,100,100,100], pf:[1,1,1,1], co:[1.0, 1.0, 1.0, 1.0]}
];
*/
/*
  add fai param
*/
var pw_params = [
  {ac:[200,200,200,200], sensor:[100,100,100,100], pf:[1,1,1,1], co:[1.0, 1.0, 1.0, 1.0], fai:[3,3,3,3]},
  {ac:[200,200,200,200], sensor:[200,100,100,100], pf:[1,1,1,1], co:[1.0, 1.0, 1.0, 1.0], fai:[3,3,3,3]},
  {ac:[200,200,200,200], sensor:[300,100,100,100], pf:[1,1,1,1], co:[1.0, 1.0, 1.0, 1.0], fai:[3,3,3,3]},
  {ac:[200,200,200,200], sensor:[400,100,100,100], pf:[1,1,1,1], co:[1.0, 1.0, 1.0, 1.0], fai:[3,3,3,3]},
  {ac:[200,200,200,200], sensor:[500,100,100,100], pf:[1,1,1,1], co:[1.0, 1.0, 1.0, 1.0], fai:[3,3,3,3]},
  {ac:[200,200,200,200], sensor:[600,100,100,100], pf:[1,1,1,1], co:[1.0, 1.0, 1.0, 1.0], fai:[3,3,3,3]},
  {ac:[200,200,200,200], sensor:[700,100,100,100], pf:[1,1,1,1], co:[1.0, 1.0, 1.0, 1.0], fai:[3,3,3,3]},
  {ac:[200,200,200,200], sensor:[800,100,100,100], pf:[1,1,1,1], co:[1.0, 1.0, 1.0, 1.0], fai:[3,3,3,3]}
];
/*
 co2 conversion param
*/
var co2_param = {
  phi1: 0.3145,
  phi3: 0.3146
}

/*
 adc param

  adc_ref_vol = 2.048;
  sensor_range_maxv = 1.7921v
  adc_bit_num = 16;
  adc_max_bin = 0x7fff;

>>> adc_ref_vol / 2^adc_bit_num = 2.0 / 65536 / 2

*/
var adc_ref_vol = 2.048;          // v
var sensor_range_maxv = 1.7921;   // v
var sensor_range_maxbin = (sensor_range_maxv / adc_ref_vol) * Math.pow(2, 15);  // range max bin code











/*
 kw >>> kwh conversion param

*/
var convKWH = 0.5;		// coeficient for 30minutes






/*
 adc val to power convert

 @param: child,  child no    (int)
         chan,   adc channel (int)
         adc,    adc val     (int)
 return: power one min
*/
/*
function adcToPower(child, chan, adc){
  var sensor_range = pw_params[child].sensor[chan];			// cur sensor range
  var ac_vol = pw_params[child].ac[chan];					// ac voltage
  var power_factor = pw_params[child].pf[chan];		// power factor
  var adc_refvol = 2.048;														// adc reference voltage
  var sensor_rated_v = 5.0;												// sensor rated output voltage
  var phi_co = pw_params[child].co[chan];				  // phi cofficient
  //var power = (sensor / sensor_rated_v) * (sensor_rated_v / adc_refvol);
  var adc_vol = adc * adc_co;
  var current = sensor_range * (adc_vol / adc_refvol);
  var power = current * ac_vol * phi_co * power_factor / 1000.0;	// kw

  return power;
}
*/
/*
 161018 henkou
*/
/*
function adcToPower(child, chan, adc){
  var sensor_range = pw_params[child].sensor[chan];			// cur sensor range
  var ac_vol = pw_params[child].ac[chan];					// ac voltage
  var power_factor = pw_params[child].pf[chan];		// power factor
  //var adc_refvol = 2.048;														// adc reference voltage
  //var sensor_rated_v = 5.0;												// sensor rated output voltage
  var phi_co = pw_params[child].co[chan];				  // phi cofficient
  //var power = (sensor / sensor_rated_v) * (sensor_rated_v / adc_refvol);
  //var adc_vol = adc * adc_co;

  var current = sensor_range * (adc / sensor_range_maxbin);
  //console.log('cur', current);
  var power = current * ac_vol * phi_co * power_factor / 1000.0;	// kw
  //console.log('pw', power);
  return power;
}
*/
function adcToPower(child, chan, adc){
  var sensor_range = pw_params[child].sensor[chan] / 100.0;			// cur sensor range / current equotion is at 100A range
  var ac_vol = pw_params[child].ac[chan];					// ac voltage
  var power_factor = pw_params[child].pf[chan];		// power factor
  var phi_co = pw_params[child].co[chan];				  // phi cofficient
  //var current = sensor_range * (adc / sensor_range_maxbin);
  /*
  current = (0.004216 * adc - 0.17026) is 100a range equotion
  other range, summing range / 100  >>> sensor_range value is already devided 100
  */
  //var current = (0.004216 * adc - 0.17026) * sensor_range;    // 100a current * sensor_range / 100
  var current = (0.004216 * adc - 0.104570) * sensor_range;    // 100a current * sensor_range / 100  
  if(current < 0){
    current = 0.0;
  }
  // 3fai ac_vol *= 1.732
  if(pw_params[child].fai[chan] === 3){
    ac_vol *= 1.732;
  }
  var power = current * ac_vol * phi_co * power_factor / 1000.0;	// kw
console.log('[', child, chan, '] ', adc, sensor_range, ac_vol, power_factor, phi_co, power);
  return power;
}





/*
  averaging power

  one child power[1-4]
*/









var calcPower = {

/*
 create parameter table

 chidlens setting parameter re-load

input: json setting data
return: non
*/
  setChildrenParams: function(docs){
    docs.children.forEach(function(child){			// child loop
      var no = parseInt(child.child, 10) - 1;
      child.sensors.forEach(function(channel){	// adc channel loop
        var chan = parseInt(channel.chan, 10) - 1;
        //phase check (3 or1)
        var testchar = channel.ac_type.slice(1,2);		// 3phi-xxx  or 1phi-xxx
        //console.log('p-phy', testchar);
        if(testchar === '3'){
          pw_params[no].pf[chan] = parseInt(docs.power_factor, 10)/100;	// 3phi >>> set power_factor param
          co2_param.phi3 = parseFloat(docs.co2_cofficient.phi3_ac200100v);			// co2 param
        }else {
          pw_params[no].pf[chan] = 1;																// 1phi >>> fix 1
          co2_param.phi1 = parseFloat(docs.co2_cofficient.phi1_ac100v);	// co2 param
        }
        //voltage set
        testchar = channel.ac_type.split('-');
        pw_params[no].ac[chan] = parseInt(testchar[1].slice(0, -1));		// 100v or 200v ,cut 'v' and convert int
        //fai
        pw_params[no].fai[chan] = parseInt(testchar[0].slice(1, 2));   // fai 1 or 3
        //sensor-type set
        if(channel.disable === true){
          pw_params[no].sensor[chan] = 0;       // sensor disable
        }else{
          pw_params[no].sensor[chan] = parseInt(channel.sensor.slice(0, -1));	// remove 'A' and convert int
        }
/*
        // ac-type check
        if(channel.ac === '3phi-200'){
          cparams.children[no].ac[chan] = 200;
					cparams.children[no].pfactor[chan] = perseInt(channel.power_factor, 10)/100;
        }else if(channel.ac === '1phi-200'){
          cparams.children[no].ac[chan] = 200;
          cparams.children[no].pfactor[chan] = 1;
        }else {
          cparams.children[no].ac[chan] = 100;
          cparams.children[no].pfactor[chan] = 1;
        }
        // sensor-type check
        if(channel.sensor === '500A'){
	        cparams.children[no].sensor[chan] = 500;
        }else if(channel.sensor === '250A'){
	        cparams.children[no].sensor[chan] = 250;
        }else {
        }
*/
      });
    });
    //console.log(cparams);
  },



/*
  calc 1min power

input
var testData = {
  time: '',
  childs:[
   {no:'1', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'2', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'3', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'4', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'5', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'6', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'7', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'8', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'}
  ]
};

return:
var testData = {
  time: '',
  childs:[
   {no:'1', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'2', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'3', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'4', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'5', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'6', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'7', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'},
   {no:'8', adc:['1234','1234','abcd','fefe'], output:'on', rssi:'32', channels:'18'}
  ]
};
*/
/*
 calc 1min data power from adc-val

*/
  calcNowPower: function(jdata){
    var pdata = JSON.parse(JSON.stringify(jdata));	//deepcopy
    jdata.childs.forEach(function(d, index1){
      var result = [];
      d.adc.forEach(function(dd, index){
        var adc = parseInt(dd, 16);
        var power = adcToPower(parseInt(d.no)-1, index, adc);	// (child_no, channel, adc val)
        result.push(power);
      });
      delete pdata.childs[index1].adc;
      var power = result.slice();
      pdata.childs[index1].power = power;
    });
    return pdata;
  },

/*
 calc 30min power

 params : jdata 1min power data
 return : power_1fai, power_3fai sum separatly
*/
/*
  calc30min : function(jdata){
    //console.log(jdata);
    //console.log(jdata[100]);
    var num;	// power data num;
    var power_sum = new Array();
    var hai = new Array();
    for(var j=0; j<serverConfig.ADC_NUM; j++){
      hai.push(0.0);
    }
    for(var i=0; i<serverConfig.CHILD_NUM; i++){
      power_sum.push(hai.slice(0));	// 8child * 4chan data , deep copy
    }
    var avr_num = 0;
    jdata.forEach(function(d){	// root
      if ('childs' in d){
        var isPower = false;
        d.childs.forEach(function(dd){	           //childs loop
          if ('power' in dd){
            isPower = true;
            var no = parseInt(dd.no) - 1;					// child no
            for(var j=0; j<serverConfig.ADC_NUM; j++){	// adc chan loop
              power_sum[no][j] += dd.power[j];
            }
          }
        });
        if(isPower){
          avr_num++;						// data num count with existing check
        }
      }
    });
    //console.log('ps',power_sum);
    var power_avr = {};
    for(i=0; i<serverConfig.CHILD_NUM; i++){
      var hai = [];
      for(j=0; j<serverConfig.ADC_NUM; j++){
        hai.push(power_sum[i][j] / avr_num * convKWH);	// power_sum / dnum * hour
      }
      var keyname = 'c' + (i+1).toString(10);
      power_avr[keyname] = hai.slice(0);
    }
    return JSON.parse(JSON.stringify(power_avr))
  },
*/

/*
 calc 30min separately

 param  : 1min power data
 return : powers = {
   power1fai, power3fai}
*/
  calc30min : function(jdata){
    //console.log(jdata);
    //console.log(jdata[100]);
    var num;	// power data num;
    var power1_sum = new Array();
    var power3_sum = new Array();    
    var hai = new Array();
    for(var j=0; j<serverConfig.ADC_NUM; j++){
      hai.push(0.0); 
    }
    for(var i=0; i<serverConfig.CHILD_NUM; i++){
      power1_sum.push(hai.slice(0));	// 8child * 4chan data , deep copy
      power3_sum.push(hai.slice(0));	// 8child * 4chan data , deep copy      
    }
    var avr_num = 0;    // 1min data num (only power data exist) 
    jdata.forEach(function(d){	// root
      if ('childs' in d){
        var isPower = false;
        d.childs.forEach(function(dd){	           //childs loop
          if ('power' in dd){
            isPower = true;
            var no = parseInt(dd.no) - 1;					// child no
            for(var j=0; j<serverConfig.ADC_NUM; j++){	// adc chan loop
              if(pw_params[no].fai[j] == 1){
                power1_sum[no][j] += dd.power[j];
                power3_sum[no][j] = 0;
              }else{
                power1_sum[no][j] = 0;
                power3_sum[no][j] += dd.power[j];               
              }
            }
          }
        });
        if(isPower){
          avr_num++;						// data num count with existing check
        }
      }
    });
    //console.log('ps',power_sum);
    var power1_avr = {};
    var power3_avr = {};    
    for(i=0; i<serverConfig.CHILD_NUM; i++){
      var hai1 = [];
      var hai3 = [];      
      for(j=0; j<serverConfig.ADC_NUM; j++){
        //hai.push(power1_sum[i][j] / avr_num * convKWH);	// power_sum / dnum * hour
        hai1.push(power1_sum[i][j] / avr_num );	// power_sum / dnum (kw)
        hai3.push(power3_sum[i][j] / avr_num );	// power_sum / dnum (kw)        
      }
      var keyname = 'c' + (i+1).toString(10);
      power1_avr[keyname] = hai1.slice(0);
      power3_avr[keyname] = hai3.slice(0);      
    }
    powers = {};
    powers.power1 = power1_avr;
    powers.power3 = power3_avr;  
    return JSON.parse(JSON.stringify(powers))
  },
/*
  power sum 30min

@param: input json
  'power': {
    'c1':[10.01,10.02,10.03,10.04],
    'c2':[10.01,10.02,10.03,10.04],
    'c3':[10.01,10.02,10.03,10.04],
    'c4':[10.01,10.02,10.03,10.04],
    'c5':[10.01,10.02,10.03,10.04],
    'c6':[10.01,10.02,10.03,10.04],
    'c7':[10.01,10.02,10.03,10.04],
    'c8':[10.01,10.02,10.03,10.04]
  },

return: all sum value
*/
  get30minPowerSum : function(powerData){
    //console.log('powerD',powerData);
    var power_sum = 0.0;
    for(var j=0; j<serverConfig.CHILD_NUM; j++){
      var keyname = 'c'+ (j+1).toString();
      var hai = powerData[keyname];
      for(var i=0; i<hai.length; i++){
        power_sum += hai[i];
      }
    }
    return power_sum;
  },


/*
  co2 conversion

*/
  getCo2 : function(powerData){
    var co2_sum = 0.0;
    for(var j=0; j<serverConfig.CHILD_NUM; j++){
      var keyname = 'c'+ (j+1).toString();
      var hai = powerData[keyname];
      for(var i=0; i<hai.length; i++){
        if(pw_params[j].ac[i] === 100){
          co2_sum += hai[i] * co2_param.phi1;
        }else {
          co2_sum += hai[i] * co2_param.phi3;
        }
      }
    }
    console.log('co2 sum',co2_sum,co2_param);
    return co2_sum;
  },

  /*
    get power fai
  */
  getPowerFai : function(childNo, adcNo){
    return pw_params[childNo].fai[adcNo];
  },
/*
  co2 calculation from power

*/
  calcCo2 : function(power, fai){
    var co2Val;
    if(fai === 1){
      co2Val = power * co2_param.phi1;
    }else {
      co2Val = power * co2_param.phi3;
    }
    return co2Val;
  },
  /*
    get power fai add
  */
  /*
  getPowerFai : function(){
    var fais = [];
    for(var i=0; i<serverConfig.CHILD_NUM; i++){
      var faisSub = [];
      for(var j=0; j<serverConfig.ADC_NUM; j++){
        faisSub.push(pw_params[i].fai[])
      }
    }
    return pw_params[childNo].fai[adcNo];
  }
  */
}







module.exports = calcPower;
