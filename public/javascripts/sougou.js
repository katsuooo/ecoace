﻿/*
160512 kdesign

 sougou page control
*/


/*
 config data
*/
var ecoClientConfig = {
  sensorNum : 4       // one child's sensor number
};

var katsuooo = 1;
console.log('read sougou');

var app = angular.module('ecoAceApp', []);




/***********************************************************
 sougou page controller
*/
// test data

var sougou = [
  {name:'子機番号', child1:'1', child2:'2', child3:'3',child4:'4',child5:'5',child6:'6',child7:'7',child8:'8'},
  {name:'交信状況', child1:'OK', child2:'-', child3:'OK',child4:'-',child5:'OK',child6:'-',child7:'OK',child8:'-'},
  {name:'センサ入力1', child1:'10.11', child2:'10.12', child3:'10.13',child4:'10.14',child5:'10.15',child6:'10.16',child7:'10.17',child8:'10.18'},
  {name:'センサ入力2', child1:'10.21', child2:'10.22', child3:'10.23',child4:'10.24',child5:'10.25',child6:'10.26',child7:'10.27',child8:'10.28'},
  {name:'センサ入力3', child1:'10.31', child2:'10.32', child3:'10.33',child4:'10.34',child5:'10.35',child6:'10.36',child7:'10.37',child8:'10.38'},
  {name:'センサ入力4', child1:'10.41', child2:'10.42', child3:'10.43',child4:'10.44',child5:'10.45',child6:'10.46',child7:'10.47',child8:'10.48'},
  {name:'エアコン設定', child1:'エアコン室外', child2:'エアコン室外', child3:'エアコン室外',child4:'エアコン室外',child5:'エアコン室外',child6:'エアコン室外',child7:'エアコン室外',child8:'エアコン室外'},
  {name:'タイマ設定', child1:'有', child2:'有', child3:'有',child4:'有',child5:'有',child6:'有',child7:'有',child8:'有'},
  {name:'タイマON時刻', child1:'8:00', child2:'8:00', child3:'8:00',child4:'8:00',child5:'8:00',child6:'8:00',child7:'8:00',child8:'8:00'},
  {name:'タイマOFF時刻', child1:'17:00', child2:'17:00', child3:'17:00',child4:'17:00',child5:'17:00',child6:'17:00',child7:'17:00',child8:'17:00'},
  {name:'現在のエアコン状態', child1:'ON', child2:'OFF', child3:'ON',child4:'OFF',child5:'ON',child6:'OFF',child7:'ON',child8:'OFF'}
];



/*
  soubou child no register

    param  : child : child json data
             label : view data index
    return : none
*/
function sougouNoRegisteredChild(child, label, $scope){
  $scope.sougou[1][label] = '-';
  $scope.sougou[2][label] = (Math.round(0)).toFixed(2);
  $scope.sougou[3][label] = (Math.round(0)).toFixed(2);
  $scope.sougou[4][label] = (Math.round(0)).toFixed(2);
  $scope.sougou[5][label] = (Math.round(0)).toFixed(2);
  $scope.sougou[6][label] = '-';
  $scope.sougou[7][label] = '-';
  $scope.sougou[8][label] = '--:--';
  $scope.sougou[9][label] = '--:--';
  $scope.sougou[10][label] = '-';
}
/*
 sougou set childs

   param  : child : child json data
            label : view data index
            $scope :
            outerRegistered : demand reg info
   return : none
*/
var clientUartState = false;  // uart connection, set in headerCont

function sougouSetChild(child, label, $scope, outerRegistered){
  //交信状況  
  //console.log(child.wcon, label);
  if (clientUartState === false) {
    $scope.sougou[1][label] = '×';
  }else if(child.wcon === '0'){
    $scope.sougou[1][label] = '×';
  }else if(child.wcon === '1'){
    $scope.sougou[1][label] = '中';
  }else{
    $scope.sougou[1][label] = '良';
  }

  /*
  if(child.)
    $scope.sougou[1][label]
*/

  //入力チャンネル1-4
  $scope.sougou[2][label] = (Math.round(child.power[0]*100)/100).toFixed(2);
  $scope.sougou[3][label] = (Math.round(child.power[1]*100)/100).toFixed(2);
  $scope.sougou[4][label] = (Math.round(child.power[2]*100)/100).toFixed(2);
  $scope.sougou[5][label] = (Math.round(child.power[3]*100)/100).toFixed(2);
  //エアコンセーブ状態
  if(outerRegistered === 1){
    if(child.output == 1){
      $scope.sougou[10][label] = 'エアコンON';
    }else{
      $scope.sougou[10][label] = 'OFF';
    }
  }else{
    $scope.sougou[10][label] = '-';
  }
}




/*
 sougou controller




*/


app.controller('sougouCont', function($scope, socket){
  $scope.test = 'test';
  $scope.sougou = sougou;
  var childRegistered = [0,0,0,0,0,0,0,0];        // child touroku
  var childOuterRegistered = [0,0,0,0,0,0,0,0];   // child demand touroku

  /*
   latest data
  */

  socket.on('r_readLatest', function(docs){
    console.log('latest',docs);
    console.log(childRegistered);
    docs.childs.forEach( (child, index) => {
      //console.log(child.power);
      var label = 'child' + child.no.toString(10);
      //console.log(typeof(child.power[0]));

      //登録
      if( childRegistered[index] === 0){
        sougouNoRegisteredChild(child, label, $scope);
        //$scope.sougou[1][label] = '-';
      }else{
        console.log(child, childOuterRegistered[index] )
        sougouSetChild(child, label, $scope, childOuterRegistered[index]);
      //}
/*
      //交信状況
      if(child.wcon === '0'){
        $scope.sougou[1][label] = '×';
      }else if(child.wcon === '1'){
        $scope.sougou[1][label] = '中';
      }else{
        $scope.sougou[1][label] = '良';
      }



      //入力チャンネル1-4
      $scope.sougou[2][label] = (Math.round(child.power[0]*100)/100).toFixed(2);
      $scope.sougou[3][label] = (Math.round(child.power[1]*100)/100).toFixed(2);
      $scope.sougou[4][label] = (Math.round(child.power[2]*100)/100).toFixed(2);
      $scope.sougou[5][label] = (Math.round(child.power[3]*100)/100).toFixed(2);
      //エアコンセーブ状態
      if(child.output == 1){
        $scope.sougou[9][label] = 'セーブ中';
      }else{
        $scope.sougou[9][label] = '-';
      }
    }
    */
      }

    });
  });
  /*
   config data
  */
  socket.emit('readConfig');  // setting data read
  socket.on('r_readConfig', function(docs){

    docs.children.forEach( (childx, index) =>{
      var label = 'child' + childx.child.toString(10);
      //登録
      if(childx.registered === 'yes'){
        childRegistered[index] = 1;
      }else{
        childRegistered[index] = 0;
      }

      //出力設定
      if(childx.output.registered === 'yes'){
        childOuterRegistered[index] = 1;
        $scope.sougou[6][label] = '有';
        if(childx.output.timerEna === 'yes'){
          $scope.sougou[7][label] = '有';
          $scope.sougou[8][label] = childx.output.start;
          $scope.sougou[9][label] = childx.output.stop;
        } else{
          $scope.sougou[7][label] = '無';
          $scope.sougou[8][label] = '--:--';
          $scope.sougou[9][label] = '--:--';
        }
      } else{
        childOuterRegistered[index] = 0;
        $scope.sougou[6][label] = '無';
        $scope.sougou[7][label] = '-';
        $scope.sougou[8][label] = '--:--';
        $scope.sougou[9][label] = '--:--';
      }

    });
    socket.emit('readLatest');  // setting data read
  });
   /*
    table class
  */
  $scope.sougouTblCls = function(name){
    if(name === '子機番号' | name === '交信状況'){
      return 'sougouTableHead';
    }
    return '';
  }
  /*
    on label
  */
  $scope.sougouOnLabel = function(text, index){
    /*
    if((text === 'ON') || (text === 'on')){
	    return {
	      'label': true,
	      'label-pill': true,
	      'label-success': true,
        'on_anime': true,
			};
    }
    else if((text === 'OK') || (text === 'ok')){
	    return {
	      'label': true,
	      'label-pill': true,
	      'label-success': true,
        'ok_anime': true
			};
      */
    if((index === 0)){
      return {
        'noTextArea': true
      };
    }
    /*
    kousin jyoukyou
    */
    if((index === 1)&&(text === '良')){
      return {
	      'label': true,
	      'label-pill': true,
	      'label-success': true,
        'on_anime': true,
        'sougouTableBody': true
			};
    }else if((index === 1)&&(text === '中')){
      return {
        'label': true,
        'label-pill': true,
        'label-warning': true,
        'sougouTableBody': true
        //'on_anime': true,
      };
    }else if((index === 1)&&(text === '×')){
      return {
        'label': true,
        'label-pill': true,
        'label-danger': true,
        'sougouTableBody': true
        //'on_anime': true,
      };
    } else if((index === 7) && (text = '-')){
      return {

      }
    }
    if((index === 10)&&(text === 'エアコンON')){
      return {
        'label': true,
        'label-pill': true,
        'label-info': true,
        'saveState' : true,
        'sougouTableBodyOuter': true
        //'text-size': 0.8rem
        //'on_anime': true,
      };
    } /*else if((index === 10)&&(text === '-')){
      return {
        'addTextArea': true
      };
    } else if((index === 10)&&(text === 'OFF')){
      return {
        'addTextArea2': true
      };
    }
*/
    return '';
  }
  /*
    text disable
  */
  /*
  $scope.sougouOnLabel = function(text){
    if((text === 'ON') || (text === 'on')){
      return {
        'label': true,
        'label-pill': true,
        'label-success': true,
        'on_anime': true,
      };
    }
    else if((text === 'OK') || (text === 'ok')){
      return {
        'label': true,
        'label-pill': true,
        'label-success': true,
        'ok_anime': true
      };
    }
    return '';
  }
  */
  /*
   latest data refresh
  */
  socket.on('refreshLatest', (powerData) =>{
    console.log(powerData);
  });
  /*
  uartState check
  if uartState = false, kousin >>> all ×
  */
  
//  socket.on('uartStateSougou', (uartState)=>{
  socket.on('uartState', (uartState)=>{  
    console.log('uart change_sougou: ', uartState);
    if(uartState === false){
      clientUartState = false;
      for(var i=1; i<9; i++){
        var label = 'child' + i.toString(10);
        if(($scope.sougou[1][label] === '良') || ($scope.sougou[1][label] === '中')){
          $scope.sougou[1][label] = '×';
        }
      }
    } else {
      clientUartState = true;      
    }
  });
  
});











/*******************************************************************************************
parent controller
*/
app.controller('parentCont', function($scope, socket){
  console.log('parent controller');
  $scope.child1_registered = 'set';
  $scope.child2_registered = 'set';
  $scope.child3_registered = 'set';
  $scope.child4_registered = 'set';
  $scope.child5_registered = 'set';
  $scope.child6_registered = 'set';
  $scope.child7_registered = 'set';
  $scope.child8_registered = 'set';

  $scope.dousaMode = '通常';    // 通常 / テストモード
  /*
   read config
  */
  var mongoid;    // config data id (use at save)
  socket.emit('readConfig');  // setting data read
  socket.on('r_readConfig', function(docs){
    console.log('configs', docs);
    mongoid = docs._id;
    //親機アドレス
    $scope.address = docs.address;
    //グループアドレス
    //$scope.group = docs.group;
    //appid
    $scope.appid = docs.appId;
    // 周波数チャンネル
    $scope.fchannel = docs.fchannel;
    //power factor
    $scope.pfactor = docs.power_factor;
    //co2 cofficient
    $scope.co2_3phi = docs.co2_cofficient.phi3_ac200100v;
    $scope.co2_1phi = docs.co2_cofficient.phi1_ac100v;
    docs.children.forEach(childx =>{
      var label = 'child' + childx.child.toString(10) + '_registered'
      //子機登録
      if(childx.registered === 'yes'){
        $scope[label] = 'set';
      }else{
        $scope[label] = '-';
      }
    });
    if(docs.dousaMode === 'test'){
      if(docs.demandCtrl === true){
        $scope.dousaModeText = 'テストモード / エアコン制御 有り日';
      }else{
        $scope.dousaModeText = 'テストモード / エアコン制御 無し日';
      }
    }else{
      $scope.dousaModeText = '通常';
    }

  });
  /*
    on label
  */
  $scope.parentOnLabel = function(text){
    if((text === 'SET') || (text === 'set')){
	    return {
	      'label': true,
	      'label-pill': true,
	      'label-success': true
			};
    }
    else if((text === 'OK') || (text === 'ok')){
	    return {
	      'label': true,
	      'label-pill': true,
	      'label-success': true
			};
    }
    return '';
  }
  /*
    setting change button
  */
  $scope.pfChannels = [
  　　{name: 'channel11', value: 11},
  　　{name: 'channel12', value: 12},
  　　{name: 'channel13', value: 13},
　　  {name: 'channel14', value: 14},
　　  {name: 'channel15', value: 15},
　　  {name: 'channel16', value: 16},
　　  {name: 'channel17', value: 17},
　　  {name: 'channel18', value: 18},
　　  {name: 'channel19', value: 19},
　　  {name: 'channel20', value: 20},
　　  {name: 'channel21', value: 21},
　　  {name: 'channel22', value: 22},　
　　  {name: 'channel23', value: 23},
　　  {name: 'channel24', value: 24},
　　  {name: 'channel25', value: 25},
　　  {name: 'channel26', value: 26}
　　]
　　/*
　　 s1Labels index serch
　　*/
　　var getChannelIndex = function(label){
　　  var getval = 1;
　 　 $scope.pfChannels.forEach((s, index)=>{
　  　  if(s.value === label) {
　    　  getval = index;
　   　 }
　  　});
　  　return getval;
  };

  $scope.parentSetting = function(){
    //console.log('p-setting', $scope.fchannel, typeof($scope.fchannel));
    $scope.pfChannel = $scope.pfChannels[getChannelIndex(parseInt($scope.fchannel, 10))];
    //console.log($scope.pfChannel );
    // power factor
    $scope.pfPfactor = parseInt($scope.pfactor, 10);
    // co2 co2 cofficient
    $scope.pfCo2coff1phi = $scope.co2_1phi;
    $scope.pfCo2coff3phi = $scope.co2_3phi;
    if( $scope.dousaModeText === '通常'){
      $scope.testMode = false;
    }else{
      $scope.testMode = true;
    }

  }
  /*
   save button
  */
  $scope.pSave = function(){
    console.log('save');
    var dousaMode;
    var demandCont;
    if($scope.testMode){
      dousaMode = 'test';
      demandCont = false;
    }else{
      dousaMode = 'normal';
      demandCont = true;
    }

    $scope.parentNew = {
      _id: mongoid,
      fchannel: $scope.pfChannel.value.toString(10),
      power_factor: $scope.pfPfactor.toString(10),
      co2_cofficient: {
           phi3_ac200100v: $scope.pfCo2coff3phi.toString(10),
           phi1_ac100v: $scope.pfCo2coff1phi.toString(10)
        },
      dousaMode: dousaMode,
      demandCtrl: demandCont,
    }
    var ret = confirm('設定を保存しますか? \n' + JSON.stringify($scope.parentNew, null, '  '));
    if(ret){
      socket.emit('parentSave', JSON.parse(JSON.stringify($scope.parentNew)));
      $('#modalParent').modal('hide');
    }
  }
  socket.on('r_parentSave', function(){
    console.log('upsert_ok');
    socket.emit('readConfig');
  });
  /*
    duosa mode label
  */
  $scope.parentDousaLabel = function(text){
    if(text === '通常'){
	    return {
	      'label': true,
	      'label-pill': true,
	      'label-success': true
			};
    }
    else {
	    return {
	      'label': true,
	      'label-pill': true,
	      'label-warning': true
			};
    }
    return '';
  }


});

















/**************************************************
child controller
*/

var children=[
  {name:'子機番号', child1:'1',child2:'2',child3:'3',child4:'4',child5:'5',child6:'6',child7:'7',child8:'8'},
  {name:'登録',child1:'registered',child2:'registered',child3:'registered',child4:'registered',child5:'registered',child6:'registered',child7:'registered',child8:'registered'},
  {name:'シリアルNo', child1:'exist',child2:'exist',child3:'exist',child4:'exist',child5:'exist',child6:'exist',child7:'exist',child8:'exist'},
  {name:'エアコン設定', child1:'exist',child2:'exist',child3:'exist',child4:'exist',child5:'exist',child6:'exist',child7:'exist',child8:'exist'},
  {name:'タイマ設定', child1:'exist',child2:'exist',child3:'exist',child4:'exist',child5:'exist',child6:'exist',child7:'exist',child8:'exist'},
  {name:'タイマON時刻', child1:'8:00',child2:'8:00',child3:'8:00',child4:'8:00',child5:'8:00',child6:'8:00',child7:'8:00',child8:'8:00'},
  {name:'タイマOFF時刻', child1:'17:00',child2:'17:00',child3:'17:00',child4:'17:00',child5:'17:00',child6:'17:00',child7:'17:00',child8:'17:00'},
  {name:'力率', child1:'80',child2:'800',child3:'80',child4:'80',child5:'80',child6:'80',child7:'80',child8:'80'},
  {name:'子機送信間隔', child1:'1:00',child2:'1:00',child3:'1:00',child4:'1:00',child5:'1:00',child6:'1:00',child7:'1:00',child8:'1:00'},
/*
  {name:'センサ1', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ2', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ3', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ4', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']}
*/
];
var childrenSensor=[
  {name:'センサ1', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ2', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ3', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ4', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']}
];

/*
var children=[
  {name:'子機番号', child1:'1',child2:'2',child3:'3',child4:'4',child5:'5',child6:'6',child7:'7',child8:'8'},
  {name:'登録',child1:'registered',child2:'registered',child3:'registered',child4:'registered',child5:'registered',child6:'registered',child7:'registered',child8:'registered'},
  {name:'室外機設定', child1:'exist',child2:'exist',child3:'exist',child4:'exist',child5:'exist',child6:'exist',child7:'exist',child8:'exist'},
  {name:'ON時間', child1:'8:00',child2:'8:00',child3:'8:00',child4:'8:00',child5:'8:00',child6:'8:00',child7:'8:00',child8:'8:00'},
  {name:'OFF時間', child1:'17:00',child2:'17:00',child3:'17:00',child4:'17:00',child5:'17:00',child6:'17:00',child7:'17:00',child8:'17:00'},
  {name:'力率', child1:'80',child2:'800',child3:'80',child4:'80',child5:'80',child6:'80',child7:'80',child8:'80'},
  {name:'スリープ時間', child1:'1:00',child2:'1:00',child3:'1:00',child4:'1:00',child5:'1:00',child6:'1:00',child7:'1:00',child8:'1:00'},
  {name:'センサ1', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ1', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ1', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ1', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ2', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ2', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ2', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ2', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ3', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ3', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ3', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ3', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ4', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ4', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ4', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']},
  {name:'センサ4', child1:['事務所','1','100A','φ3-200'],child2:['事務所','1','100A','φ3-200'],child3:['事務所','1','100A','φ3-200'],child4:['事務所','1','100A','φ3-200'],child5:['事務所','1','100A','φ3-200'],child6:['事務所','1','100A','φ3-200'],child7:['事務所','1','100A','φ3-200'],child8:['事務所','1','100A','φ3-200']}
];
*/




app.controller('childCont',['$scope', 'socket', function($scope, socket){

  $scope.children = JSON.parse(JSON.stringify(children));
  $scope.childrenSensor = JSON.parse(JSON.stringify(childrenSensor));

  var storeConfig;    // config backkup
  /*
   read config
  */
  var childs;   // child data buffer
  socket.emit('readConfig');  // setting data read
  socket.on('r_readConfig', docs => {
    storeConfig = JSON.parse(JSON.stringify(docs));  // restore for save
    docs.children.forEach( childx => {
      var label = 'child' + childx.child.toString(10);
      //登録
      console.log('childx reg', childx.registered);
      if(childx.registered === 'yes'){
        $scope.children[1][label] = '登録';
      }else{
        $scope.children[1][label] = '-';
      }
      //serial No
　　　　　$scope.children[2][label] = childx.serial_no;
      //室外機設定
      if(childx.output.registered === 'yes'){
        $scope.children[3][label] = '有';
      }else{
        $scope.children[3][label] = '無';
      }
      //室外機タイマ設定
      if(childx.output.registered === 'yes' && childx.output.timerEna === 'yes'){
        $scope.children[4][label] = '有';
        $scope.children[5][label] = childx.output.start;  // timer on time
        $scope.children[6][label] = childx.output.stop;   // timer off time
      }else if(childx.output.registered === 'yes' && childx.output.timerEna !== 'yes'){
        $scope.children[4][label] = '無';
      }else{
        $scope.children[4][label] = '-';
      }
      // timer on, off time
      if(childx.output.registered === 'yes' && childx.output.timerEna === 'yes'){
        $scope.children[5][label] = childx.output.start;  // timer on time
        $scope.children[6][label] = childx.output.stop;   // timer off time
      }else{
        $scope.children[5][label] = '--:--'  // timer on time
        $scope.children[6][label] = '--:--';   // timer off time
      }
/*
      //ON時間
　　　　　$scope.children[5][label] = childx.output.start;
      //OFF時間
　　　　　$scope.children[6][label] = childx.output.stop;
*/
      //力率
      $scope.children[7][label] = docs.power_factor;;
      //スリープ時間
      $scope.children[8][label] = childx.sleep_time;
      //sendors

      for(var i=0; i<4; i++){
        if(childx.sensors[i].disable === false){
          var ac_type = childx.sensors[i]['ac_type'];
          var name = childx.sensors[i]['name'];
          var sensor = childx.sensors[i]['sensor'];
          var sub_no = childx.sensors[i]['sub_no'];
          $scope.childrenSensor[i][label] = [name, sub_no, sensor, ac_type];
        }else{
          var ac_type = '';
          var name = '使用しない';
          var sensor = '';
          var sub_no = '';
          $scope.childrenSensor[i][label] = [name, sub_no, sensor, ac_type];
        }
      }

    });
    console.log($scope.childrenSensor);
    //form data
    childs = JSON.parse(JSON.stringify(docs.children));
    console.log('childs', childs);

    $scope.childForm = JSON.parse(JSON.stringify(childs[0]));
    $scope.childForm.child = $scope.childNos[0].name;
  });   // endod config read


  /*
  settin sw on / form parameters
  */
  /*
  var childForm = {
    childNo : 1,
    register : 1,
    serial : '100000',
    outerRegister : 1,
    startTime : '12:00',
    stopTime : '18:00',
    sleepTime : '1:00',
    sensers :[
      {place:'a', bangou:'1',senserRange:'100', ac:'100'},
      {place:'a', bangou:'1',senserRange:'100', ac:'100'},
      {place:'a', bangou:'1',senserRange:'100', ac:'100'},
      {place:'a', bangou:'1',senserRange:'100', ac:'100'}
    ]
  };
  */
  /*
   子機セレクタ　オプション
  */
  $scope.childNos = [
    {name: '子機No1', value: 1},
    {name: '子機No2', value: 2},
    {name: '子機No3', value: 3},
    {name: '子機No4', value: 4},
    {name: '子機No5', value: 5},
    {name: '子機No6', value: 6},
    {name: '子機No7', value: 7},
    {name: '子機No8', value: 8}
  ]
  /*
   sensor place options
  */
  $scope.s1Places = [
    {name: '事務所', value: 1},
    {name: '会議室', value: 2},
    {name: '応接室', value: 3},
    {name: 'ホール', value: 4},
    {name: '設計室', value: 5},
    {name: '工場', value: 6},
    {name: '倉庫', value: 7},
    {name: '室外機', value: 8},
    {name: '機械', value: 9},
    {name: '昇降機', value: 10},
    {name: 'コンプレッサ', value: 11},
    {name: 'チラー', value: 12},
    {name: '集塵機', value: 13},
    {name: 'エアコン', value: 14},
    {name: '炉', value: 15},
    {name: 'ヒーター', value: 16},
    {name: '冷蔵庫', value: 17},
    {name: '冷凍庫', value: 18},
    {name: 'レーザー加工機', value: 19},
    {name: 'タレパン', value: 20},
    {name: 'ブレーキP', value: 21},
    {name: 'その他', value: 22}
  ]
  $scope.s2Places = $.extend(true, [], $scope.s1Places);
  $scope.s3Places = $.extend(true, [], $scope.s1Places);
  $scope.s4Places = $.extend(true, [], $scope.s1Places);
  /*
   s1Places index serch
  */
  var getPlaceIndex = function(name){
    var getval;
    $scope.s1Places.forEach((s, index)=>{
      if(s.name === name) {
        getval = index;
      }
    });
    return getval;
  };

  $scope.s1Labels = [
    {name: '無し', value: 0},
    {name: '1', value: 1},
    {name: '2', value: 2},
    {name: '3', value: 3},
    {name: '4', value: 4},
    {name: '5', value: 5},
    {name: '6', value: 6},
    {name: '7', value: 7},
    {name: '8', value: 8},
    {name: '9', value: 9},
    {name: '10', value: 10},
    {name: '11', value: 11},
    {name: '12', value: 12},
    {name: '13', value: 13},
    {name: '14', value: 14},
    {name: '15', value: 15},
    {name: '16', value: 16},
    {name: '17', value: 17},
    {name: '18', value: 18},
    {name: '19', value: 19},
    {name: '20', value: 20},
  ]
  $scope.s2Labels = $.extend(true, [], $scope.s1Labels);
  $scope.s3Labels = $.extend(true, [], $scope.s1Labels);
  $scope.s4Labels = $.extend(true, [], $scope.s1Labels);
  /*
   s1Labels index serch
  */
  var getLabelIndex = function(label){
    var getval = 1;
    $scope.s1Labels.forEach((s, index)=>{
      if(s.name === label) {
        getval = index;
      }
    });
    return getval;
  };
  /*
   sensor types
  */
  $scope.s1Sensors = [
    {name: '100A', value: 1},
    {name: '250A', value: 2},
    {name: '500A', value: 3}
  ]
  $scope.s2Sensors = $.extend(true, [], $scope.s1Sensors);
  $scope.s3Sensors = $.extend(true, [], $scope.s1Sensors);
  $scope.s4Sensors = $.extend(true, [], $scope.s1Sensors);
  /*
   s1Labels index serch
  */
  var getSensorIndex = function(sensor){
    var getval = 0;
    $scope.s1Sensors.forEach((s, index)=>{
      if(s.name === sensor) {
        getval = index;
      }
    });
    return getval;
  };

  /*
   ac-type options
  */
  $scope.s1AcTypes = [
    {name: 'φ3-200V', value: 1},
    {name: 'φ1-200V', value: 2},
    {name: 'φ1-100V', value: 3}
  ]

  $scope.s2AcTypes = $.extend(true, [], $scope.s1AcTypes);
  $scope.s3AcTypes = $.extend(true, [], $scope.s1AcTypes);
  $scope.s4AcTypes = $.extend(true, [], $scope.s1AcTypes);
  /*
   s1Labels index serch
  */
  var getAcTypeIndex = function(actype){
    var getval = 0;
    $scope.s1AcTypes.forEach((s, index)=>{
      if(s.name === actype) {
        getval = index;
      }
    });
    return getval;
  };

  $scope.childsetting = function(no){
    $scope.childForm = JSON.parse(JSON.stringify(childs[no-1]));
    // childNo
    var x = $scope.childForm.child;
    console.log(childs);
    $scope.childNo = $scope.childNos[no-1];
    // register
    if($scope.childForm.registered == 'yes'){
      $scope.Fregister = true;
    }else{
      $scope.Fregister = false;
    }
    // serial
    //console.log($scope.childForm.serial_no);
    // outer
    if($scope.childForm.output.registered == 'yes'){
      $scope.Fouter = true;
    }else{
      $scope.Fouter = false;
    }
    // outer timer
    if($scope.childForm.output.timerEna == 'yes'){
      $scope.FouterTimer = true;
    }else{
      $scope.FouterTimer = false;
    }
    // outer start time
    var stime = new Date(1970, 0, 1, 14, 57, 0);
    var startNew = $scope.childForm.output.start.split(':');
    stime.setHours(parseInt(startNew[0], 10));
    stime.setMinutes(parseInt(startNew[1], 10));
    $scope.Fstart = stime;
    // outer stop time
    var stoptime = new Date(1970, 0, 1, 14, 57, 0);

    var stopNew = $scope.childForm.output.stop.split(':');
    stoptime.setHours(parseInt(stopNew[0], 10));
    stoptime.setMinutes(parseInt(stopNew[1], 10));
    $scope.Fstop = stoptime;
    // sleep time
    $scope.FsleepTime = parseInt($scope.childForm.sleep_time, 10);
    // sensers
    $scope.childForm.sensors.forEach((s)=>{
      no = parseInt(s.chan, 10) - 1;
      if(no === 0){
        $scope.s1Place = $scope.s1Places[getPlaceIndex(s.name)];
        $scope.s1Label = $scope.s1Labels[getLabelIndex(s.sub_no)];
        $scope.s1Sensor = $scope.s1Sensors[getSensorIndex(s.sensor)];
        $scope.s1AcType = $scope.s1AcTypes[getAcTypeIndex(s.ac_type)];
        $scope.s1Disable = s.disable;
      }else if(no === 1){
        $scope.s2Place = $scope.s2Places[getPlaceIndex(s.name)];
        $scope.s2Label = $scope.s2Labels[getLabelIndex(s.sub_no)];
        $scope.s2Sensor = $scope.s2Sensors[getSensorIndex(s.sensor)];
        $scope.s2AcType = $scope.s2AcTypes[getAcTypeIndex(s.ac_type)];
        $scope.s2Disable = s.disable;
      }else if(no === 2){
        $scope.s3Place = $scope.s3Places[getPlaceIndex(s.name)];
        $scope.s3Label = $scope.s3Labels[getLabelIndex(s.sub_no)];
        $scope.s3Sensor = $scope.s3Sensors[getSensorIndex(s.sensor)];
        $scope.s3AcType = $scope.s3AcTypes[getAcTypeIndex(s.ac_type)];
        $scope.s3Disable = s.disable;
      }else if(no === 3){
        $scope.s4Place = $scope.s4Places[getPlaceIndex(s.name)];
        $scope.s4Label = $scope.s4Labels[getLabelIndex(s.sub_no)];
        $scope.s4Sensor = $scope.s4Sensors[getSensorIndex(s.sensor)];
        $scope.s4AcType = $scope.s4AcTypes[getAcTypeIndex(s.ac_type)];
        $scope.s4Disable = s.disable;
      }
    });
  };


  /*
    on label
  */
  $scope.childrenOnLabel = function(text){
    if((text === 'SET') || (text === 'set')){
	    return {
	      'label': true,
	      'label-pill': true,
	      'label-success': true
			};
    }
    else if((text === 'OK') || (text === 'ok')){
	    return {
	      'label': true,
	      'label-pill': true,
	      'label-success': true
			};
    }
    return '';
  }
  /*
   line arry split
  */
  $scope.childrenLine = function(line){
    //console.log(line);
    if(line.name === 'センサ1'){
      console.log(line);
      return{
        view : 'test'
      }
    }
  }
  /*
    設定保存
  */
  var childInfo ={
    "child" : "1",
    "registered" : "yes",
    "sleep_time" : "1",
    "serial_no" : "10077F4",
    "output" : {
      "registered" : "yes",
      "stop" : "17:00",
      "start" : "10:00"
      },
    "sensors" : [
      {
        "sensor" : "500a",
        "ac_type" : "3phi-100v",
        "sub_no" : "1",
        "name" : "工場",
        "chan" : "1",
        "disable" : false
      },
      {
        "sensor" : "500a",
        "ac_type" : "3phi-100v",
        "sub_no" : "1",
        "name" : "工場",
        "chan" : "2",
        "disable" : false
      },
      {
        "sensor" : "500a",
        "ac_type" : "3phi-100v",
        "sub_no" : "1",
        "name" : "工場",
        "chan" : "3",
        "disable" : false
      },
      {
        "sensor" : "500a",
        "ac_type" : "3phi-100v",
        "sub_no" : "1",
        "name" : "工場",
        "chan" : "4",
        "disable" : false
      }
    ]
  };
  $scope.childSave = function(){
    childInfo.child = $scope.childNo.value.toString(10);
    if($scope.Fregister === true){
      childInfo.registered = 'yes';
    }else{
      childInfo.registered = 'no';
    }
    childInfo.serial_no = $scope.childForm.serial_no;
    childInfo.sleep_time = $scope.FsleepTime.toString(10);
    if($scope.Fouter === true){
      childInfo.output.registered = 'yes';
    }else{
      childInfo.output.registered = 'no'
    }
    if($scope.FouterTimer === true){
      childInfo.output.timerEna = 'yes';
    }else{
      childInfo.output.timerEna = 'no'
    }
    var stime =  $scope.Fstart.toLocaleTimeString().split(':');
    childInfo.output.start = stime[0] + ':' + stime[1];
    var etime =  $scope.Fstop.toLocaleTimeString().split(':');
    childInfo.output.stop = etime[0] + ':' + etime[1];
    childInfo.sensors.forEach( (s) =>{
      var no = parseInt(s.chan, 10);
      if(no === 1){
        //s.name = $scope.s1Place.name;
        //console.log($scope.s1Place.name);
        childInfo.sensors[0].name = $scope.s1Place.name;
        childInfo.sensors[0].sub_no = $scope.s1Label.value.toString(10);
        childInfo.sensors[0].sensor = $scope.s1Sensor.name;
        childInfo.sensors[0].ac_type = $scope.s1AcType.name;
        childInfo.sensors[0].disable = $scope.s1Disable;
      }
      if(no === 2){
        childInfo.sensors[1].name = $scope.s2Place.name;
        childInfo.sensors[1].sub_no = $scope.s2Label.value.toString(10);
        childInfo.sensors[1].sensor = $scope.s2Sensor.name;
        childInfo.sensors[1].ac_type = $scope.s2AcType.name;
        childInfo.sensors[1].disable = $scope.s2Disable;
      }
      if(no === 3){
        childInfo.sensors[2].name = $scope.s3Place.name;
        childInfo.sensors[2].sub_no = $scope.s3Label.value.toString(10);
        childInfo.sensors[2].sensor = $scope.s3Sensor.name;
        childInfo.sensors[2].ac_type = $scope.s3AcType.name;
        childInfo.sensors[2].disable = $scope.s3Disable;
      }
      if(no === 4){
        childInfo.sensors[3].name = $scope.s4Place.name;
        childInfo.sensors[3].sub_no = $scope.s4Label.value.toString(10);
        childInfo.sensors[3].sensor = $scope.s4Sensor.name;
        childInfo.sensors[3].ac_type = $scope.s4AcType.name;
        childInfo.sensors[3].disable = $scope.s4Disable;
      }
    });

    //console.log(childInfo);
    var ret = confirm('設定を保存しますか？ \n' + JSON.stringify(childInfo, null, '  ') );
    //var ret = confirm('設定を保存しますか？ \n');
    if(ret){
      var newIndex;
      storeConfig.children.forEach( (childx, index) => {
        if(childx.child === childInfo.child){
          newIndex = index;
        }
      });
      storeConfig.children[newIndex] = JSON.parse(JSON.stringify(childInfo));
      console.log('store', newIndex, storeConfig);
      socket.emit('childSave', JSON.parse(JSON.stringify(storeConfig)));
      $('#myChild').modal('hide');    // modal close only save OK!!!
    }

    socket.on('r_childSave', function(){
      console.log('upsert_ok');
      socket.emit('readConfig');
    });

  }
  /*
$(document).ready( ()=> {
  $('#childModal')
});
*/

}]);






/* *********************************************************

  meas controller

format
{
[time: xx:xx:xx, c1:[x,x,x,x], c2:[x,x,x,x] ...
}
*/

var power = {
  time:'00:00',
  c1:['0','0','0','0'],
  c2:['0','0','0','0'],
  c3:['0','0','0','0'],
  c4:['0','0','0','0'],
  c5:['0','0','0','0'],
  c6:['0','0','0','0'],
  c7:['0','0','0','0'],
  c8:['0','0','0','0'],
  powerSum: 0,
  co2Sum: 0,
};
var powers;   // json power data

/*
  power data format

*/
function powerFormat(powerArray){
  var formatedPower = [];
  powerArray.forEach( (power) => {
    var num = new Number(power);
    formatedPower.push(parseFloat(num.toFixed(2)));
  });
  return formatedPower;
}

/*
 format output

  param  : output , 1 or 0
  return : text string
*/
function formatOutput(output){
  if(output === 1){
    return 'on';
  }
  return 'off';
}


/*
   calc meas power
   sum power1fai & power3fai >>> return  hai[chan1 .. chan4]

   param  : doc (30min json)
   return : power sum array[4]

*/
function calcMeasPower(doc, chanLabel){
  var fpower = powerFormat(doc.power[chanLabel]);
  var fpower3 = powerFormat(doc.power3[chanLabel]);
  var sums = [4];
  for (var i=0; i<4; i++){
    sums[i] = fpower[i] + fpower3[i];
  }
  console.log('sums', sums);
  return sums;
}




/*
  meas controller

  param  : $scope, socket
  return : none
*/
/*
rev101まで
fai1,3の加算をとり表示する
*/
/*
app.controller('measCont',['$scope', 'socket', function($scope, socket){
  socket.emit('read30min', 0);  // today 30min data read
  socket.on('r_read30min', docs => {
    $scope.measPower = [];
    docs.forEach( (doc) => {
      var date = doc.time.split('T');
      $scope.measDay = date[0];
      var time = date[1].split(':');
      power.time = time[0]+':'+time[1];

      var powerMix = calcMeasPower(doc, 'c1');       // sum 1fai power & 3fai power
      power.c1 = powerMix.toString() + '/' + formatOutput(doc.output[0]);
      powerMix = calcMeasPower(doc, 'c2');       // sum 1fai power & 3fai power
      power.c2 = powerMix.toString() + '/' + formatOutput(doc.output[1]);
      powerMix = calcMeasPower(doc, 'c3');       // sum 1fai power & 3fai power
      power.c3 = powerMix.toString() + '/' + formatOutput(doc.output[2]);
      powerMix = calcMeasPower(doc, 'c4');       // sum 1fai power & 3fai power
      power.c4 = powerMix.toString() + '/' + formatOutput(doc.output[3]);
      powerMix = calcMeasPower(doc, 'c5');       // sum 1fai power & 3fai power
      power.c5 = powerMix.toString() + '/' + formatOutput(doc.output[4]);
      powerMix = calcMeasPower(doc, 'c6');       // sum 1fai power & 3fai power
      power.c6 = powerMix.toString() + '/' + formatOutput(doc.output[5]);
      powerMix = calcMeasPower(doc, 'c7');       // sum 1fai power & 3fai power
      power.c7 = powerMix.toString() + '/' + formatOutput(doc.output[6]);
      powerMix = calcMeasPower(doc, 'c8');       // sum 1fai power & 3fai power
      power.c8 = powerMix.toString() + '/' + formatOutput(doc.output[7]);
      var num = new Number(doc.power_sum_30m);
      power.powerSum = parseFloat(num.toFixed(2));
      num = new Number(doc.power3_sum_30m);
      power.power3Sum = parseFloat(num.toFixed(2));      
      $scope.measPower.push(JSON.parse(JSON.stringify(power)));
    });

  });
  //
  //  folder open button action
  //
  $scope.folderOpen = function(){
    socket.emit('folderOpen');
  };
}]);
*/
/*
 rev102
 powerWithErrWithoutDemand.view.powerデータを計測値として使用する。
 f1,3の区別はない。
 ERR文字列がはいっている場合があるので、その判別をする。
*/
/*
 check err ([])
*/
function checkErr(val){
  var checked = '';
  for(var i=0; i<4; i++){
    if(typeof(val[i]) !== 'string'){
      val[i] = val[i].toFixed(2);
    }
    checked += val[i] + ',';
  }
  checked = checked.substring(0, checked.length-1);
  return checked;
}
function checkErrF(fval){
  if(typeof(fval) !== 'string'){
    fval = fval.toFixed(2);
  }
  return fval;
}
/*
 format output

  param  : output 1 or 0; err 1=err, 0=normal
  return : text string
*/
function formatOutputWithErr(output, err){
  if(err !== 0){
    return 'ERR';
  }
  if(output === 1){
    return 'on';
  }
  return 'off';
}
/*
*/
app.controller('measCont',['$scope', 'socket', function($scope, socket){
  socket.emit('read30min', 0);  // today 30min data read
  socket.on('r_read30min', docs => {
    $scope.measPower = [];
    docs.forEach( (doc) => {
      /*
       30min time label source
      */
      /*
       time label = meas start time
      */
      //var docpv = doc.powerWithErrWithoutDemand.view;
      //var date = docpv.time.split('T');
      /*
       time label = meas end time
      */
      var docpv = doc.powerWithErrWithoutDemand.view;
      var date = doc.time.split('T');
      
      $scope.measDay = date[0];
      var time = date[1].split(':');
      power.time = time[0]+':'+time[1];

      /*
      var checked = checkErr(docpv.power.c1);
      power.c1 = checked + '/' + formatOutput(doc.output[0]);
      checked = checkErr(docpv.power.c2);
      power.c2 = checked + '/' + formatOutput(doc.output[1]);
      checked = checkErr(docpv.power.c3);
      power.c3 = checked + '/' + formatOutput(doc.output[2]);
      checked = checkErr(docpv.power.c4);
      power.c4 = checked + '/' + formatOutput(doc.output[3]);
      checked = checkErr(docpv.power.c5);
      power.c5 = checked + '/' + formatOutput(doc.output[4]);
      checked = checkErr(docpv.power.c6);
      power.c6 = checked + '/' + formatOutput(doc.output[5]);
      checked = checkErr(docpv.power.c7);
      power.c7 = checked + '/' + formatOutput(doc.output[6]);
      checked = checkErr(docpv.power.c8);
      power.c8 = checked + '/' + formatOutput(doc.output[7]);
      */
      var checked = checkErr(docpv.power.c1);
      power.c1 = checked + '/' + formatOutputWithErr(doc.output[0], doc.powerWithErrWithoutDemand.measErr[0]);
      checked = checkErr(docpv.power.c2);
      power.c2 = checked + '/' + formatOutputWithErr(doc.output[1], doc.powerWithErrWithoutDemand.measErr[1]);
      checked = checkErr(docpv.power.c3);
      power.c3 = checked + '/' + formatOutputWithErr(doc.output[2], doc.powerWithErrWithoutDemand.measErr[2]);
      checked = checkErr(docpv.power.c4);
      power.c4 = checked + '/' + formatOutputWithErr(doc.output[3], doc.powerWithErrWithoutDemand.measErr[3]);
      checked = checkErr(docpv.power.c5);
      power.c5 = checked + '/' + formatOutputWithErr(doc.output[4], doc.powerWithErrWithoutDemand.measErr[4]);
      checked = checkErr(docpv.power.c6);
      power.c6 = checked + '/' + formatOutputWithErr(doc.output[5], doc.powerWithErrWithoutDemand.measErr[5]);
      checked = checkErr(docpv.power.c7);
      power.c7 = checked + '/' + formatOutputWithErr(doc.output[6], doc.powerWithErrWithoutDemand.measErr[6]);
      checked = checkErr(docpv.power.c8);
      power.c8 = checked + '/' + formatOutputWithErr(doc.output[7], doc.powerWithErrWithoutDemand.measErr[7]);

      power.powerSum = checkErrF(docpv.power_sum_30m);
      power.power3Sum = checkErrF(docpv.power3_sum_30m);
      
      $scope.measPower.push(JSON.parse(JSON.stringify(power)));
    });

  });
  //
  //  folder open button action
  //
  $scope.folderOpen = function(){
    socket.emit('folderOpen');
  };
}]);


/*
 header view data

  server uart state data
*/

app.controller('headerCont', function($scope, socket){
  $scope.uartState = '×';
  socket.on('uartState', (uartState)=>{
    console.log('uart change: ', uartState);
    if(uartState === false){
      $scope.uartState = '×';      
    }else{
      $scope.uartState = '良'; 
    }
  });
  socket.emit('getUartState');
  $scope.headerUartStateLabel = function(state){
    if(state === '×') {
	    return {
	      'label': true,
	      'label-pill': true,
	      'label-danger': true
			};
    }
    else {
	    return {
	      'label': true,
	      'label-pill': true,
	      'label-success': true
			};
    }
    return '';
  }
});